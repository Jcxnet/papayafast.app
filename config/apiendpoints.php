<?php

	define('API_PFAST_URL','http://papayafast.api.local.vh');

return [
	'API_APP_VERSION'			=> API_PFAST_URL."/version",
	'API_LOCALS_LIST' 		=> API_PFAST_URL."/locals",
	'API_LOCALS_FAVORITE'	=> API_PFAST_URL."/locals/user/favorite",
	'API_LOCALS_USER'			=> API_PFAST_URL."/locals/user/favorites",
	'API_LOCALS_STATUS'		=> API_PFAST_URL."/locals/status",
	'API_LOCALS_OPEN'			=> API_PFAST_URL."/locals/open",
	'API_LOCALS_DELIVERY'	=> API_PFAST_URL."/locals/delivery",

	'API_MENUS_LIST'   				=> API_PFAST_URL."/menus",
	'API_PRODUCT_AVAILABLE'		=> API_PFAST_URL."/menus/product/available",
	'API_PRODUCTS_AVAILABLE'	=> API_PFAST_URL."/menus/products/available",

	'API_USER_LOGIN'			=> API_PFAST_URL."/users/login",
	'API_USER_LOGOUT'			=> API_PFAST_URL."/users/logout",
	'API_USER_REGISTER'		=> API_PFAST_URL."/users/register",
	'API_USER_GUEST'			=> API_PFAST_URL."/users/guest",
	'API_USER_INFO' 			=> API_PFAST_URL."/users/info",
	'API_USER_UPDATE' 		=> API_PFAST_URL."/users/update",
	'API_USER_LOCALS' 		=> API_PFAST_URL."/users/locals",

	'API_ADDRESSES_USER' 		=> API_PFAST_URL."/addresses/user",
	'API_ADDRESSES_ADD' 		=> API_PFAST_URL."/addresses/create",
	'API_ADDRESSES_UPDATE' 	=> API_PFAST_URL."/addresses/update",
	'API_ADDRESSES_REMOVE' 	=> API_PFAST_URL."/addresses/remove",
	'API_ADDRESSES_INFO' 	  => API_PFAST_URL."/addresses/info",
	'API_ADDRESSES_COVER' 	=> API_PFAST_URL."/locals/address/cover",

	'API_PAYMENTS_VALIDATE' => API_PFAST_URL."/checkouts/validate",
	'API_PAYMENTS_GENERATE' => API_PFAST_URL."/payments/generate",
	'API_PAYMENTS_LOCAL'  	=> API_PFAST_URL."/checkouts/local",

	'API_ORDERS_CREATE'  			=> API_PFAST_URL."/orders/create",
	'API_ORDERS_GENERATE'  		=> API_PFAST_URL."/orders/generate",
	'API_ORDERS_RESUME'  			=> API_PFAST_URL."/orders/resume",
	'API_ORDERS_SEND'  				=> API_PFAST_URL."/orders/send",
	'API_ORDERS_VERIFY' 			=> API_PFAST_URL."/orders/verify",
	'API_ORDERS_PAID' 				=> API_PFAST_URL."/orders/paid",
	'API_ORDERS_CONFIRMATION'	=> API_PFAST_URL."/orders/confirmation",
	'API_ORDERS_HISTORY'			=> API_PFAST_URL."/orders/history",
	'API_ORDERS_DETAIL'				=> API_PFAST_URL."/orders/detail",
	'API_ORDERS_STATUS'				=> API_PFAST_URL."/orders/status",

	'API_CREDITCARD_CHARGE'	  => API_PFAST_URL."/creditcards/charge",
	'API_CREDITCARD_USER'	  	=> API_PFAST_URL."/creditcards/list",
	'API_CREDITCARD_REMOVE'	 	=> API_PFAST_URL."/creditcards/remove",

	'API_CHECKOUT_BILLING' 		=> API_PFAST_URL."/checkouts/billing",
	'API_CHECKOUT_PAYMENT' 		=> API_PFAST_URL."/checkouts/payment",

];
