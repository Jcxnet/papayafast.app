
function productPrice(data){
	var product = getStorageItem(orderMenu,'products',data.productid);
	if(product){
		var price = parseFloat(product.price);
		for(var i=0;i<data.properties.length;i++){
			price += getGroupsPrice(product.groups, data.properties[i]);
			price += getPropertiesPrice(product.properties, data.properties[i]);
		}
		return parseFloat(price).toFixed(2);
	}
}

/*function getProductPrice(product){
	var price = parseFloat(product.price);
	price += getGroupsPrice(product.groups, data.properties[i]);
	price += getPropertiesPrice(product.properties, data.properties[i]);
	return parseFloat(price*parseInt(product.cantidad)).toFixed(2);
}*/

function getGroupsPrice(groups, id){
	var price = 0;
	for(var index in groups) {
    var group = groups[index];
    price += getPropertiesPrice(group.properties,id);
	}
	return parseFloat(price);
}

function getPropertiesPrice(props, id){
	for(var index in props){
		var prop = props[index];
		if(parseInt(prop.id) == parseInt(id)){
			return parseFloat(prop.price);
		}
	}
	return 0;
}

function deleteOrderProduct(id){
	var order	= getLocalData(orderProducts,'list');
	var list = [];
	for(var j in order){
		var item = order[j];
		if(parseInt(item.orderid) != parseInt(id)){
			list.push(item);
		}
	}
	setLocalData(orderProducts,'list',list);
}

function duplicateOrderProduct(id){
	var order	= getLocalData(orderProducts,'list');
	var list = []; var product=[]; var max = 0;
	for(var j in order){
		var item = order[j];
		if(parseInt(item.orderid) == parseInt(id)){
			var product = (JSON.parse(JSON.stringify(item)));
		}
		if( parseInt(item.orderid)> max )
			max = parseInt(item.orderid);
		list.push(item);
	}
	product.orderid = max+1;
	list.push(product);
	setLocalData(orderProducts,'list',list);
	return JSON.stringify(product);
}

function updateCart(data){
	var product = getCartProduct(data.orderid);
	if(product){
		product = getProductPropertiesSorted(product,data.properties);
		product.cantidad = data.cantidad;
		order = updateCartProduct(product);
		setLocalData(orderProducts,'list',order);
		return true;
	}
	return false;
}

function getCartProduct(orderid){
	var list = getLocalData(orderProducts,'list');
	for(var j in list){
		var item = list[j];
		if(parseInt(item.orderid) == parseInt(orderid)){
			return item
		}
	}
	return false;
}

function updateCartProduct(product){
	var order 	= getLocalData(orderProducts,'list');
	var list = [];
	for(var j in order){
		var item = order[j];
		if(parseInt(item.orderid) == parseInt(product.orderid)){
			list.push(product);
		}else{
			list.push(item);
		}
	}
	return list;
}

function updateCartProductAmount(id, cantidad){
	var order 	= getLocalData(orderProducts,'list');
	var list = [];
	for(var p in order){
		var item = order[p];
		if(parseInt(item.orderid) == id){
			item.cantidad = cantidad;
		}
		list.push(item);
	}
	setLocalData(orderProducts,'list',list);
}

function addToCart(data){
	var product = getStorageItem(orderMenu,'products',data.productid);
	product = getProductPropertiesSorted(product,data.properties);
	//save in local storage
	var order = getLocalData(orderProducts,'list');
	if(!order){
		order = new Array();
		product.orderid = 1;
	}else{
		product.orderid = getLatestPosition(order)+1;
	}
	product.cantidad = data.cantidad;
	order.push(product);
	setLocalData(orderProducts,'list',order);
	return true;
}

function orderTotal(){
	var order = getLocalData(orderProducts,'list');
	var total = 0;
	for(var p in order){
		var item = order[p];
		total += parseInt(item.cantidad);
	}
	return total;
}

function showOrderTotal(item){
	$('#'+item).html('<i class="fa fa-cutlery"></i> <span class="notification">'+orderTotal()+'</span> Mi pedido');
}


function getLatestPosition(order){
	var max = 0;
	for(var j in order){
		var item = order[j];
		if(parseInt(item.orderid) > max){
			max = parseInt(item.orderid);
		}
	}
	return max;
}

function getProductPropertiesSorted(product,props){
	// sort properties order
	for(var i=0;i<props.length;i++){
		for(var index in product.groups) {
    	if(foundInProperties(product.groups[index].properties,props[i])){
    		product.groups[index].properties = orderProperties(product.groups[index].properties,props[i]);
    	}
		}
	}
	return product;
}

function foundInProperties(props,id){
	for(var pos in props){
		if(parseInt(props[pos].id) == parseInt(id)){
			return true;
		}
	}
	return false;
}

function orderProperties(props,id){
	var list = []; var order = 2;
	for(var j in props){
		var prop = props[j];
		if(parseInt(prop.id) == parseInt(id)){
			prop.order = 1;
		}else{
			prop.order = order;
			order++;
		}
		list.push(prop);
	}
	return list;
}


function existProductMenuActual(product){
	var productsMenu = getLocalData(orderMenu,'products');
	for(var x in productsMenu){
		var productMenu = productsMenu[x];
		if(productMenu.code == product.code)
			return productMenu;
	}
	return false;
}

function verifyActualOrderProducts(){
	var productsOrder = getLocalData(orderProducts,'list');
	var orderActual = new Array();
	var total = productsOrder.length;
	var added = 0;
	if(total == 0)
		return true;
	for (var x in productsOrder){
		var productOrder = productsOrder[x];
		productOrder = existProductMenuActual(productOrder);
		if(productOrder != false){
			productOrder.orderid = getLatestPosition(orderActual)+1;
			orderActual.push(productOrder);
			added++;
		}
	}
	setLocalData(orderProducts,'list',orderActual);
}

function orderProductsList(productsOrder){
	var list 	= new Array();
	for (var x in productsOrder){
		var productOrder = productsOrder[x];
		var product = new Object();
		product.code 				= productOrder.code;
		product.cantidad 		= productOrder.cantidad;
		product.order 			= productOrder.order;
		product.properties 	= orderProductProperties (productOrder.properties, false);
		product.groups 			= orderProductGroups (productOrder.groups);
		product.image				= productOrder.image;
		product.title				= productOrder.title;
		list.push(product);
	}
	return list;
}

function orderProductProperties(Properties, selected){
	var list = new Array();
	for (var p in Properties){
		var property = Properties[p];
		var item = new Object();
		item.code 		= property.code;
		item.cantidad = property.cantidad;
		item.order 		= property.order;
		item.title		= property.title;
		if(selected){
			if(property.order == 1)
				list.push(item);
		}else{
			list.push(item);
		}
	}
	return list;
}

function orderProductGroups(Groups){
	var list = new Array();
	for(var g in Groups){
		var group = Groups[g];
		var item 	= new Object();
		item.id			= group.id;
		item.order 	= group.order;
		item.properties = orderProductProperties(group.properties,true);
		list.push(item);
	}
	return list;
}