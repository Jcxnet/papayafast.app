	var tokenStorage 			= $.initNamespaceStorage("userlocal");
	var userStorage 			= $.initNamespaceStorage("user");
	var appStorage 				= $.initNamespaceStorage("app");
	var addressesStorage 	= $.initNamespaceStorage("addresses");
	var localsStorage 		= $.initNamespaceStorage("locals");
	var menusStorage 			= $.initNamespaceStorage("menus");
	var geoStorage        = $.initNamespaceStorage("geolocation");
	var orderAddress      = $.initNamespaceStorage("orderAddress");
	var orderMenu      		= $.initNamespaceStorage("orderMenu");
	var orderProducts  		= $.initNamespaceStorage("orderProducts");
	var orderRuc  				= $.initNamespaceStorage("orderRuc");
	var orderPayment			= $.initNamespaceStorage("orderPayment");



function storageSupported(){
	try {
    localStorage.setItem("test", "test");
    localStorage.removeItem("test");
    return true;
  }catch (e) {
    return false;
  }
}

function getLocalStorage(storage){
	return storage.localStorage.get();
}

function getLocalData(storage, key){
	if (storage.localStorage.isEmpty(key))
		return false;
	return storage.localStorage.get(key);
}

function setLocalData(storage, key, value){
	storage.localStorage.set(key,value);
}

function deleteLocalData(storage, key){
	storage.localStorage.remove(key);
}

function deleteAllLocalData(storage){
	storage.localStorage.removeAll();
}

function setArrayLocalData(storage,data){
	for(var key in data) {
			setLocalData(storage,key, data[key]);
	}
}

function getStorageArray(storage,key,ids){
	var items = Array();
	var localItems = getLocalData(storage,key);
	if(localItems){
		for (var i = 0; i < ids.length; i++) {
	    items.push(localItems[ids[i]]);
		}
		return items;
	}
	return null;
}

function getStorageItem(storage,key,id){
	var item = getLocalData(storage,key);
	if(item){
		return item[id];
	}else{
		return null;
	}
}

function clearOrderStorage(){
	deleteAllLocalData(orderMenu);
	deleteAllLocalData(orderProducts);
	deleteAllLocalData(orderPayment);
	deleteAllLocalData(orderAddress);
}

function getRandom(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function showMessage(title,message){
	if(message == '' || typeof(message) == 'undefined')
		return false;
  ons.notification.alert({
    messageHTML: message,
    title: title,
    animation: 'fade',
  });
}


function showToast(title, message, time){
	if(typeof(time) == 'undefined')
		time = 2;
	$.toast({
    heading   : title,
    text      : message,
    hideAfter : time * 1000,
    showHideTransition: 'slide',
    allowToastClose: true,
    position: 'bottom-center',
    loader: false,
  });
}

/*function showConfirmationCallback(title, message, buttons, callback){
	ons.notification.confirm({
    message: message,
    title: title,
    buttonLabels: buttons,
    animation: 'fade',
    primaryButtonIndex: 0,
    cancelable: false,
    callback: function(index) {
      if(index == 0) callback;
    }
  });
}*/