<?php

namespace App\Http\Transformers;

use App\Local;
use League\Fractal\TransformerAbstract;

class LocalTransformer extends TransformerAbstract{

	public function transform(Local $local){
		return [
			'id'	    => $local->id,
      'name'    => $local->name,
      'address'	=> $local->address['address'],
      'menu'		=> $local->menu,
      'status' 	=> $local->status,
      'open'		=> $local->open,
      'image'		=> $local->image,
		];
	}

}
