<?php

namespace App\Http\Transformers;

use App\Local;
use League\Fractal\TransformerAbstract;

class LocalinfoTransformer extends TransformerAbstract{

	public function transform(Local $local){
		return [
			'id'	      => $local->id,
      'name'      => $local->name,
      'address'   => $local->address['address'],
      'point'			=> $local->address['point'],
      'status' 		=> $local->status,
      'open'			=> $local->open,
      'image'			=> $local->image,
      'horario'		=> $local->timetable,
      'contacto'	=> $local->contact,
		];
	}

}
