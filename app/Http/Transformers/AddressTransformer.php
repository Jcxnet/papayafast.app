<?php

namespace App\Http\Transformers;

use App\Address;
use League\Fractal\TransformerAbstract;

class AddressTransformer extends TransformerAbstract{

	public function transform(Address $address){
		return [
			'id'	      => $address->id,
      'name'      => $address->name,
      'address'   => $address->address,
      'phone' 		=> $address->phone,
      'zone'			=> $address->zone,
      'point'			=> $address->point,
      'local'			=> $address->local['data'],
      'selected'	=> $address->selected,
      'indatabase'=> true,
      'deleted'		=> false,
		];
	}

}
