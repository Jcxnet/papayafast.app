<?php

namespace App\Http\Transformers;

use App\Local;
use League\Fractal\TransformerAbstract;

class LocalmapTransformer extends TransformerAbstract{

	public function transform(Local $local){
		return [
			'id'	    => $local->id,
      'address' => $local->address['address'],
      'name' 		=> $local->name,
      'point'		=> $local->address['point'],
      'zone'  	=> $this->pairCoordinates($local->zone['green']),
		];
	}


	private function pairCoordinates($points=[]){
    if(count($points)==0 || count($points)<2)
    	return "[]";
    return (collect($points)->toArray());
  }

}
