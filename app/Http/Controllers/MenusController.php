<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class MenusController extends Controller
{
    public function load(Request $request){
    	$address 	= isset($request->address)?$request->address:false;
    	$local 		=	isset($request->local)?$request->local:false;
    	$token 		=	isset($request->token)?$request->token:false;
    	if(!$local){
    		$request->session()->flash('message','No se encontraron locales');
    		return redirect()->route('main.load');
    	}
    	if(!$address){
    		$request->session()->flash('message','Es necesaria una dirección');
    		return redirect()->route('addresses.delivery',['token'=>$token]);
    	}
    	return view('app.menus.load',['address'=>$address,'local'=>$local,'token'=>$token]);
    }

    public function view(Request $request){
   		$name 	=	isset($request->name)?$request->name:false;
   		$menu 	=	isset($request->menu)?$request->menu:false;
   		$token 	=	isset($request->token)?$request->token:null;
   		if(!$menu){
    		$request->session()->flash('message','Verifique la dirección seleccionada');
    		return redirect()->route('main.menu',['token'=>$token]);
    	}
    	return view('app.menus.view',['name'=>$name,'menu'=>$menu,'token'=>$token]);
    }

    public function select(Request $request){
    	if($request->ajax()){
    		$menus 	= isset($request->storage)?$request->storage:false;
    		$id 		=	isset($request->menu)?$request->menu:false;
    		$token 	=	isset($request->token)?$request->token:null;
    		if(!$menus){
    			$request->session()->flash('message','El menú del local no se encuentra disponible.');
    			return $this->ajaxError('El menú del local no se encuentra disponible.');
    		}
    		$menus = $this->getKeysDecrypted(['data'],$menus);
    		if(!$menus){
    			$request->session()->flash('message','El menú del local no se encuentra disponible.');
    			return $this->ajaxError('El menú del local no se encuentra disponible.');
    		}
    		if(!$id){
    			$request->session()->flash('message','El menú seleccionado no se encuentra.');
    			return $this->ajaxError('El menú seleccionado no se encuentra.');
    		}

    		if(array_key_exists('data',$menus)){
    			$menu = [];
    			foreach($menus['data']['versions'] as $item){
    				if($item['id'] == $id){
    					$menu = $menus['data']['menus'][$id];
    					break;
    				}
					}
					return $this->ajaxData(['menu' =>$menu,'url'=>route('menus.view.page',['page'=>$menu['page'],'token'=>$token])]);
    		}else{
    			return $this->ajaxError('No se encontró la dirección');
    		}
    	}else{
 				return $this->ajaxError('Acción no permitida',[],401);
 			}
    }

    public function showPage(Request $request){
    	$page		=	isset($request->page)?$request->page:false;
    	$token 	=	isset($request->token)?$request->token:null;
    	if(!$page){
    		$request->session()->flash('message','No se encontró la página del menú');
    		return redirect()->route('main.menu',['token'=>$token]);
    	}
    	$this->setUserToken(['user'=> '','token' =>$token]);
    	return view('app.menus.pageload',['page'=>$page,'token'=>$token,'body_class'=>'body-bg-white']);
    }

    public function menuPageHtml(Request $request){
    	if($request->ajax()){
    		$page		=	isset($request->page)?$request->page:false;
    		$token 	=	isset($request->token)?$request->token:null;
    		if(!$page)
    			return $this->ajaxError('Se necesita la página del menú');
    		$page['image'] 		= array_key_exists('image',$page)?$page['image']:false;
    		$page['products'] = array_key_exists('products',$page)?collect($page['products'])->toJson():false;
    		$page['buttons'] 	= array_key_exists('buttons',$page)?collect($page['buttons'])->toJson():false;
    		$page = view('app.menus.page',['page'=>$page,'token'=>$token]);
    		return $this->ajaxData(['html'=>$page->render()],'html');
   		}else{
   	 		return $this->ajaxError('Acción no permitida',[],401);
  	 	}
    }

    public function menuButtonHtml(Request $request){
  		if($request->ajax()){
  			$buttons	=	isset($request->buttons)?$request->buttons:false;
    		$token 		=	isset($request->token)?$request->token:null;
    		if(!$buttons)
    			return $this->ajaxError('Se necesitan los botones para mostrar');
    		for($i=0;$i<count($buttons);$i++){
    			$buttons[$i]['image'] = array_key_exists('image',$buttons[$i])?$buttons[$i]['image']:false;
    		}
    		$buttons = view('app.menus.button',['buttons'=>$buttons,'token'=>$token]);
    		return $this->ajaxData(['html'=>$buttons->render()],'html');
  		}else{
  	 		return $this->ajaxError('Acción no permitida',[],401);
  	 	}
    }

    public function menuProductHtml(Request $request){
  		if($request->ajax()){
  			$products	=	isset($request->products)?$request->products:false;
    		$token 		=	isset($request->token)?$request->token:null;
    		$order 		=	isset($request->order)?$request->order:false;
    		if(!$products || $products == 'false'){
    			//$request->session()->flash('message','No se encontraron productos para mostrar');
    			return $this->ajaxError('Debes agregar productos a tu pedido');
    		}
    		if(!is_array($products))
    			$products = json_decode($products,true);
    		for($i=0;$i<count($products);$i++){
    			$products[$i]['image'] 			= array_key_exists('image',$products[$i])?$products[$i]['image']:false;
    			$products[$i]['properties'] = array_key_exists('properties',$products[$i])?collect($products[$i]['properties'])->toJson():false;
    			$products[$i]['groups'] 		= array_key_exists('groups',$products[$i])?collect($products[$i]['groups'])->toJson():false;
    			$products[$i]['orderid'] 	  = array_key_exists('orderid',$products[$i])?$products[$i]['orderid']:0;
    			if($order){ //show products in actual order
    				$products[$i]['price']  =  $this->productPrice($products[$i]);
    			}
    		}

    		$products = view('app.menus.productnew',['products'=>$products,'token'=>$token,'order'=>$order]);
    		return $this->ajaxData(['html'=>$products->render()],'html');
  		}else{
  	 		return $this->ajaxError('Acción no permitida',[],401);
  	 	}
    }

    public function menuProduct(Request $request){
  		if($request->ajax()){
  			$product	=	isset($request->product)?$request->product:false;
    		$order 		=	isset($request->order)?$request->order:false;
    		if(!$product || $product == 'false'){
    			return $this->ajaxError('No se encontró el producto');
    		}
    		if(!is_array($product))
    			$product = json_decode($product,true);
  			$product['image'] 			= array_key_exists('image',$product)?$product['image']:false;
  			$product['properties'] 	= array_key_exists('properties',$product)?collect($product['properties'])->toJson():false;
  			$product['groups'] 			= array_key_exists('groups',$product)?collect($product['groups'])->toJson():false;
  			$product['orderid'] 	  = array_key_exists('orderid',$product)?$product['orderid']:0;
  			if($order){
  				$product['price']  =  $this->productPrice($product);
  			}

    		$product = view('app.menus.productitem',['product'=>$product,'order'=>$order,'pos'=>$product['orderid']]);
    		return $this->ajaxData(['html'=>$product->render()],'html');
  		}else{
  	 		return $this->ajaxError('Acción no permitida',[],401);
  	 	}
    }

    public function productAvailable(Request $request){
    	if($request->ajax()){
    		if(!$product = json_decode($request->product,true))
    			return $this->ajaxError('No se encontró el producto',[],202);
    		if(!$address = json_decode($request->address,true))
    			return $this->ajaxError('No se encontró la ubicación',[],202);
    		$data = ['product'=>$product['code'],'local'=>$address['local']['id']];
    		$result	= $this->sendData(config('apiendpoints.API_PRODUCT_AVAILABLE'),$data);
    		return $result;
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

}
