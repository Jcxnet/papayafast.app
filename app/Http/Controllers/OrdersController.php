<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class OrdersController extends Controller
{
    public function actual(Request $request){
    	$token 	=	isset($request->token)?$request->token:null;
    	$user = null;
    	if($token){
    		$token 	= $request->token;
    		$user 	= $this->sendData(config('apiendpoints.API_USER_INFO'),[],$this->createTokenHeader($token));
    	}
    	$this->setUserToken(['user'=> $user,'token' =>$token]);
    	return view('app.orders.orderload',['token'=>$token,'body_class'=>'body-bg-white', 'title'=>'Tu pedido','page_title'=>'Tu pedido']);
    }


    public function amount(Request $request){
   		if($request->ajax()){
   	  	$products	=	isset($request->products)?$request->products:false;
    		$token 		=	isset($request->token)?$request->token:null;
    		if(!$products || $products == 'false'){
    			return $this->ajaxError('Debes agregar productos a tu pedido');
    		}
    		if(!is_array($products))
    			$products = json_decode($products,true);
    		$payments = [];
    		$subtotal = 0;
    		foreach ($products as $product) {
    			$subtotal += $this->productPrice($product) * (int) $product['cantidad'];
    		}
    		if($subtotal>0){
    			$online = $this->calculateComission($subtotal);
    			$payments[] = ['name'=>'Sub total','amount' =>$subtotal];
    			$payments[]	= ['name'=>'Comisión online','amount' =>$online];
    			$total			= $subtotal + $online;
    			$amount = view('app.orders.amount',['payments'=>$payments,'total'=>$total,'token'=>$token]);
    			return $this->ajaxData(['html'=>$amount->render(),'total'=>$total],'html');
    		}
    		return $this->ajaxData(['html'=>'&nbsp;'],'html');
   		}else{
   	 		return $this->ajaxError('Acción no permitida',[],401);
   	 	}
    }

    /**
     * verifica si el local puede recibir la orden por el tiempo de atenció y si el servicio de delivery está activo
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    private function canSendOrder($address){
    	if(!is_array($address))
    		$address = json_decode($address,true);
    	if(!array_key_exists('local',$address)){
    		return ['error'=>true,'message'=>'No se encontró la información del local'];
    	}
    	$local = $address['local'];
    	//local is open
    	if(!$open = $this->sendData(config('apiendpoints.API_LOCALS_OPEN'),['id'=>$local['id']])){
    		return ['error'=>true,'message'=>'El local no se encuentra disponible para recibir pedidos.'];
    	}
   		if($open['meta']['status'] == 'ok'){
   			if(!$open['data']['open'])
	   			return ['error'=>true,'message'=>'El local se encuentra cerrado.'];
   		}else{
   			return ['error'=>true,'message'=>$open['data']['message']];
   		}
   		//local is delivery
   		if(!$delivery = $this->sendData(config('apiendpoints.API_LOCALS_DELIVERY'),['id'=>$local['id']]))
   			return ['error'=>true,'message'=>'El local no se encuentra disponible para atender pedidos.'];
   		if($delivery['meta']['status'] == 'ok'){
   			if($delivery['data']['delivery'] != true)
	   			return ['error'=>true,'message'=>'El local no está recibiendo pedidos.'];
   		}else{
   			return ['error'=>true,'message'=>$delivery['data']['message']];
   		}

   		return ['error'=>false];
    }

    public function create(Request $request){
    	if($request->ajax()){
    		if(!$products = json_decode($request->products,true))
    			return $this->ajaxError('No se encontró el producto',[],202);
    		if(!$address = json_decode($request->address,true))
    			return $this->ajaxError('No se encontró la dirección para enviar el pedido',[],202);
    		if(!$delivery = json_decode($request->delivery,true))
    			return $this->ajaxError('No se encontró el menú del local',[],202);
    		if(!$token = $request->token)
    			return $this->ajaxError(sprintf('Debes iniciar sesión<br/><a href="%s" class="button">Iniciar sesión</a>',route('users.login.form')),[],202);

    		$local = $this->canSendOrder($address);
    		if($local['error'])
  				return $this->ajaxError($local['message'],[],202);

    		$list = collect($products)->pluck('code','orderid')->toJson();
    		$data = ['products'=>$list,'local'=>$address['local']['id']];
    		$result	= $this->sendData(config('apiendpoints.API_PRODUCTS_AVAILABLE'),$data);
    		if($result['meta']['status'] == 'ok'){
    			if($result['data']['total']>0){
    				$total = $result['data']['total'];
    				$result['data']['message'] = sprintf("Hay %d %s %s en el local, debe %s de la lista",$total,str_plural('producto',$total),str_plural('agotado',$total),str_plural('eliminarlo',$total));
    			}elseif($result['data']['total']==0){
    				$result	= $this->sendData(config('apiendpoints.API_ORDERS_CREATE'),['data'=>['products'=>$products,'address'=>$address,'delivery'=>$delivery]],$this->createTokenHeader($token));
    				if($result['meta']['status']=='ok'){
    					$result['data']['total'] = 0;
    					$result['data']['url'] = route('checkouts.start',['token'=>$token]);
    				}
    				return $result;
    			}
    		}
    		return $result;
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }


    public function generate(Request $request){
    	if($request->ajax()){
	    	$checkout = isset($request->checkout)?$request->checkout:false;
	    	if(!$checkout)
	    		return $this->ajaxError('No se encontró el identificador del proceso');
	    	$token = isset($request->token)?$request->token:false;
	    	if(!$token)
	    		return $this->ajaxError('Debes iniciar sesión para continuar con el proceso');
	    	$data = collect($request->all())->except('token')->toJson();
	    	return $this->sendData(config('apiendpoints.API_ORDERS_GENERATE'),['data'=>$data],$this->createTokenHeader($token));
			}else{
		 		return $this->ajaxError('Acción no permitida',[],401);
		 	}
    }

    public function resume(Request $request){
    	if($request->ajax()){
	    	$checkout = isset($request->checkout)?$request->checkout:false;
	    	if(!$checkout)
	    		return $this->ajaxError('No se encontró el identificador del proceso');
	    	$token = isset($request->token)?$request->token:false;
	    	if(!$token)
	    		return $this->ajaxError('Debes iniciar sesión para continuar con el proceso');
	    	$data = collect($request->all())->except('token')->toJson();
	    	$result = $this->sendData(config('apiendpoints.API_ORDERS_RESUME'),['data'=>$data],$this->createTokenHeader($token));
	    	if($result['meta']['status']=='ok'){
	    		$result['data'] = view('app.checkouts.resume',['data'=>$result['data']])->render();
	    	}else{
	    		$result['data']= view('app.checkouts.noresume',['token'=>$token])->render();
	    	}
	    	return $result;
			}else{
		 		return $this->ajaxError('Acción no permitida',[],401);
		 	}
    }


    public function send(Request $request){
    	if($request->ajax()){
	    	$checkout = isset($request->checkout)?$request->checkout:false;
	    	if(!$checkout)
	    		return $this->ajaxError('No se encontró el identificador del proceso');
	    	if(!$address = json_decode($request->address,true))
    			return $this->ajaxError('No se encontró la dirección para enviar el pedido',[],202);
	    	$token = isset($request->token)?$request->token:false;
	    	if(!$token)
	    		return $this->ajaxError('Debes iniciar sesión para continuar con el proceso');

	    	$local = $this->canSendOrder($address);
    		if($local['error'])
  				return $this->ajaxError($local['message'],[],202);

	    	$data = collect($request->all())->except('token')->toJson();
	    	return $this->sendData(config('apiendpoints.API_ORDERS_SEND'),['data'=>$data],$this->createTokenHeader($token));
			}else{
		 		return $this->ajaxError('Acción no permitida',[],401);
		 	}
    }

    public function verify(Request $request){
    	if($request->ajax()){
	    	$checkout = isset($request->checkout)?$request->checkout:false;
	    	if(!$checkout)
	    		return $this->ajaxError('No se encontró el identificador del proceso');
	    	$token = isset($request->token)?$request->token:false;
	    	if(!$token)
	    		return $this->ajaxError('Debes iniciar sesión para continuar con el proceso');
	    	$data = collect($request->all())->except('token')->toJson();
	    	$result = $this->sendData(config('apiendpoints.API_ORDERS_VERIFY'),['data'=>$data],$this->createTokenHeader($token));
	    	if($result['meta']['status']=='ok'){
	    		$number = $result['data']['number'];
					$email 	= $result['data']['email'];
					$data 	= ['number'=>$number,'email'=>$email,'token'=>$token];
					$url 		= route('orders.completed',['data'=>$this->encryptData($data)]);
					return $this->ajaxData(['url'=>$url],'ok');
	    	}
	    	return $result;
			}else{
		 		return $this->ajaxError('Acción no permitida',[],401);
		 	}
    }


    public function check(Request $request){
    	if($request->ajax()){
	    	$checkout = isset($request->checkout)?$request->checkout:false;
	    	if(!$checkout)
	    		return $this->ajaxError('No se encontró el identificador del proceso');
	    	$token = isset($request->token)?$request->token:false;
	    	if(!$token)
	    		return $this->ajaxError('Debes iniciar sesión para continuar con el proceso');
	    	$data = collect($request->all())->except('token')->toJson();
	    	$result = $this->sendData(config('apiendpoints.API_ORDERS_PAID'),['data'=>$data],$this->createTokenHeader($token));
    		if($result['meta']['status'] == 'ok'){
    			$number = $result['data']['number'];
					$email 	= $result['data']['email'];
					$data 	= ['number'=>$number,'email'=>$email,'token'=>$token];
					$url 		= route('orders.completed',['data'=>$this->encryptData($data)]);
					return $this->ajaxData(['url'=>$url],'ok');
    		}
    		return $result;
			}else{
		 		return $this->ajaxError('Acción no permitida',[],401);
		 	}
    }

     /**
     * el proceso de pago y envío de la orden finalizado
     * @param  Request $request
     * @return View
     */
    public function completed(Request $request){
    	if(!$data = $request->data){
    		$request->session()->flash('message','No se encontró la información del pedido');
    		return redirect()->route('main.menu');
    	}
    	$data 	= $this->decryptData($data);
    	if(!is_array($data))
    		$data = json_decode($data,true);
    	$number = $data['number'];
      $email  = $data['email'];
      $token  = $data['token'];
      $encoded = $this->encryptData($number);
      $this->setUserToken(['user'=> '','token' =>$token]);
      return view('app.orders.completed',['number'=>$number,'encoded' => $encoded,'email'=>$email,'token'=>$token,'title'=>'Confirmación','page_title'=>'Confirmación','body_class'=>'body-bg-white','back_link'=>route('main.menu',['token'=>$token])]);
    }


    public function confirmation(Request $request){
    	if($request->ajax()){
	      $token = isset($request->token)?$request->token:false;
	    	if(!$token){
	    		$request->session()->flash('message','Debe iniciar sesión para continuar');
	    		return $this->ajaxData([],'login');
	    	}
	      $number  = $this->decryptData($request->number);
	      $result	= $this->sendData(config('apiendpoints.API_ORDERS_CONFIRMATION'),['number'=>$number],$this->createTokenHeader($token));
	      return $result;
	    }else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    public function history(Request $request){
    	$token 	= isset($request->token)?$request->token:false;
    	$page 	= isset($request->page)?$request->page:false;
    	if(!$token){
    		$request->session()->flash('message','Debes iniciar sesión para poder ver tus pedidos');
    		return redirect()->route('users.login.form');
    	}
    	$this->setUserToken(['user'=> '','token' =>$token]);
    	$data=[];
    	if($page)
    		$data = ['page' =>$page];
    	$result	= $this->sendData(config('apiendpoints.API_ORDERS_HISTORY'),$data,$this->createTokenHeader($token),"GET");
    	if($result['meta']['status']!='ok'){
    		$request->session()->flash('message',$result['data']['message']);
    		return redirect()->route('main.menu',['token'=>$token]);
    	}
    	return view('app.orders.history',['orders'=>$result['data'],'paginator'=>$this->paginator($result['meta']['pagination'],$token),'token'=>$token,'title'=>'Mis pedidos','page_title'=>'Mis pedidos','body_class'=>'body-bg-white', 'back_link'=>route('main.menu',['token'=>$token]) ]);
    }

    private function paginator($data,$token){
    	$paginator=[];
    	$paginator['title'] 		= sprintf("Página %d de %d",$data['current_page'],$data['total_pages']);
    	$paginator['first'] 		= false;
    	$paginator['last'] 			= false;
    	$paginator['previous'] 	= false;
    	$paginator['next'] 		 	= false;
    	if(isset($data['links']['previous'])){
    		$paginator['previous'] = route('orders.history',['page'=>$data['current_page']-1,'token'=>$token]);
    		$paginator['first'] 	 = route('orders.history',['page'=>1,'token'=>$token]);
    	}
    	if(isset($data['links']['next'])){
    		$paginator['next'] = route('orders.history',['page'=>$data['current_page']+1,'token'=>$token]);
    		$paginator['last'] = route('orders.history',['page'=>$data['total_pages'],'token'=>$token]);
    	}
    	return $paginator;
    }
    public function detail(Request $request){
    	$token = isset($request->token)?$request->token:false;
    	if(!$token){
    		$request->session()->flash('message','Debes iniciar sesión para poder ver el pedido');
    		return redirect()->route('users.login.form');
    	}

    	$id = isset($request->id)?$request->id:false;
    	if(!$id){
    		$request->session()->flash('message','Hace falta el número de pedido');
    		return redirect()->route('orders.history',['token'=>$token]);
    	}
    	$this->setUserToken(['user'=> '','token' =>$token]);
    	$result	= $this->sendData(config('apiendpoints.API_ORDERS_DETAIL'),['id'=>$id],$this->createTokenHeader($token),"GET");
    	if($result['meta']['status']=='ok'){
    		$order = $result['data'];
    		if(!is_array($result['data']))
    			$order = json_decode($result['data']);
    		$products = $order['products'];
    		if(!is_array($products))
    			$products = json_decode($products,true);
    		$list = [];
    		foreach ($products as $product) {
    			$product['priceorder'] = $this->productPrice($product);
    			$list[] = $product;
    		}
				$payments = [];
  			$payments[] = ['name'=>'Sub total','amount' =>$order['subtotal']];
  			$payments[]	= ['name'=>'Comisión online','amount' =>$order['comision']];
    	}else{
    		$request->session()->flash('message','No se encontró el pedido');
    		return redirect()->route('orders.history',['token'=>$token]);
    	}
    	return view('app.orders.detail',['products'=>$list,'payments'=>$payments,'total'=>$order['total'],'token'=>$token,'time'=>$order['updated']['date'],'title'=>"Pedido {$order['number']}",'page_title'=>"Pedido {$order['number']}",'body_class'=>'body-bg-white', 'back_link'=>route('orders.history',['token'=>$token])]);
    }


  	public function status(Request $request){
  		if($request->ajax()){
	      $token 	= isset($request->token)?$request->token:false;
	      $id 		= isset($request->id)?$request->id:false;
	    	if(!$token){
	    		$request->session()->flash('message','Debe iniciar sesión para continuar');
	    		return $this->ajaxData([],'login');
	    	}
	      if(!$id){
	      	return $this->ajaxError('Hace falta el número de pedido',202);
	    	}
	      $result	= $this->sendData(config('apiendpoints.API_ORDERS_STATUS'),['id'=>$id],$this->createTokenHeader($token));
	      if($result['meta']['status']=='ok'){
	      	$result['data']['status'] = strtolower($result['data']['status']);
	      	unset($result['data']['Descripcion']);
	      	unset($result['data']['Codigoestado']);
	      	$result['data']['removelink'] = false;
	      	if($result['data']['status']=='recibido' || $result['data']['status']=='anulado' || $result['data']['status']=='error datos' || $result['data']['status']=='error conexion' || $result['data']['status']=='no status'){
	      		$result['data']['removelink'] = true;
	      	}
	      	$result['data']['status'] = str_replace(' ','-',$result['data']['status']);
	      }
	      return $result;
	    }else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
  	}
}
