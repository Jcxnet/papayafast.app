<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\User;
use App\Address;

use App\Http\Transformers\AddressTransformer;

use \League\Fractal\Manager;
use \League\Fractal\Resource\Collection as FractalCollection;
use \League\Fractal\Resource\Item as FractalItem;

class MainController extends Controller
{
    public function asklocation(Request $request){
    	$token = isset($request->token)?$request->token:null;
      return view('app.maps.asklocation',['title' => 'Compartir mi ubicación','token'=>$token]);
    }

    public function getMenu(Request $request){
    	$user = null;
    	$token = isset($request->token)?$request->token:null;
    	if($token && $token != 'false'){
    		$this->setUserToken(['user'=> '','token' =>$token]);
    	}else{
    		$token = null;
    	}
    	return view('app.main.menu',['title'=>'Bienvenido','token'=>$token,'body_class'=>'bg-food','page_title'=>'Pardos App','back_link'=>'#']);
    }

    public function islocated(Request $request){
      if($request->ajax()){
        $data  	= $this->getKeysDecrypted(['point'],$request->data);
        $token  = isset($request->token)?$request->token:null;
        if($token && $token != 'false')
        	$token = $this->decryptData($token);
        else
        	$token = null;
        try{
          if(!$data['point']){
            return $this->ajaxData(['url'=>route('main.asklocation',['token'=>$token])],'redirect');
          }
          return $this->ajaxData(['url'=>route('main.menu',['token'=>$token])],'redirect');
        }catch(Exception $e){
          return $this->ajaxError('No se pudo verificar su ubicación');
        }
      }else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    /**
     * Muestra la pantalla de carga de la aplicación
     * @return View
     */
    public function load(Request $request){
      $token = isset($request->token)?$request->token:false;
    	return view('app.main.loader',['token'=>$token]);
    }

    public function loadAddress(Request $request){
    	if($request->ajax()){
    		$data  	= $this->getKeysDecrypted(['data'],$request->data);
        $token  = isset($request->token)?$request->token:false;//$this->getKeysDecrypted(['token'],$request->token);
        if(!$token || $token == 'false')
 					return $this->ajaxData([],'nochange',['message'=>'Lista de direcciones verificada']);
 				$token = $this->decryptData($token);
 				$this->updateUserAddress($data['data'], $token);
 				$data =  $this->getUserAddress($token);
 				if ($data)
 					return $this->ajaxData(['data'=>$this->encryptData(['data' => $data])],'update',['message' => 'Lista de direcciones actualizada']);
 				else
 					return $this->ajaxData([],'nochange',['message'=>'Lista de direcciones verificada']);
			}else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    /**
     * Verifica la versión de la App
     * @param  Request $request
     * @return json
     */
    public function loadApp(Request $request){
 			if($request->ajax()){
 				$data			  = $this->getKeysDecrypted(['version'],$request->data);
 				$result 	  = $this->checkAppVersion( $data['version']);
 				if(!$result)
 					return $this->ajaxData([],'nochange',['message' => 'La aplicación se encuentra actualizada']);
 				$data = $this->encryptData(['version' => $data['version'],'name' => $result['name'], 'updated' => false ]);
 				return $this->ajaxData(['data'=>$data], 'update',['message'=>"Está disponible la versión <b>{$result['version']}</b> de la aplicación"]);
			}else{
 				return $this->ajaxError('Acción no permitida',[],401);
 			}
    }

    /**
     * verifica si hay cambios en la lista de locales
     * @param  Request $request
     * @return json
     */
    public function loadLocals(Request $request){
 			if($request->ajax()){
 				$locals	= $this->getKeysDecrypted(['version','data','total'],$request->data);
 				$result	= $this->getLocalsInfo($locals['version']);
 				if(!$result)
 					return $this->ajaxData([],'nochange',['message' => 'La lista de locales se encuentra actualizada']);
 				$data = $this->encryptData([ 'data' =>$result['data'], 'version' => $result['version'], 'total'=>$result['total'] ]);
 				return $this->ajaxData(['data'=>$data], 'update',['message'=>"Se actualizó la lista con {$result['total']} locales"]);
			}else{
 				return $this->ajaxError('Acción no permitida',[],401);
 			}
    }

    public function loadMenus(Request $request){
 			if($request->ajax()){
 				$menus	= $this->getKeysDecrypted(['data'],$request->data); //menus collection
 				$result	= $this->getMenusInfo($this->menuVersions($menus['data']['versions']));
 				if(!$result)
 					return $this->ajaxData([],'nochange',['message' => 'La lista de menús se encuentra actualizada']);
 				$data = $this->menusUpdate($menus['data']['menus'], $result);
 				$data = $this->encryptData([ 'data' => $data ]);
 				return $this->ajaxData(['data'=>$data], 'update',['message'=>"Se actualizó la lista de menús "]);
			}else{
 				return $this->ajaxError('Acción no permitida',[],401);
 			}
    }

    /**
     * Verifica y actualiza información del usuario
     * @return json
     */
    public function loadUser(Request $request){
 			if($request->ajax()){
 				$token = isset($request->data)?$request->data:false;
 				if(!$token || $token == 'false')
 					return $this->ajaxData([],'nochange',['message'=>'Su información se encuentra actualizada']);
 				$token 	= $this->decryptData($token);
 				try{
 					$result = $this->sendData( config('apiendpoints.API_USER_INFO'), [], $this->createTokenHeader($token) );
 					switch(strtolower($result['meta']['status'])){
 						case 'ok'			: $request->session()->flash('message',"Bienvenido de nuevo <b>{$result['data']['name']}</b>");
 														return $this->ajaxData([],'nochange',['message'=>'Su información se encuentra actualizada']);
 														break;
 						case 'error'	: return $this->ajaxData( [], 'remove', ['message'=>'Sin información de usuario']);
 														break;
 						default: return $this->ajaxError('Se encontró un error',$result['data']);
 					}
 				}catch(Exception $e){
 					return $this->ajaxError('No se pudo recuperar información del usuario');
 				}
 			}else{
 				return $this->ajaxError('Acción no permitida',[],401);
 			}

    }

    protected function checkAppVersion($version){
    	$result = $this->sendData( config('apiendpoints.API_APP_VERSION'),['version'=>$version]);
    	if($result['meta']['status'] == 'update'){
    		return $result['data'];
    	}
    	return false;
    }

    protected function getLocalsInfo($version){
    	$result = $this->sendData( config('apiendpoints.API_LOCALS_LIST'),['version'=>$version]);
    	switch(strtolower($result['meta']['status'])){
    		case 'update': $result['version'] = $result['meta']['version'];
    									 $result['total'] 	= $result['meta']['total'];
    									return $result;
    	}
    	return false;
    }

    protected function getMenusInfo($menus){
    	$result = $this->sendData(config('apiendpoints.API_MENUS_LIST'),['menus'=>$menus]);

    	switch (strtolower($result['meta']['status'])) {
    		case 'update': return $result['data'];
    		case 'nochange': return false;
    	}
    	return false;
    }

    protected function menuVersions($versions){
    	if (!$versions || $versions == '')
    		return [];
    	return $versions;
    }

    protected function menusUpdate($menus, $data){
    	$ids = [];
    	if(!$menus || count($menus)==0)
    		$menus = [];

    	foreach($menus as $menu)
    		$menus[$menu['id']]['status'] = 'remove';

    	foreach($data as $key => $menu){
    		$ids[] = collect($menu)->only(['version','id'])->toArray();
    		switch ($menu['action']){
    			case 'update': case 'new' : $menus[$menu['id']] = $menu; $menus[$menu['id']]['status'] = 'updated'; break;
    			case 'nochange': $menus[$menu['id']]['status'] = 'updated'; break;
    		}
    	}
    	$data = ['versions'=>$ids, 'menus' =>''];
    	foreach ($menus as $menu) {
    		if($menu['status'] != 'remove')
    			$data['menus'][$menu['id']] = $menu;
    	}

    	return $data;
    }

    private function getUserAddress($token){
    	$result = $this->sendData(config('apiendpoints.API_ADDRESSES_USER'),[],$this->createTokenHeader($token),"GET");
    	$addresses = [];
    	if($result['meta']['status'] == 'ok'){
    		$list = $this->prepareLocalAddress($result['data']);
	    	return $list['data'];
	    }elseif($result['meta']['status'] == 'error'){
	    	return false;
	    }
    }

    private function prepareLocalAddress($addresses){
      $list = new \Illuminate\Database\Eloquent\Collection;
      foreach ($addresses as $address) {
        $list->push(new Address($address));
      }
      $fractal  = new Manager();
      $resource = new FractalCollection( $list, new AddressTransformer );
      return $fractal->createData($resource)->toArray();
    }

    private function saveAddress($address,$token=false){
    	$data = [	'name'		=>	$address['name'],
    						'address'	=>	$address['address'],
    						'phone'		=>	preg_replace('/\\(|\\)|\\-|\\+|\\s+/','',$address['phone']),
    						'lat'			=> 	$address['point']['lat'],
    						'lng'			=> 	$address['point']['lng'],
    						'zone'		=>	$address['zone'],
    						'local_id'=> 	$address['local']['id'],
    						'selected'=>	$address['selected'],
    					];
    	if(!$address['deleted']){
    		if($address['indatabase']){
    			$data['id']  = $address['id'];
    			$result = $this->sendData(config('apiendpoints.API_ADDRESSES_UPDATE'),$data,$this->createTokenHeader($token),"PUT"); //update address
    		}else
    			$result = $this->sendData(config('apiendpoints.API_ADDRESSES_ADD'),$data,$this->createTokenHeader($token)); //new address
    	}else{
    		$data = ['id'=>$address['id']];
    		$result = $this->sendData(config('apiendpoints.API_ADDRESSES_REMOVE'),$data,$this->createTokenHeader($token),"DELETE"); //remove address
    	}
    }

    private function updateUserAddress($datalocal, $token){
    	if(count($datalocal)>0){
    		foreach ($datalocal as $address) { //address from storage
    			$this->saveAddress($address, $token);
    		}
    	}
    }

}
