<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Local;

use App\Http\Transformers\LocalTransformer;
use App\Http\Transformers\LocalmapTransformer;
use App\Http\Transformers\LocalinfoTransformer;

use \League\Fractal\Manager;
use \League\Fractal\Resource\Collection as FractalCollection;
use \League\Fractal\Resource\Item as FractalItem;

class LocalsController extends Controller
{
    public function all(Request $request){
    	$token = isset($request->token)?$request->token:null;
    	$this->setUserToken(['user'=> '','token' =>$token]);
    	return view('app.locals.list',['token'=>$token,'title'=>'Nuestros locales','page_title'=>'Nuestros locales','body_class'=>'body-bg-white', 'back_link'=>route('main.menu') ]);
    }

    public function allList(Request $request){
      if($request->ajax()){
        try{
          $locals = $request->data;
        }catch(Exception $e){
          return $this->ajaxError('No se pudo encontrar la lista de locales');
        }
        $token  = isset($request->token)?$request->token:false;
        $favorites = [];
        if($token){
        	$favorites = $this->sendData( config('apiendpoints.API_USER_LOCALS'), [], $this->createTokenHeader($token) );
        	if($favorites['meta']['status']=='ok'){
        		$favorites = $favorites['data'];
        	}
        }
        $locals = $this->getKeysDecrypted(['data'],$locals);
        $locals = $locals['data'];
        //$locals = $this->localsOpenTime($locals);
        $locals = $this->dataHtml($locals);
        $locals = $this->orderLocals($locals['data'],$favorites);
        $list   = view('app.locals.listhtml',['locals'=>$locals,'favorites'=>$favorites,'token'=>$token]);
        return $this->ajaxData(['html'=>$list->render(),'locals'=>$locals,'favs'=>$favorites],'show');
      }else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    private function orderLocals($locals, $favorites){
    	if(!is_array($favorites) || count($favorites)==0)
    		return $locals;
    	$first = []; $last =[];
    	foreach ($locals as $local) {
    		if(in_array($local['id'],$favorites)){
    			$first[] = $local;
    		}else{
    			$last[] = $local;
    		}
    	}
    	return array_merge($first,$last);
    }

    private function dataHtml($locals){
      $list = new \Illuminate\Database\Eloquent\Collection;
      foreach ($locals as $local) {
        $list->push(new Local($local));
      }
      $fractal  = new Manager();
      $resource = new FractalCollection( $list, new LocalTransformer );
      return $fractal->createData($resource)->toArray();
    }

    public function allMap(Request $request){
      if($request->ajax()){
        try{
          $locals   = $request->locals;
          $position = $request->point;
        }catch(Exception $e){
          return $this->ajaxError('No se pudo encontrar la lista de locales');
        }
        $locals = $this->getKeysDecrypted(['data'],$locals);
        $locals = $locals['data'];
        $position = $this->getKeysDecrypted(['point'],$position);
        $position = $position['point'];

        $locals = $this->dataMap($locals);
        return $this->ajaxData(['locals'=>$locals,'point'=>$position],'ok');
      }else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    private function dataMap($locals){
      $list = new \Illuminate\Database\Eloquent\Collection;
      if(count($locals)==0)
      	return [];
      foreach ($locals as $local) {
        $list->push(new Local($local));
      }
      $fractal  = new Manager();
      $resource = new FractalCollection( $list, new LocalmapTransformer );
      $resource = $fractal->createData($resource)->toArray();
      return $resource['data'];
    }

    /*protected function localsOpenTime($locals){
      $list = [];
      if(count($locals)== 0)
      	return $list;
      foreach($locals as $local){
    		//$list[] = $this->localOpenTime($local);
      }
      return $list;
    }*/

    /*private function localOpenTime($local){
      $year = date('Y');
      $month = date('n');
      $day = date('w');
      $nday = date('j');
      $hour = date('G');
      $minute = date('i');

      $thistime = strtotime("$year-$month-$nday $hour:$minute:00");

      $local['open'] = false;
      if(count($local['timetable'])>0){
        $local['status'] = "Cerrado. Horario de hoy {$local['timetable'][$day][$day]}: {$local['timetable'][$day]['open']} a {$local['timetable'][$day]['close']}";
        $opentime = strtotime("$year-$month-$nday {$local['timetable'][$day]['open']}:00");
        $nextday = false;
        $closehour = (integer) $local['timetable'][$day]['close'];
        if( $closehour < 12 ){
          $nnday = $nday+1; $nextday = true;
          $closetime = strtotime("$year-$month-$nnday {$local['timetable'][$day]['close']}:00");
        }else{
          $closetime = strtotime("$year-$month-$nday {$local['timetable'][$day]['close']}:00");
        }

        if($thistime >= $opentime && $thistime <= $closetime){
            if($nextday){
              $dday = ($day+1>6)?0:$day+1;
              $nextday = $local['timetable'][$dday][$dday];
              $local['status']  = "Abierto hasta las {$local['timetable'][$day]['close']} del día $nextday";
            }
            else{
              $local['status']  = "Abierto hasta las {$local['timetable'][$day]['close']}";
            }
            $local['open']    = true;
        }
      }else{
        $local['status'] = "Sin información del horario";
      }
      return $local;
    }*/

    public function show(Request $request){
      $token 	= isset($request->token)?$request->token:null;
      try{
        $id = $request->id;
        if($token){
        	$this->setUserToken(['user'=>'','token'=>$token]);
        }
        return view('app.locals.localview',['id'=>$id,'body_class'=>'body-bg-white', 'back_link'=>route('locals.view',['token'=>$token])]);
      }catch(Exception $e){
        return $this->ajaxError('No se pudo encontrar la información del local');
      }
    }

    public function localInfo(Request $request){
      if($request->ajax()){
        try{
          $locals = $request->locals;
          $id     = $request->id;
        }catch(Exception $e){
          return $this->ajaxError('No se pudo encontrar la información de local');
        }
        $locals = $this->getKeysDecrypted(['data'],$locals);
        $local  = collect($locals['data'])->whereIn('id',[$id])->first();
        if(count($local)>0){
          //$local  = $this->localOpenTime($local);
          $local  = $this->dataInfo($local);
          $info   = view('app.locals.localhtml',['local'=>$local['data']]);
          return $this->ajaxData(['html'=>$info->render()],'html');
        }else{
          return $this->ajaxError('No se encontró información del local');
        }
      }else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    public function openDelivery(Request $request){
    	if($request->ajax()){
    		$locals = isset($request->storage)?$request->storage:false;
    		$id 		= isset($request->id)?$request->id:false;
    		$token	= isset($request->token)?$request->token:null;
    		if(!$locals){
    			$request->session()->flash('message','No se encontró información de locales');
    			return $this->ajaxError('No se encontró información de locales',['url'=>route('main.load')]);
    		}
    		if(!$id){
    			$request->session()->flash('message','Hace falta el identificador del local');
    			return $this->ajaxError('Hace falta el identificador del local',['url'=>route('main.menu',['token'=>$token])]);
    		}
    		$locals = $this->getKeysDecrypted(['data'],$locals);
    		$local  = collect($locals['data'])->whereIn('id',[$id])->first();
    		if(count($local)>0){
    			//$local = $this->localOpenTime($local);
    			if(!$open = $this->sendData(config('apiendpoints.API_LOCALS_OPEN'),['id'=>$local['id']]))
	    			return $this->ajaxError('No se pudo determinar el estado del local',[],202);
	    		$local['open'] = $open['data']['open'];
    			$local = $this->dataDelivery($local);
    			$url = route('menus.view',['name'=>$local['name'],'menu'=>$local['menu']['delivery'],'token'=>$token]);
    			if($local['menu']['delivery'] == '' || $local['menu']['delivery'] == null)
    				$url = route('locals.nodelivery',['name'=>$local['name'],'token'=>$token]);
    			if(!$local['open'])
    				$url = route('locals.timeout',['name'=>$local['name'],'time'=>$local['status'],'menu'=>$local['menu']['delivery'],'token'=>$token]);
    			return $this->ajaxData(['delivery'=>$local['menu']['delivery'],'url'=>$url]);
    		}else{
    			$request->session()->flash('message','No se encontró información del local');
    			return $this->ajaxError('No se encontró información del local',['url'=>route('main.load')]);
    		}
			}else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    public function nodelivery(Request $request){
    	$name = isset($request->name)?$request->name:false;
    	$token	= isset($request->token)?$request->token:false;
    	return view('app.locals.nodelivery',['name'=>$name,'token'=>$token]);
    }

    public function timeout(Request $request){
    	$name = isset($request->name)?$request->name:false;
    	$time = isset($request->time)?$request->time:false;
    	$menu = isset($request->menu)?$request->menu:false;
    	$token	= isset($request->token)?$request->token:false;
    	return view('app.locals.timeout',['name'=>$name,'time'=>$time,'menu'=>$menu,'token'=>$token]);
    }

    private function dataInfo($local){
      $local = new Local($local);
      $fractal  = new Manager();
      $resource = new FractalItem( $local, new LocalinfoTransformer );
      return $fractal->createData($resource)->toArray();
    }

    private function dataDelivery($local){
    	$local = new Local($local);
      $fractal  = new Manager();
      $resource = new FractalItem( $local, new LocalTransformer );
      $resource = $fractal->createData($resource)->toArray();
      $resource = $resource['data'];
      $list = ['delivery'=>'','takein'=>'','takeout'=>''];
      foreach ($resource['menu'] as $key => $item) {
      	foreach($item as $name => $value)
      		$list[$name] = $value;
      }
      $resource['menu'] = $list;
      return $resource;
    }

    public function favorite(Request $request){
    	if($request->ajax()){
        try{
          $token 	= $request->token;
          $id  		= $request->local;
          $result = $this->sendData( config('apiendpoints.API_LOCALS_FAVORITE'), ['id'=>$id], $this->createTokenHeader($token) );
          return $result;
        }catch(Exception $e){
          return $this->ajaxError('No se pudo encontrar el local');
        }
			}else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    public function user(Request $request){
    	$token 	= isset($request->token)?$request->token:false;
    	if(!$token){
    		$request->session()->flash('message','Debes iniciar sesión para ver tus locales favoritos');
    		return redirect()->route('users.login.form');
    	}
    	$result = $this->sendData( config('apiendpoints.API_LOCALS_USER'), [], $this->createTokenHeader($token) );
    	$locals = [];
    	if($result['meta']['status']=='ok'){
    		$locals = $result['data'];
    		//$locals = $this->localsOpenTime($locals);
    	}
    	return view('app.locals.favorites',['locals'=>$locals,'token'=>$token,'title'=>'Mis locales favoritos','page_title'=>'Mis locales favoritos','body_class'=>'body-bg-white']);
    }

    public function route(Request $request){
    	$token 	= isset($request->token)?$request->token:null;
    	$lat	= isset($request->lat)?$request->lat:false;
    	$lng	= isset($request->lng)?$request->lng:false;
    	$name	= isset($request->name)?$request->name:'Local';
    	if(!$lat && !$lng){
    		$request->session()->flash('message','No se encontró la ubicación del local');
    		return redirect()->route('main.menu',['token'=>$token]);
    	}
    	return view('app.locals.route',['lat'=>$lat,'lng'=>$lng,'token'=>$token,'page_title'=>"Como llegar a <strong>$name</strong>",'name'=>$name,'body_class'=>'bg-white']);
    }

}
