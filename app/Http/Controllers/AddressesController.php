<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Address;
use App\Http\Transformers\AddressTransformer;

use \League\Fractal\Manager;
use \League\Fractal\Resource\Collection as FractalCollection;
use \League\Fractal\Resource\Item as FractalItem;

use Validator;

class AddressesController extends Controller
{
    public function getAll(Request $request){
    	$user = null;
    	$token = isset($request->token)?$request->token:null;
    	/*if($request->token!="false" && $request->token != null){
    		$token 	= $request->token;
    		$user 	= $this->sendData(env('API_USER_INFO'),[],$this->createTokenHeader($token));
    	}*/
    	$this->setUserToken(['user'=> '','token' =>$token]);
    	return view('app.addresses.list',['title'=>'Mis direcciones','page_title'=>'Mis direcciones','body_class'=>'body-bg-white','delivery'=>false]);
    }

    public function postAll(Request $request){
    	if($request->ajax()){
        $data  	= $this->getKeysDecrypted(['data'],$request->data);
        $delivery = $request->delivery;
        $token 		= isset($request->token)?$request->token:null;

	    	$total = collect($data['data'])->values()->where('deleted',false)->count();
	    	if($total>0){
	    		$addresses = collect($data['data'])->toArray();
	    		$list = view('app.addresses.listitem',['addresses'=>$addresses,'token'=>$token,'delivery'=>$delivery]);
	    		return $this->ajaxData(['html' => $list->render() ,'token' => $token],'html');
	    	}elseif($delivery){
	    		$request->session()->flash('message','Debe agregar una dirección para poder hacer un pedido');
	    		return $this->ajaxData(['url' => route('addresses.add.form',['token' => $token]) ],'redirect');
	    	}
			}else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    public function delivery(Request $request){
    	$user = null;
    	$token =isset($request->token)?$request->token:null;
    	if($token){
    		$user	= $this->sendData(config('apiendpoints.API_USER_INFO'),[],$this->createTokenHeader($token));
    	}
    	$this->setUserToken(['user'=> $user,'token' =>$token]);
    	return view('app.addresses.list',['title'=>'Elegir dirección','page_title'=>'Elegir dirección','body_class'=>'body-bg-white','delivery'=>true]);
    }

    public function getCreate(Request $request){
    	$address 	= null;
    	$token 		= isset($request->token)?$request->token:null;
    	$id 			= isset($request->id)?$request->id:null;
    	$title 		= ($id)?'Modificar dirección':'Añadir dirección';
    	$this->setUserToken(['user'=> '','token' =>$token]);
    	return view('app.addresses.add',['title'=>$title,'page_title'=>$title,'body_class'=>'body-bg-white','id'=>$id,'token'=>$token,'address'=>$address]);
    }

    public function postCreate(Request $request){
    	$data = $request->except(['token','storage']);
    	$valid = $this->isDataValid($data);
    	if($valid===true){
    		$result	= $this->sendData(config('apiendpoints.API_ADDRESSES_COVER'),['lat'=>$data['txtlat'],'lng'=>$data['txtlng']]);
	    	switch($result['meta']['status']){
	    		case 'covered':  $data['zone'] = $result['data']['zone'];
	    										 $data['local_id'] = $result['data']['id'];
	    										 break;
	    		case 'nearest': return $this->ajaxData(['locals'=>view('app.locals.listaddress',['locals'=>$result['data']])->render()],'nocover');
	    		default:  return $result;
	    	}
	    	$token = ($request->token)?$request->token:null;
	    	//save address in localstorage
	    	$addresses = $this->getKeysDecrypted(['data'],$request->storage);
	    	if(!$addresses)
	    		$max = 0;
	    	elseif(array_key_exists('data',$addresses))
	    		$max = count($addresses['data']);
	    	$newAddress = $this->storageAddress($data,$result['data'], $max);
	    	if($token){ //save address in database
	    		$data['lat'] = $data['txtlat']; $data['lng'] = $data['txtlng'];
	    		if($data['id'] != ''){
	    			$result = $this->sendData(config('apiendpoints.API_ADDRESSES_UPDATE'),$data,$this->createTokenHeader($token),"PUT");
	    		}else{
	    			$result = $this->sendData(config('apiendpoints.API_ADDRESSES_ADD'),$data,$this->createTokenHeader($token));
	    		}
	    		if($result['meta']['status']=='ok'){
	    			$newAddress = new Address($result['data']);
		    		$fractal  	= new Manager();
		      	$resource 	= new FractalItem( $newAddress, new AddressTransformer );
		      	$resource 	= $fractal->createData($resource)->toArray();
		      	$newAddress = $resource['data'];
	    		}
	    	}
	    	if ($data['id']!=''){
	    		$addresses = ['data' => $this->updateAddressStorage($addresses['data'],$newAddress)];
	    		$request->session()->flash('message',"Se actualizó la dirección<br/><b>{$data['address']}</b>");
	    	}else{
					$request->session()->flash('message',"Se agregó la dirección<br/><b>{$data['address']}</b>");
	    		$addresses['data'][] = $newAddress;
	    	}
    		return $this->ajaxData(['storage'=>['data'=>$this->encryptData($addresses)],'url'=>route('addresses.view',['token'=>$token])],'storage');
	   	}else{
    		return $this->ajaxError($valid);
    	}
    }


		public function delete(Request $request){
    	if($request->ajax()){
	    	$id = ($request->id)?$request->id:false;
	    	if($id){
	    		$token 			= isset($request->token)?$request->token:false;
		    	$addresses 	= $this->getKeysDecrypted(['data'],$request->storage);
		    	if($token){ //delete address in database
		    		$result = $this->sendData(config('apiendpoints.API_ADDRESSES_REMOVE'),['id'=>$id],$this->createTokenHeader($token),"DELETE");
		    		if($result['meta']['status']!='ok'){
		    			return $this->ajaxError($result['data']['message']);
		    		}
		    	}
		    	$list = ['data'=>[]];
		    	foreach($addresses['data'] as $address){
	    			if($address['id'] == $id)
	    				$address['deleted'] = true;
	    			$list['data'][] = $address;
	    		}
		    	return $this->ajaxData(['storage'=>['data'=>$this->encryptData($list)]]);
		   	}else{
	    		return $this->ajaxError('No se encontró la dirección');
	    	}
	    }else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    private function updateAddressStorage($addresses, $address){
    	$list = [];
    	if(count($addresses)>0){
    		foreach ($addresses as $item) {
    			if($item['id'] == $address['id']){
    				$address['indatabase'] 	= $item['indatabase'];
    				$address['selected'] 		= $item['selected'];
    				$item = $address;
    			}
    			$list[] = $item;
    		}
    	}
    	return $list;
    }

    private function isDataValid($data){
     	$validator = Validator::make($data, [
	      'address'	=> 'required',
	      'name'   	=> 'required|string|min:4|max:100',
	      'phone'		=> 'required|numeric',
	      'txtlat' 	=> 'required|numeric',
	      'txtlng' 	=> 'required|numeric',
    	]);
    	if($validator->fails())
        return implode('<br>',$validator->errors()->all());
	    return true;
    }

    private function storageAddress($data, $local, $pos){
    	$address = [];
    	$address['id'] 			= ($data['id']!='')?$data['id']:++$pos;
    	$address['name']		= $data['name'];
    	$address['address']	= $data['address'];
    	$address['phone']		= $data['phone'];
    	$address['zone']		= $local['zone'];
    	$address['point']		= ['lat' => $data['txtlat'], 'lng' => $data['txtlng']];
    	$address['local']		= ['id' => $local['id'], 'name'=>$local['name'],'point'=>$local['point']];
    	$address['indatabase'] = false;
    	$address['selected'] 	 = false;
    	$address['deleted'] 	 = false;
    	return $address;
    }

    public function inuse(Request $request){
    	if($request->ajax()){
	    	$addresses = $this->getKeysDecrypted(['data'],$request->storage);
	    	$id = isset($request->id)?$request->id:false;
	    	if(!$id)
	    		return $this->ajaxError('Debe seleccionar una dirección',[],401);
	    	if(!$addresses)
	    		return $this->ajaxData([]);
	    	if(array_key_exists('data',$addresses)){
	    		foreach($addresses['data'] as $address){
	    			if($address['id']==$id && $address['selected'])
	    				return $this->ajaxData([],'inuse');
	    		}
	    	}
	    	return $this->ajaxData([]);
	    }else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    public function info(Request $request){
    	if($request->ajax()){
    		$addresses = $this->getKeysDecrypted(['data'],$request->data);
    		$id = $request->id;
	    	if(!$addresses || !$id)
	    		return $this->ajaxError('No hay direcciones');
	    	if(array_key_exists('data',$addresses)){
	    		foreach($addresses['data'] as $address){
	    			if($address['id'] == $id){
	    				$address['phone'] = str_replace('-', '', $address['phone']);
	    				$address['phone'] = str_replace('(', '', $address['phone']);
	    				$address['phone'] = str_replace(')', '', $address['phone']);
	    				$address['phone'] = str_replace(' ', '', $address['phone']);
	    				return $this->ajaxData($address);
	    			}
	    		}
	    	}
	    	return $this->ajaxError('No se encontró la dirección');
			}else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    public function select(Request $request){
    	if($request->ajax()){
	    	$id 				= isset($request->id)?$request->id:false;
	    	$addresses 	=	isset($request->storage)?$request->storage:false;
	    	if ($id && $addresses){
	    		$addresses = $this->getKeysDecrypted(['data'],$addresses);
	    		if(array_key_exists('data',$addresses)){
	    			$list = []; $data = [];
	    			foreach($addresses['data'] as $address){
	    				$address['selected'] == false;
	    				if($address['id'] == $id){
	    					$address['selected'] = true;
	    					$data = $address;
	    				}
	    				$list['data'][] = $address;
						}
						return $this->ajaxData(['storage'=>['data'=>$this->encryptData($list)],'address' =>$data]);
	    		}else{
	    			return $this->ajaxError('No se encontró la dirección');
	    		}
	    	}else{
	    		return $this->ajaxError('No se encontró la dirección');
	    	}
	    }else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    public function order(Request $request){
  		if($request->ajax()){
  			$token 		=	isset($request->token)?$request->token:null;
  			$address 	=	isset($request->address)?$request->address:false;
  			$page 		=	isset($request->page)?$request->page:false;

  			if(!$address){
  				$address = view('app.addresses.noaddress',['token'=>$token,'page'=>0]);
  				return $this->ajaxError('Debes seleccionar una dirección',['html'=>$address->render()]);
  			}
  			if(!$page){
  				$address = view('app.addresses.noaddress',['token'=>$token,'page'=>0]);
  				return $this->ajaxError('No se encontró el menú para el local',['html'=>$address->render()]);
  			}
  			if(!is_array($address))
  				$address = json_decode($address,true);
  			if(count($address)==0){
  				$address = view('app.addresses.noaddress',['token'=>$token,'page'=>$page]);
  				return $this->ajaxError('Debes seleccionar una dirección',['html'=>$address->render()]);
  			}
  			$address = view('app.addresses.order',['address'=>$address,'token'=>$token,'page'=>$page]);
  			return $this->ajaxData(['html'=>$address->render()],'html');
  		}else{
  	 		return $this->ajaxError('Acción no permitida',[],401);
  	 	}
    }

    public function release(Request $request){
    	if($request->ajax()){
        $data  	= $this->getKeysDecrypted(['data'],$request->storage);
        $token  = isset($request->token)?$request->token:null;
	    	$total = collect($data['data'])->values()->where('deleted',false)->count();
	    	if($total>0){
	    		$addresses = collect($data['data'])->toArray();
	    		$list = [];
	    		foreach ($addresses as $addres) {
	    			$addres['selected'] = false;
	    			$list['data'][] = $address;
	    		}
	    		return $this->ajaxData(['storage' =>['data'=>$this->encryptData($list)]],'ok');
	    	}
			}else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }
}
