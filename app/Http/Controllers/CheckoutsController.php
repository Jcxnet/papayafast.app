<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


class CheckoutsController extends Controller{

	public function start(Request $request){
		$token = isset($request->token)?$request->token:null;
		return view('app.checkouts.start',['token'=>$token,'title'=>'Enviar pedido','page_title'=>'Enviar pedido <ons-toolbar-button id="cart-button-show"><span class="notification toolbar-notification" id="cart-products-total">0</span><i class="fa fa-cutlery"></i></ons-toolbar-button>','body_class'=>'body-bg-white']);
	}

	public function billing(Request $request){
		if($request->ajax()){
			$billing = isset($request->billing)?$request->billing:false;
  		if(!$billing)
    		return $this->ajaxError('No se encontraron los datos de envío, intente nuevamente');
    	$checkout = isset($request->checkout)?$request->checkout:false;
    	if(!$checkout)
    		return $this->ajaxError('No se encontró el identificador del proceso');
    	$token = isset($request->token)?$request->token:false;
    	if(!$token)
    		return $this->ajaxError('Debes iniciar sesión para continuar con el proceso');
    	$data = collect($request->all())->except('token')->toJson();
    	return $this->sendData(config('apiendpoints.API_CHECKOUT_BILLING'),['data'=>$data],$this->createTokenHeader($token));
		}else{
	 		return $this->ajaxError('Acción no permitida',[],401);
	 	}
	}

	public function payment(Request $request){
		if($request->ajax()){
			$payment = isset($request->payment)?$request->payment:false;
  		if(!$payment)
    		return $this->ajaxError('No se encontraron los datos del método de pago, intente nuevamente');
    	$checkout = isset($request->checkout)?$request->checkout:false;
    	if(!$checkout)
    		return $this->ajaxError('No se encontró el identificador del proceso');
    	$token = isset($request->token)?$request->token:false;
    	if(!$token)
    		return $this->ajaxError('Debes iniciar sesión para continuar con el proceso');
    	$data = collect($request->all())->except('token')->toJson();
    	return $this->sendData(config('apiendpoints.API_CHECKOUT_PAYMENT'),['data'=>$data],$this->createTokenHeader($token));
		}else{
	 		return $this->ajaxError('Acción no permitida',[],401);
	 	}
	}

	public function card(Request $request){
		if($request->ajax()){
    	$checkout = isset($request->checkout)?$request->checkout:false;
    	if(!$checkout)
    		return $this->ajaxError('No se encontró el identificador del proceso');
    	$token = isset($request->token)?$request->token:false;
    	if(!$token)
    		return $this->ajaxError('Debes iniciar sesión para continuar con el proceso');
    	$data = collect($request->all())->except('token')->toJson();
    	return $this->sendData(config('apiendpoints.API_CREDITCARD_CHARGE'),['data'=>$data],$this->createTokenHeader($token));
		}else{
	 		return $this->ajaxError('Acción no permitida',[],401);
	 	}
	}

}
