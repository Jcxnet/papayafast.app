<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PaymentsController extends Controller
{
    public function method(Request $request){
    	$token = isset($request->token)?$request->token:false;
    	if(!$token){
    		$request->session()->flash('message','Debes iniciar sesión para continuar con el pago de tu pedido');
    		return redirect()->route('users.login');
    	}

    	$this->setUserToken(['user'=> '','token' =>$token]);
    	return view('app.payments.methods',[
    	    'title'=>'Pagar pedido',
    	    'page_title'=>'Pagar pedido',
    	    'body_class'=>'body-bg-white']);
    }

    public function validateMethod(Request $request){
    	if($request->ajax()){
    		$data 	= isset($request->data)?$request->data:false;
    		if(!$data)
	    		return $this->ajaxError('No se encontró la información del método de pago, intente nuevamente');
	    	$token = $data['token'];
	    	if(!$token){
	    		$request->session()->flash('message','Debes iniciar sesión para continuar con el pago de tu pedido');
	    		return $this->ajaxData([],'login');
	    	}
	    	$data = collect($data)->except('token')->toJson();
	    	$result	= $this->sendData(config('apiendpoints.API_PAYMENTS_VALIDATE'),['data'=>$data],$this->createTokenHeader($token));
	    	return $result;
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

		public function generate(Request $request){
    	if($request->ajax()){
    		$data 	= isset($request->data)?$request->data:false;
    		if(!$data)
	    		return $this->ajaxError('No se encontró la información del método de pago, intente nuevamente');
	    	$token = isset($request->token)?$request->token:false;
	    	if(!$token){
	    		$request->session()->flash('message','Debes iniciar sesión para continuar con el pago de tu pedido');
	    		return $this->ajaxData([],'login');
	    	}
	    	$payment = isset($request->payment)?$request->payment:false;
	    	if(!$payment)
	    		return $this->ajaxError('No se encontró la información del tipo de pago, intente nuevamente');
	    	$data = collect(json_decode($data,true))->merge(['payment'=>$payment])->toJson();
	    	$result	= $this->sendData(config('apiendpoints.API_PAYMENTS_GENERATE'),['data'=>$data],$this->createTokenHeader($token));
	    	return $result;
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    public function products(Request $request){
    	if($request->ajax()){
    		$products	=	isset($request->products)?$request->products:false;
    		$token 		=	isset($request->token)?$request->token:null;
    		if(!$products || $products == 'false')
    			return $this->ajaxError('No hay productos en la orden actual');
    		if(!$token)
    			return $this->ajaxError('Debes iniciar sesión para continuar');
    		if(!is_array($products))
    			$products = json_decode($products,true);
    		$payments = [];
    		$subtotal = 0;
    		$units = 0;
    		foreach ($products as $product) {
    			$subtotal += $this->productPrice($product) * $product['cantidad'];
    			$units 		+= $product['cantidad'];
    		}
    		if($subtotal>0){
    			$online = $this->calculateComission($subtotal);
    			$payments[] = ['name'=>'Sub total','amount' =>$subtotal];
    			$payments[]	= ['name'=>'Comisión online','amount' =>$online];
    			$total			= $subtotal + $online;
    			$products = view('app.payments.products',['products'=>$products, 'payments'=>$payments, 'total'=>$total,'token'=>$token]);
    			return $this->ajaxData(['html'=>$products->render(),'total'=>sprintf("%0.2f",$total),'units' => $units],'html');
    		}
    		return $this->ajaxError('Parece que hay un error en tu pedido, intenta nuevamente');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    public function billing(Request $request){
    	if($request->ajax()){
    		$address	=	isset($request->address)?$request->address:false;
    		$ruc			=	isset($request->ruc)?$request->ruc:[];
    		$token 		=	isset($request->token)?$request->token:null;
    		if(!$address || $address == 'false')
    			return $this->ajaxError('No hay información de envío');
    		if(!$token)
    			return $this->ajaxError('Debes iniciar sesión para continuar');

    		$name = '';
    		if($user = $this->sendData(config('apiendpoints.API_USER_INFO'),[],$this->createTokenHeader($token))){
    			if(array_key_exists('data',$user)){
    				if(array_key_exists('name',$user['data']))
    					$name = $user['data']['name'];
    			}
    		}

    		if(!is_array($address))
    			$address = json_decode($address,true);
    		if(!is_array($ruc))
    			$ruc = json_decode($ruc,true);

    		$address 	= "Enviaremos el pedido a <br/><strong>{$address['address']}</strong><br/> y confirmaremos con el número <br/><strong>{$address['phone']}</strong>";
    		$ruc 			= view('app.payments.ruc',['ruclist'=>$ruc]);
    		return $this->ajaxData(['address'=>$address, 'ruc' =>$ruc->render(), 'name' =>$name],'html');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    public function creditcard(Request $request){
    	if($request->ajax()){

	    	$token = isset($request->token)?$request->token:false;
	    	if(!$token){
	    		$request->session()->flash('message','Debe iniciar sesión para continuar con el pago de tu pedido');
	    		return $this->ajaxData([],'login');
	    	}

	    	$products = isset($request->products)?$request->products:false;
	    	if(!$products)
	    		return $this->ajaxError('No se encontraron productos en tu pedido, intenta nuevamente');

	    	$payment = isset($request->payment)?$request->payment:false;
	    	if(!$payment)
	    		return $this->ajaxError('No se encontró la información del tipo de pago, intente nuevamente');

	    	$method = isset($request->method)?$request->method:false;
	    	if(!$method)
	    		return $this->ajaxError('No se encontró la información del método de pago, intente nuevamente');

	    	$card = isset($request->card)?$request->card:false;
	    	if(!$card)
	    		return $this->ajaxError('No se encontró la información de la tarjeta, intente nuevamente');

	    	$data = ['products'=>$products,'payment'=>$payment,'method'=>$method,'card'=>$card];
	    	$result	= $this->sendData(config('apiendpoints.API_CREDITCARD_CHARGE'),['data'=>$data],$this->createTokenHeader($token));
	    	switch($result['meta']['status']){
	    		case 'ok'					: $number = $result['data']['number'];
	    												$email 	= $result['data']['email'];
	    												$data 	= ['number'=>$number,'email'=>$email,'token'=>$token];
	    												$url 		= route('orders.completed',['data'=>$this->encryptData($data)]);
	    												return $this->ajaxData(['url'=>$url],'ok');
	    												break;
	    		case  'notorder'	: return $this->ajaxError('El pedido se ha modificado o el método de pago ha cambiado, verifique el pedido nuevamente.<br/><a href="'.route("orders.actual",['token'=>$token]).'" class="no-underline text-high text-danger"><i class="fa fa-list-alt"></i>&nbsp;Verificar mi pedido</a>',[],202);
	    												break;
	    		case 'nowebservice': return $this->ajaxError('El servicio de tarjeta de crédito se encuentra ocupado, intenta nuevamente en un momento.');
	    												break;
	    		default: return $result;
	    	}
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    public function mycards(Request $request){
    	if($request->ajax()){
				$token = isset($request->token)?$request->token:false;
    		if($token){
    			$result = $this->sendData(config('apiendpoints.API_CREDITCARD_USER'),[],$this->createTokenHeader($token),"GET");
    			if($result['meta']['status']=='ok'){
    				$cards = $result['data'];
    				$cards = view('app.payments.listcards',['cards'=>$cards]);
    				return $this->ajaxData(['html'=>$cards->render()],'html');
    			}elseif($result['meta']['status']=='none'){
    				return $this->ajaxData(['html'=>'&nbsp;'],'html');
    			}
    			return $result;
    		}

			}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    public function remove(Request $request){
    	if($request->ajax()){
    			$token = isset($request->token)?$request->token:false;
    		if($token){
    			$cardid = isset($request->cardid)?$request->cardid:false;
		    	if(!$cardid)
		    		return $this->ajaxError('No se encontró la tarjeta para eliminar');
		    	$result = $this->sendData(config('apiendpoints.API_CREDITCARD_REMOVE'),['cardid'=>$cardid],$this->createTokenHeader($token));
		    	return $result;
				}else{
	     		return $this->ajaxError('Acción no permitida',[],401);
	    	}
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }
}
