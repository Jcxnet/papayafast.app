<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

class UsersController extends Controller
{
    public function getLogin(){
    	return view('app.users.login',['page_title'=>'Ingresa']);
    }

    public function postLogin(Request $request){
    	if($request->ajax()){
    		$result = $this->sendData( config('apiendpoints.API_USER_LOGIN'),$request->all());
        if($result['meta']['status'] == 'ok'){
    			$user = $this->sendData( config('apiendpoints.API_USER_INFO'), [], $this->createTokenHeader($result['data']['token']) );
    			//if($user['meta']['status']== 'update'){

            if($user['data']){
            $user = new User($user['data']);
            $userData = ['name'=>$user->name,'email'=>$user->email,'id'=>$user->id,'version'=>$this->calculateChecksum(['name'=>$user->name,'email'=>$user->email]) ];
            $request->session()->flash('message',"Bienvenido <b>{$user->name}</b>");
            $appToken = $this->encryptData($result['data']['token']);
            return $this->ajaxData(['userdata'=>$this->encryptData($userData),'token'=>$appToken,'url'=>route('main.home',['token'=>$result['data']['token']])],'update');
          }
    			/*}*/
          return $this->ajaxData(['url'=>route('main.menu',['token'=>$result['data']['token']])],'ok');
    		}
    		return $result;
    	}else{
    		return $this->ajaxError('Acción no permitida',[],401);
    	}
    }

    public function getGuest(){
    	return view('app.users.loginguest',['page_title'=>'Accede como invitado']);
    }

    public function postGuest(Request $request){
      if($request->ajax()){
    		$result = $this->sendData( config('apiendpoints.API_USER_GUEST'),$request->all());
    		if($result['meta']['status'] == 'ok'){
    			$user = $this->sendData( config('apiendpoints.API_USER_INFO'), [], $this->createTokenHeader($result['data']['token']) );
    			//if($user['meta']['status']== 'update'){
    			if($user['data']){
            $user = new User($user['data']);
            $userData = ['name'=>$user->name,'email'=>$user->email,'id'=>$user->id,'version'=>$this->calculateChecksum(['name'=>$user->name,'email'=>$user->email]) ];
            $request->session()->flash('message',"Bienvenido <b>{$user->name}</b>");
            $appToken = $this->encryptData($result['data']['token']);
            return $this->ajaxData(['userdata'=>$this->encryptData($userData),'token'=>$appToken,'url'=>route('main.home',['token'=>$result['data']['token']])],'update');
          }
    			/*}*/
          return $this->ajaxData(['url'=>route('main.menu',['token'=>$result['data']['token']])],'ok');
    		}
    		return $result;
    	}else{
    		return $this->ajaxError('Acción no permitida',[],401);
    	}
    }

    public function getRegister(){
    	return view('app.users.register',['page_title'=>'Crea tu cuenta']);
    }

    public function postRegister(Request $request){
    	if($request->ajax()){
    		$result = $this->sendData( config('apiendpoints.API_USER_REGISTER'),$request->all());
    		try{
    			if($result['meta']['status'] == 'ok'){
    				$message = "<b>{$result['data']['name']}</b>, tus datos han sido registrados y se envió un mensaje a tu cuenta <b>{$result['data']['email']}</b>.<br/>Ahora puedes iniciar sesión.";
    				$request->session()->flash('message',$message);
    				$url = route('users.login.form');
    				return $this->ajaxData(['url' => $url],'ok');
    			}
    			return $result;
    		}catch(Exception $e){
    			return $result;
    		}
    	}else{
    		return $this->ajaxError('Acción no permitida',[],401);
    	}
    }

    public function logout(Request $request){
    	if($request->token){
    		$token 	= $request->token;
    		$result = $this->sendData(config('apiendpoints.API_USER_LOGOUT'),[],$this->createTokenHeader($token));
    		if($result['meta']['status'] == 'ok'){
    			$this->setUserToken(['user'=>null,'token'=>null]);
    			$request->session()->flash('message','Hasta luego');
    		}
    	}
    	return redirect()->route('users.exit');
    }

    public function menu(Request $request){
    	$token = isset($request->token)?$request->token:false;
    	if(!$token){
    		$request->session()->flash('message','Debes iniciar sesión');
    		return redirect()->route('users.login');
    	}
    	$user = $this->sendData( config('apiendpoints.API_USER_INFO'), [], $this->createTokenHeader($token) );
    	if($user['meta']['status']=='ok' || $user['meta']['status']=='update'){
    		$this->setUserToken(['user'=> $user['data']['name'],'token' =>$token]);
    		return view('app.users.menu',['token'=>$token,'name'=>$user['data']['name'],'image'=>$user['data']['image']]);
    	}
			$request->session()->flash('message','No se pudo encontrar su información, intente nuevamente');
			return redirect()->route('main.menu',['token'=>$token]);
    }

    public function profile(Request $request){
    	$token = isset($request->token)?$request->token:false;
    	if(!$token){
    		$request->session()->flash('message','Debes iniciar sesión para ver tu perfil');
    		return redirect()->route('users.login');
    	}
    	$user = $this->sendData( config('apiendpoints.API_USER_INFO'), [], $this->createTokenHeader($token) );
    	if($user['meta']['status']=='ok' || $user['meta']['status']=='update'){
    		$this->setUserToken(['user'=> $user['data']['name'],'token' =>$token]);
    		return view('app.users.profile',['token'=>$token,'name'=>$user['data']['name'],'email'=>$user['data']['email'],'image'=>$user['data']['image']]);
    	}
			$request->session()->flash('message','No se pudo encontrar su información, intente nuevamente');
			return redirect()->route('main.menu',['token'=>$token]);
    }

    public function update(Request $request){
    	if($request->ajax()){
    		$token = isset($request->token)?$request->token:false;
    		if(!$token){
    			$request->session()->flash('message','Debes iniciar sesión para ver tu perfil');
    			return redirect()->route('users.login');
    		}
    		$data = [
    			'apikey'=> config('app.apikey'),
    			'name' 	=> $request->input('name'),
    			'email' => $request->input('email'),
    			'image' => ($request->hasFile('image'))? curl_file_create($request->file('image')->getRealPath(),$request->file('image')->getClientMimeType(),'image') :null,
    		];
    		$result = $this->sendData(config('apiendpoints.API_USER_UPDATE'),$data,$this->createTokenHeader($token),"POST",false);
    		if($result['meta']['status'] == 'ok'){
    			$this->setUserToken(['user'=>null,'token'=>$token]);
    			return $this->ajaxData($result['data'],'ok');
    		}else{
    			return $this->ajaxError($result['data']['message']);
    		}
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }
}
