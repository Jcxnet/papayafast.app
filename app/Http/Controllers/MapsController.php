<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class MapsController extends Controller
{
    /**
     * muestra el mapa para la geolocalización
     * @param  string 	$lat
     * @param  string 	$lng
     * @return View
     */
    public function geolocation(Request $request){
    	$lat = (isset($request->lat))?$request->lat:false;
    	$lng = (isset($request->lng))?$request->lng:false;
    	$token = (isset($request->token))?$request->token:null;

    	return view('app.maps.geolocation',['lat'=>$lat,'lng'=>$lng, 'title'=>'Compartir mi ubicación','page_title'=>'Compartir mi ubicación','body_class'=>'bg-white','token' => $token]);
    }

    public function updateGeolocation(Request $request){
      if($request->ajax()){
        try{
          $data  = $request->data;
          $token = isset($request->token)?$request->token:null;
          $token = ($token =='false')?null:$token;
          $data  = $this->getKeysDecrypted(['point'],$data);
          $data  = $data['point'];
          $url   = route('maps.geolocation', ['lat' => $data['lat'], 'lng' => $data['lng'], 'token'=>$token ]);
          return $this->ajaxData(['url'=>$url],'redirect');
        }catch(Exception $e){
          return $this->ajaxError('No se pudo verificar su ubicación');
        }
      }else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    public function getGeolocation(Request $request){
      if($request->ajax()){
        try{
          $data  = $request->data;
          $data  = $this->getKeysDecrypted(['point'],$data);
          $data  = $data['point'];
          if(is_array($data) && array_key_exists('lat',$data) && array_key_exists('lng',$data) )
          	return $this->ajaxData(['lat' => $data['lat'], 'lng' => $data['lng']],'ok');
          else
          	return $this->ajaxError('No se pudo verificar su ubicación');
        }catch(Exception $e){
          return $this->ajaxError('No se pudo verificar su ubicación');
        }
      }else{
        return $this->ajaxError('Acción no permitida',[],401);
      }
    }

    public function saveGeolocation(Request $request){
    	if($request->ajax()){
    		$data = $request->only(['lat','lng']);
    		try{
    			$data = ['point' => ['lat'=>$data['lat'], 'lng'=>$data['lng']]];
    			$data = $this->encryptData($data);
 					return $this->ajaxData(['data'=>$data], 'update',['message'=>"Ubicación actualizada"]);
    		}catch(Exception $e){
    			return $this->ajaxError('No se pudo verificar su ubicación');
    		}
			}else{
 				return $this->ajaxError('Acción no permitida',[],401);
 			}
    }

    public function address($lat=false,$lng=false){
    	$lat = ($lat)?$lat:false;
    	$lng = ($lng)?$lng:false;
    	return view('app.maps.address',['lat'=>$lat,'lng'=>$lng,'title'=>null,'page_title'=>null,'body_class'=>'bg-white']);
    }

}
