<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group([], function () {
   Route::get  ('/',			     ['as' => 'main.load', 			    'uses' => 'MainController@load']);
   Route::get  ('home',	       ['as' => 'main.home', 			    'uses' => 'MainController@load']);
   Route::post ('load/user',	 ['as' => 'main.load.user', 		'uses' => 'MainController@loadUser']);
   Route::post ('load/app',		 ['as' => 'main.load.app', 			'uses' => 'MainController@loadApp']);
   Route::post ('load/locals', ['as' => 'main.load.locals', 	'uses' => 'MainController@loadLocals']);
   Route::post ('load/menus',	 ['as' => 'main.load.menus', 		'uses' => 'MainController@loadMenus']);
   Route::post ('load/address',['as' => 'main.load.address', 	'uses' => 'MainController@loadAddress']);
   Route::post ('islocated',	 ['as' => 'main.islocated', 		'uses' => 'MainController@islocated']);
	 Route::get  ('asklocation', ['as' => 'main.asklocation', 	'uses' => 'MainController@asklocation']);
	 Route::get  ('menu',   		 ['as' => 'main.menu',     			'uses' => 'MainController@getMenu']);
 });

// users routes
Route::group(['prefix' => 'users'], function(){
	Route::get  ('login',   	['as' => 'users.login.form',  	'uses' => 'UsersController@getLogin']);
	Route::post	('login',     ['as' => 'users.login',   			'uses' => 'UsersController@postLogin']);
	Route::get  ('guest',   	['as' => 'users.guest.form',  	'uses' => 'UsersController@getGuest']);
	Route::post	('guest',     ['as' => 'users.guest',   			'uses' => 'UsersController@postGuest']);
  Route::get  ('register',	['as' => 'users.register.form', 'uses' => 'UsersController@getRegister']);
  Route::post ('register',	['as' => 'users.register', 			'uses' => 'UsersController@postRegister']);
  Route::get  ('logout',		['as' => 'users.logout', 				'uses' => 'UsersController@logout']);
  Route::get  ('menu',			['as' => 'users.menu', 					'uses' => 'UsersController@menu']);
  Route::get  ('profile',		['as' => 'users.profile', 			'uses' => 'UsersController@profile']);
  Route::post ('update',		['as' => 'users.update', 				'uses' => 'UsersController@update']);
  Route::get  ('exit',			['as' => 'users.exit', function(){
  	return view('app.users.exit',['url'=>route('main.home')]);
  }]);
});

// maps routes
Route::group(['prefix' => 'maps'], function(){
	Route::get   ('geolocation',   			['as' => 'maps.geolocation',        'uses' => 'MapsController@geolocation']);
	Route::post  ('geolocation/save',   ['as' => 'maps.geolocation.save',   'uses' => 'MapsController@saveGeolocation']);
  Route::post  ('geolocation/update', ['as' => 'maps.geolocation.update', 'uses' => 'MapsController@updateGeolocation']);
  Route::post  ('geolocation/info',   ['as' => 'maps.geolocation.info', 	'uses' => 'MapsController@getGeolocation']);
});

// locals routes
Route::group(['prefix' => 'locals'], function(){
  Route::get  ('/',      			['as' => 'locals.view',  			'uses' => 'LocalsController@all']);
  Route::post ('list', 				['as' => 'locals.list',  			'uses' => 'LocalsController@allList']);
  Route::post ('map',  				['as' => 'locals.map',   			'uses' => 'LocalsController@allMap']);
  Route::get 	('show/{id}', 	['as' => 'locals.show',   		'uses' => 'LocalsController@show']);
  Route::post ('show/info', 	['as' => 'locals.show.info',  'uses' => 'LocalsController@localInfo']);
  Route::post ('delivery',  	['as' => 'locals.delivery',   'uses' => 'LocalsController@openDelivery']);
  Route::get  ('delivery/off',['as' => 'locals.nodelivery', 'uses' => 'LocalsController@nodelivery']);
  Route::get  ('time/out',		['as' => 'locals.timeout',  	'uses' => 'LocalsController@timeout']);
  Route::post ('favorite',		['as' => 'locals.favorite',  	'uses' => 'LocalsController@favorite']);
  Route::get  ('user',				['as' => 'locals.user',  			'uses' => 'LocalsController@user']);
  Route::get  ('route',				['as' => 'locals.route', 			'uses' => 'LocalsController@route']);
});

// address routes
Route::group(['prefix' => 'address'], function(){
	Route::get   ('/', 				['as' => 'addresses.view',			'uses' => 'AddressesController@getAll']);
	Route::post  ('/', 				['as' => 'addresses.list',			'uses' => 'AddressesController@postAll']);
	Route::get   ('add', 			['as' => 'addresses.add.form',	'uses' => 'AddressesController@getCreate']);
	Route::post  ('add', 			['as' => 'addresses.add',				'uses' => 'AddressesController@postCreate']);
	Route::post  ('inuse', 		['as' => 'addresses.inuse',			'uses' => 'AddressesController@inuse']);
	Route::post  ('get', 			['as' => 'addresses.get',				'uses' => 'AddressesController@info']);
	Route::post  ('delete',		['as' => 'addresses.delete',		'uses' => 'AddressesController@delete']);
	Route::get   ('delivery',	['as' => 'addresses.delivery',	'uses' => 'AddressesController@delivery']);
	Route::post  ('select',		['as' => 'addresses.select',		'uses' => 'AddressesController@select']);
	Route::post  ('order',		['as' => 'addresses.order',			'uses' => 'AddressesController@order']);
	Route::post  ('release',	['as' => 'addresses.release',		'uses' => 'AddressesController@release']);
});

// menu routes
Route::group(['prefix' => 'menus'], function(){
	Route::get   ('load',   			['as' => 'menus.load',			'uses' => 'MenusController@load']);
	Route::get   ('view',   			['as' => 'menus.view',			'uses' => 'MenusController@view']);
	Route::post  ('select', 			['as' => 'menus.select',		'uses' => 'MenusController@select']);
	Route::get   ('page', 		['as' => 'menus.view.page',			'uses' => 'MenusController@showPage']);
	Route::post  ('page', 		['as' => 'menus.page.html',			'uses' => 'MenusController@menuPageHtml']);
	Route::post  ('buttons', 	['as' => 'menus.button.html',		'uses' => 'MenusController@menuButtonHtml']);
	Route::post  ('products',	['as' => 'menus.product.html',	'uses' => 'MenusController@menuProductHtml']);
	Route::post  ('product',	['as' => 'menus.product',	'uses' => 'MenusController@menuProduct']);
	Route::post  ('product/available',	['as' => 'menus.product.available',		'uses' => 'MenusController@productAvailable']);
	//Route::post  ('products/available',	['as' => 'menus.products.available',	'uses' => 'MenusController@productsAvailable']);
});

// order routes
Route::group(['prefix' => 'orders'], function(){
	Route::get  ('actual',  	['as' => 'orders.actual',		'uses' => 'OrdersController@actual']);
	Route::post ('amount', 		['as' => 'orders.amount',		'uses' => 'OrdersController@amount']);
	Route::post ('create', 		['as' => 'orders.create',		'uses' => 'OrdersController@create']);
	Route::post ('generate', 	['as' => 'orders.generate',	'uses' => 'OrdersController@generate']);
	Route::post ('resume', 		['as' => 'orders.resume',		'uses' => 'OrdersController@resume']);
	Route::post ('send', 			['as' => 'orders.send',			'uses' => 'OrdersController@send']);
	Route::post ('verify', 		['as' => 'orders.verify',		'uses' => 'OrdersController@verify']);
	Route::post ('check', 		['as' => 'orders.check',		'uses' => 'OrdersController@check']);
	Route::get  ('completed/{data}', ['as' => 'orders.completed','uses' => 'OrdersController@completed']);
	Route::post ('confirmation',	['as' => 'orders.confirmation',	'uses' => 'OrdersController@confirmation']);
	Route::get  ('history', ['as' => 'orders.history','uses' => 'OrdersController@history']);
	Route::get  ('detail', ['as' => 'orders.detail','uses' => 'OrdersController@detail']);
	Route::post ('status',	['as' => 'orders.status',	'uses' => 'OrdersController@status']);
});

// payments routes
Route::group(['prefix' => 'payments'], function(){
	Route::get  ('select',  	['as' => 'payments.method',		'uses' => 'PaymentsController@method']);
	Route::post ('products',  ['as' => 'payments.products',		'uses' => 'PaymentsController@products']);
	Route::post ('billing',   ['as' => 'payments.billing',		'uses' => 'PaymentsController@billing']);
	Route::post ('validate',  ['as' => 'payments.validate',		'uses' => 'PaymentsController@validateMethod']);
	Route::post ('generate',  ['as' => 'payments.generate',		'uses' => 'PaymentsController@generate']);
	Route::post ('card',  		['as' => 'payments.card',				'uses' => 'PaymentsController@creditcard']);
	Route::post ('mycards', 	['as' => 'payments.mycards',		'uses' => 'PaymentsController@mycards']);
	Route::post ('remove', 	['as' => 'payments.card.remove',		'uses' => 'PaymentsController@remove']);
});

// checkouts routes
Route::group(['prefix' => 'checkouts'], function(){
	Route::get   ('start',		['as' => 'checkouts.start',    'uses' => 'CheckoutsController@start']);
	Route::post  ('billing',	['as' => 'checkouts.billing',  'uses' => 'CheckoutsController@billing']);
	Route::post  ('payment',	['as' => 'checkouts.payment',  'uses' => 'CheckoutsController@payment']);
	Route::post  ('card',			['as' => 'checkouts.card',  	 'uses' => 'CheckoutsController@card']);
});