<?php

namespace App;

use Jenssegers\Model\Model;

class Address extends Model
{
    protected $casts = ['local'=>'array','point'=>'array'];
}
