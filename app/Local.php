<?php

namespace App;

use Jenssegers\Model\Model;


class Local extends Model
{
    protected $casts = [
    		'address'		=>	'array',
    		'contact'		=>	'array',
    		'zone'			=>	'array',
    		'timetable'	=>	'array',
    		'menu'			=>	'array',
    		'image'			=>	'array'];

}
