<ons-row>
	<ons-col>
		<span class="text-desc">&iquest;Con cuánto dinero en efectivo pagarás el pedido?</span>
		<div class="inputbox">
		  <div class="input-container center-block">
		    <input type="text" id="cash_amount" name="cash_amount" placeholder="" class="font-type-2 priceCash" required />
		    <button class="icon" tabindex="-1"><i class="fa fa-dollar"></i></button>
		  </div>
		</div>
		<span class="text-info text-left clearfix text-normal" id="cashout"></span>
		<br/>
	</ons-col>
</ons-row>

@push ('scripts')
<script>
	jQuery(document).ready(function($) {
		$('.priceCash').numeric({decimal:'.',negative:false,'scale':2}).on('keyup',function(){
			var cashout =  ($(this).val() - $('#total-amount').val()).toFixed(2);
			if(cashout>0)
				if(cashout>1000)
					$('#cashout').html ('Estás seguro?');
				else
					$('#cashout').html ('Cambio S/. '+cashout);
			else if(cashout<0)
				$('#cashout').html ('Parece que falta un poco más!');
			else
				$('#cashout').html ('');
		});

	});

</script>
@endpush