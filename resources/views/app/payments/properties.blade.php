@if(count($properties)>0)

	<?php

		if(!isset($group_id)) $group_id = 0;
		$item = 0;
		if(!is_array($properties))
			$properties = collect(json_decode($properties,true))->sortBy('order')->toArray();
		else
			$properties = collect($properties)->sortBy('order')->toArray();

	?>

	<div class="div_product_props">
	@if ($group_id==0)

		@foreach($properties as $property)
			<span class="product-property-selection product-property-unique text-muted">
				{{$property['title']}} @if($property['price']>0) + S/.{!!sprintf("%.2f",$property['price'])!!} @endif
			</span>
		@endforeach

	@else

		@foreach($properties as $property)
			@if ($property['order']==1)
				<span class="product-property-selection product-property-unique text-muted">
					{{$property['title']}} @if($property['price']>0) + S/.{!!sprintf("%.2f",$property['price'])!!} @endif
				</span>
	    @endif
		@endforeach

	@endif
	</div>

@endif
