<div class="alert alert-white text-left" role="alert" id="products_list">
</div>

@push('scripts')
<script type="text/javascript">

	function showProducts(){
	    $.ajax({
	      method: "POST",
	      url: "{{ route('payments.products') }}",
	      data: {'products': JSON.stringify(getLocalData(orderProducts,'list')),'token':'{{$token}}'},
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'html'	: $('#products_list').html(data.data.html);
	      									$('#cart-products-total').html(data.data.units);
	      									break;
	      		default		: showAlert('',data.data.message);
	      								window.location= "{{ route('main.menu',['token'=>$token])}}";
	      	}
				},
	      error: function (){
	      	showAlert('Error','Ocurrió un error al intentar mostrar los productos');
	      }
	    });
    }

		jQuery(document).ready(function($) {
			showProducts();
    });

</script>
@endpush