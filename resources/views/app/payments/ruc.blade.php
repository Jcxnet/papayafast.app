@if(count($ruclist)>0 AND $ruclist !== FALSE)
  <a href="#" class="link-menu list-ruc subtitle">usar RUC <i class="fa fa-keyboard-o"></i></a>
  <div class="webui-popover-content">
    <ul class="list-properties" id="">
    @foreach($ruclist as $ruc)
    <li >
      <a href="#" class="link-menu select-ruc" id="{{$ruc['id']}}" rucnumber="{{$ruc['number']}}" rucname="{{$ruc['name']}}" rucaddress="{{$ruc['address']}}">{{$ruc['number']}} - {{$ruc['name']}}</a>
    </li>
    @endforeach
    </ul>
  </div>
@endif