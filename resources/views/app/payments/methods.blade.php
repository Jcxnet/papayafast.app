@extends('layouts.app')

@push('styles')
  <link rel="stylesheet" href="{{ asset('css/tabs.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/jquery.webui-popover.css') }}" />
@endpush

@section('content')
  <ons-page>
   @include ('app.includes.toolbar')

  <div class="alert alert-white" role="alert" id="products_list">
  	<div class="font-type-2 text-high text-center">
      <p><i class="fa fa-spinner fa-spin fa-fw"></i>&nbsp;Productos de tu pedido...</p>
    </div>
  </div>

    <ons-row>
      <ons-col align="center">

      <div id="tabs" class="tabs text-normal font-type-1">
        <nav>
          <ul>
            <li >
              <a href="#cards" class="payment-selected icon-card info-popover"><span>Tarjeta de crédito</span></a>
              <div class="webui-popover-content text-normal">Paga con tarjeta de crédito</div>
            </li>
            <li>
              <a href="#pos"  class="payment-selected icon-pos info-popover"><span>POS inalámbrico</span></a>
              <div class="webui-popover-content text-normal">Paga con POS inalámbrico</div>
            </li>
            <li>
              <a href="#cash" class="payment-selected icon-cash info-popover"><span>Efectivo</span></a>
              <div class="webui-popover-content text-normal">Paga en efectivo</div>
            </li>
          </ul>
        </nav>
        <div class="content">
          <section id="cards">
            <div class="mediabox">
              @include ('app.payments.method-card',['token'=>$token,'cards'=>[]])
            </div>
          </section>
          <section id="pos">
            <div class="mediabox">
              @include ('app.payments.method-pos',['token'=>$token])
            </div>
          </section>
          <section id="cash">
            <div class="mediabox">
              @include ('app.payments.method-cash',['token'=>$token])
            </div>
          </section>
        </div>
      </div>

      </ons-col>
    </ons-row>

    <ons-row>
    	<ons-col width="99%">
    		@include ('app.payments.billing',['token'=>$token])
    	</ons-col>
    </ons-row>


    <br />
    <input type="hidden" name="total-amount" id="total-amount" value="" />

<ons-bottom-toolbar >
	<ons-button modifier="large" id="checkout_button" class="button-green">
		<i class="fa fa-lock"></i>&nbsp;<span class="text-desc">Enviar pedido</span> &nbsp;<i class="fa fa-chevron-right"></i>
	</ons-button>
</ons-bottom-toolbar>
  </ons-page>
<ons-modal id="myModal">&nbsp;</ons-modal>
@endsection

@push('scripts')
  <script src="{{ asset('lib/jquery/jquery.webui-popover.js') }}"></script>
  <script src="{{ asset('lib/js/tab.js') }}"></script>
  <script src="{{ asset('lib/js/numeric.js') }}"></script>
  <script src="{{ asset('lib/js/order.js') }}"></script>

  <script >
   var theTabs; var theToast;
    jQuery(document).ready(function($) {
      theTabs = new CBPFWTabs( document.getElementById( 'tabs' ) );
      showProducts();
    });

    $('a.info-popover').webuiPopover({trigger:'hover',animation:'pop',multi:true,placement:'vertical'});

    $('input.font-type-2').on('focus',function(){
      $(this).removeClass('input-error').parent().find('.label-error').remove();
    });

    $('#checkout_button').on('click',function(e){
      e.preventDefault();
      $('#myModal').show();

      $('body').find('.label-error').remove();
      $('body').find('.font-type-2').removeClass('input-error');
      var method = theTabs.current;
      switch(method){
        case 0  : verifyCreditCard(method); break;
        case 1  : verifyPOS(method);        break;
        case 2  : verifyCash(method);       break;
        default : showAlert('Aviso','Verifique el método de pago seleccionado');
      }
      return false;
    });

    function showProducts(){
	    $.ajax({
	      method: "POST",
	      url: "{{ route('payments.products') }}",
	      data: {'products': JSON.stringify(getLocalData(orderProducts,'list')),'token':'{{$token}}'},
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'html'	: $('#products_list').html(data.data.html);
	      									//$('#payment-total').html(data.data.total);
	      									$('#total-amount').val(data.data.total);
	      									showBilling();
	      									break;
	      		default		: showAlert('',data.data.message);
	      								window.location= "{{ route('main.menu',['token'=>$token])}}";
	      	}
				},
	      error: function (){
	      	showAlert('Error','Ocurrió un error al intentar mostrar los productos');
	      }
	    });
    }


    function validateResponse(data,method){
      switch(data.meta.status){
        case 'error'        : updateNotification('Error','Revisa los datos ingresados','error');
                              showErrors(data.data.message);
                              break;
        case 'invalidtoken' : updateNotification('Aviso','Verifica tu pedido','warning');
                              showAlert('Aviso',data.data.message);
                              break;
        case 'ok'           : updateNotification('Verificado','Los datos fueron verificados','success');
                              generatePayment(method,data.data.payment);
                              break;
        case 'storage'			: updateNotification('Verificado','Los datos fueron verificados','success');
                              setLocalData(orderPayment,'data',data.data.storage);
                              generatePayment(method,data.data.payment);
                              break;
        case 'login'				: updateNotification('Aviso','Credenciales incorrectas','warning');
        											window.location = "{{route('users.login')}}";
        											break;
        default             : updateNotification('Aviso','Mensaje inesperado','warning');
                              showAlert('Aviso','Se recibió un mensaje inesperado, intente de nuevo.');
      }
    }

    function generatePayment(method,payment){
      $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
      $.ajax({
        method: "POST",
        url: "{{ route('payments.generate') }}",
        data: {'payment': payment,'data':JSON.stringify(getLocalData(orderPayment,'data')),'token':'{{$token}}'},
        beforeSend: function(){
          showNotification('Método de pago','<i class="fa fa-spinner fa-spin"></i> Estamos verificando el método de pago...')
        },
        success: function(data){
          switch(data.meta.status){
            case 'ok' 	:	updateNotification('Método de pago','Verificado','success');
                        	generateOrder(method,data.data.payment);
                        	break;
            case 'login': updateNotification('Aviso','Credenciales incorrectas','warning');
        									window.location = "{{route('users.login')}}";
        									break;
            default   	: updateNotification('Método de pago','Revisa el método de pago','error');
                        	showAlert('Error',data.data.message);
          }
        }
      });
    }

    function generateOrder(method,payment){
      $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{csrf_token()}}"} });
      $.ajax({
        method: "POST",
        url: "{{ route('orders.generate') }}",
        data: {	'payment'	: payment,
        				'address'	: JSON.stringify(getLocalStorage(orderAddress)),
        				'billing'	: JSON.stringify(getLocalData(orderPayment,'data')),
        				'delivery': getLocalData(orderMenu,'id'),
        				'products': JSON.stringify(getLocalData(orderProducts,'list')),
        				'token' 	: '{{$token}}'
        			},
        beforeSend: function(){
          showNotification('Generar pedido','<i class="fa fa-spinner fa-spin"></i> Estamos generando tu pedido...')
        },
        success: function(data){
          switch(data.meta.status){
            case 'ok'         	: updateNotification('Generar pedido','Pedido generado','success');
                                  sendOrder(method,data.data.payment);
                                  break;
            case 'invalidtoken' : updateNotification('Generar pedido','No se pudo completar el pedido','warning');
                                  showAlert('Aviso',data.data.message);
                                  break;
            case 'error'        : updateNotification('Generar pedido','Ocurrió un error al generar el pedido','error');
                                  showAlert('Error',data.data.message);
                                  break;
						case 'login'				: updateNotification('Aviso','Credenciales incorrectas','warning');
        													window.location = "{{route('users.login')}}";
        													break;
            default             : updateNotification('Generar pedido','Mensaje inesperado','warning');
                                  showAlert('Aviso','Se recibió un mensaje inesperado, intente de nuevo.');
          }
        }
      });
    }

    function sendOrder(method,payment){
      $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
      $.ajax({
        method: "POST",
        url: "{{ route('orders.send') }}",
        data: {'payment': payment,'token':'{{$token}}'},
        beforeSend: function(){
          showNotification('Enviar pedido','<i class="fa fa-spinner fa-spin"></i> Estamos enviando tu pedido al local...')
        },
        success: function(data){
          switch(data.meta.status){
            case 'ok'     : updateNotification('Enviar pedido','El pedido fue enviado','success');
            								clearOrderStorage();
            								releaseAddress();
                            window.location = data.data.url;
                            break;
            case 'payment': updateNotification('Enviar pedido','El pedido fue enviado','success');
                            processCard(method,data.data.payment);
                            break;
            case 'error'  : updateNotification('Enviar pedido','Ocurrió un error al enviar el pedido','error');
                            showAlert('Error',data.data.message);
                            break;
      			case 'login'	: updateNotification('Aviso','Credenciales incorrectas','warning');
  													window.location = "{{route('users.login')}}";
  													break;
            default       : updateNotification('Enviar pedido','Mensaje inesperado','warning');
                            showAlert('Aviso','Se recibió un mensaje inesperado, intente de nuevo.');
          }
        }
      });
    }

    function processCard(method,payment){
      $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
      $.ajax({
        method: "POST",
        url: "{{ route('payments.card') }}",
        data: {method: method, payment: payment, card: JSON.stringify(ccForm()), products: JSON.stringify(getLocalData(orderProducts,'list')), token:'{{$token}}'},
        beforeSend: function(){
          showNotification('Pagar pedido','<i class="fa fa-spinner fa-spin"></i> Estamos realizando el cargo en tu tarjeta...')
        },
        success: function(data){
          switch(data.meta.status){
            case 'ok'   : updateNotification('Pagar pedido','El cargo en la tarjeta fue generado','success');
            							clearOrderStorage();
            							releaseAddress();
                          window.location = data.data.url;
                          break;
            case 'error': updateNotification('Pagar pedido','Ocurrió un error al realizar el cargo en la tarjeta','error');
                          showAlert('Error',data.data.message);
                          break;
            case 'login': updateNotification('Aviso','Credenciales incorrectas','warning');
  												window.location = "{{route('users.login')}}";
  												break;
            default     : updateNotification('Pagar pedido','Mensaje inesperado','warning');
                          showAlert('Aviso','Se recibió un mensaje inesperado, intente de nuevo.');
          }
        }
      });
    }



  function releaseAddress(){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }} "} });
    $.ajax({
      method: "POST",
      url: "{{ route('addresses.release') }}",
      data: {'storage': getLocalData(addressesStorage,'data'),'token':'{{$token}}'},
      success: function(data){
        if(data.meta.status == 'ok'){
        		setArrayLocalData(addressesStorage,data.data.storage);
        }
      }
    });
  }

    function showNotification(title,msg){
      theToast = $.toast({
        heading   : title,
        text      : msg,
        hideAfter : false,
        showHideTransition: 'slide',
        allowToastClose: false,
        stack: 6,
        position: 'mid-center',
      });
    }

    function updateNotification(title,msg,type){
      var txtColor; var bgColor; var icon;
      switch(type){
      	case 'info'		: bgColor = '#31708f'; txtColor = '#d9edf7'; icon = '<i class="fa fa-info-circle"></i>'; break;
      	case 'warning': bgColor = '#8a6d3b'; txtColor = '#fcf8e3'; icon = '<i class="fa fa-exclamation-triangle"></i>'; break;
      	case 'error'	: bgColor = '#a94442'; txtColor = '#f2dede'; icon = '<i class="fa fa-exclamation-circle"></i>'; break;
      	case 'success': bgColor = '#3c763d'; txtColor = '#dff0d8'; icon = '<i class="fa fa-check-circle"></i>'; break;
      	default: bgColor = '#2D2D2D'; txtColor = '#ffffff'; icon = '<i class="fa fa-spinner fa-pulse"></i>';
      }
      theToast.update({
        heading 	: title,
        text    	: icon+'&nbsp'+msg,
        bgColor 	: bgColor,
    		textColor : txtColor,
        hideAfter : false
      });
    }

    function closeNotification(){
      $.toast().reset('all');
      $('#myModal').hide();
    }

    function showAlert(title,message){
    	closeNotification();
    	showMessage(title,message);
    }

    function showErrors(errors){
    	closeNotification();
      if(typeof(errors) == 'string')
      	errors = JSON.parse(errors);
      if (errors instanceof Array){
        for(pos in errors){
          error = errors[pos];
          $(error.id).addClass('input-error').next('button').after('<span class="text-danger text-normal label-error pull-left">'+error.msg+'&nbsp;</span>');
        }
      }else{
        $(errors.id).addClass('input-error').next('button').after('<span class="text-danger text-normal label-error pull-left">'+errors.msg+'&nbsp;</span>');
      }
      closeNotification();
      showToast('Se encontraron algunos errores');
    }

  </script>

@endpush