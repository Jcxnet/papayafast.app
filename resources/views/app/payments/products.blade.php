<ons-row>
  <ons-col>
    <div class="text-center clearfix">
      <span class="text-desc text-muted">Total a pagar S/.</span> <span class="text-title text-black payment-total">{{sprintf("%0.2f",$total)}}</span>
    </div>

    <div id="order_details">
      <div id="order_products_list">
        @foreach($products as $product)
				<ons-row class="item-product-row" id="product-row-{{$product['orderid']}}" >
				  <ons-col width="120px" class="text-center" align="center">
				    @if( count($product['image'])>0 AND $product['image'] !== FALSE )
				      <img src="{{ $product['image'][0] }}" class="circle-image">
				    @else
				      <img src="{{ asset('img/menu/0.jpg') }}" class="circle-image">
				    @endif
				  </ons-col >
				  <ons-col >
				      <div class="text-uppercase clearfix"><span class="link-menu text-black font-type-1 select-product-text" ><span class="notification">{{$product['cantidad']}}</span>&nbsp; {{ $product['title'] }} </span></div>
				      <span class="text-normal text-muted clearfix"> {{ $product['detail'] }} </span>
				      @include ('app.payments.properties',['properties' => $product['properties']])
				      @include ('app.payments.groups',['groups' => $product['groups']])
				      <span class="text-normal text-black clearfix" id="product_{{$product['orderid']}}_price">S/. {{sprintf("%.2f",$product['price'])}}</span>
				  </ons-col>
				</ons-row>
				<ons-row id="product-row-border-{{$product['orderid']}}"><div class="item-product-border">&nbsp;</div></ons-row>

				@endforeach
        @foreach($payments as $payment)
				<ons-row class="text-normal">
			    <ons-col width="65%" class="text-right">
			      <span class="detail-subtotal">{{$payment['name']}}</span>
			    </ons-col>
			    <ons-col class="text-left">
			      <div class="price-subtotal"><span >{{sprintf("S/. %0.2f",$payment['amount'])}}</span></div>
			    </ons-col>
				</ons-row>
			  @endforeach
        <ons-row>
          <ons-col class="border-top text-left text-normal">
            <span class="detail-subtotal">
              <a href="{{route('orders.actual',['token'=>$token])}}" class="no-underline text-success"><i class="fa fa-list-alt"></i>&nbsp;Cambiar pedido</a>
            </span>
          </ons-col>
        </ons-row>
       </div>
    </div>
  </ons-col>
</ons-row>