<span class="text-desc">&iquest;Con qué tipo de tarjeta pagarás tu pedido?</span>
<div class="clearfix">
  <input type="hidden" name="pos_label" id="pos_label" /><button class="no-display"></button>
</div>

		<ons-list >
			 	<ons-list-item tappable>
		      <label class="left">
		        <ons-input type="radio" input-id="radio-1" name="pos_card" value="visa" checked></ons-input>
		      </label>
		      <label for="radio-1" class="center">
		        <i class="icon-visa"></i> Visa
		      </label>
		    </ons-list-item>

        <ons-list-item tappable>
		      <label class="left">
		        <ons-input type="radio" input-id="radio-2" name="pos_card" value="visa electron" ></ons-input>
		      </label>
		      <label for="radio-2" class="center">
		        <i class="icon-visaelectron"></i> Visa electron
		      </label>
		    </ons-list-item>

		    <ons-list-item tappable>
		      <label class="left">
		        <ons-input type="radio" input-id="radio-3" name="pos_card" value="mastercard" ></ons-input>
		      </label>
		      <label for="radio-3" class="center">
		        <i class="icon-mastercard"></i> Master Card
		      </label>
		    </ons-list-item>

    </ons-list>
