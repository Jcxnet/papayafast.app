<div id="add_card_div" class="">
<ons-row >
	<ons-col >
		<!--
		<div class="text-center">
			<i class="icon-visa"></i>&nbsp;<i class="icon-mastercard"></i>&nbsp;<i class="icon-discover"></i>&nbsp;<i class="icon-americanexpress"></i>
		</div>
		-->
		<div class="clearfix">
		  <input type="hidden" name="cc_label" id="cc_label" /><button class="no-display"></button>
		</div>
	</ons-col>
</ons-row>
<ons-row>
	<ons-col>
		<div class="inputbox">
    	<div class="input-container center-block">
	      <label for="nameoncard">Nombre en la tarjeta</label>
	      <input type="text" id="nameoncard" name="nameoncard" class="font-type-2" required />
	      <button class="icon" tabindex="-1"><i class="fa fa-user"></i></button>
	    </div>
		</div>
	</ons-col>
</ons-row>
<ons-row>
	<ons-col>
		<div class="inputbox">
	    <div class="input-container center-block">
	      <label for="numbercard">Número de tarjeta</label>
	      <input type="text" id="numbercard" name="numbercard" class="font-type-2 onlyNumber" maxlength="16" required />
	      <button class="icon" tabindex="-1"><i class="fa fa-keyboard-o"></i></button>
	    </div>
		</div>
	</ons-col>
</ons-row>
<ons-row>
	<ons-col width="50%">
		<div class="inputbox">
	    <div class="input-container center-block">
	      <label for="expirecard">Fecha de expiración</label>
	      <input type="text" id="expirecard" name="expirecard" placeholder="MM/YYYY" class="font-type-2" maxlength="7" style="width:80%;" />
	      <button class="icon" tabindex="-1"><i class="fa fa-calendar"></i></button>
	    </div>
  	</div>
	</ons-col>
	<ons-col width="50%">
		<div class="inputbox">
	    <div class="input-container center-block">
	      <label for="securecard">Código de seguridad &nbsp;<a href="#" class="security-code-info"><i class="fa fa-question-circle text-info"></i></a>
	      	<div class="webui-popover-content text-normal">
	      		<span class="clearfix">Para Visa, Mastercard, y Discover (izquierda),<br/> los 3 dígitos en el reverso de su tarjeta.</span>
	      		<span class="clearfix">Para American Express (derecha), <br/>los 4 dígitos en la parte frontal de su tarjeta</span>
	      		<div style="with:48%;float:left;"><img src="{{asset('css/img/card-back.png')}}" alt="" class="img-responsive" /></div>
	      		<div style="with:48%;float:right;"><img src="{{asset('css/img/card-front.png')}}" alt="" class="img-responsive" /></div>
	      	</div>
	      </label>
	      <input type="text" id="securecard" name="securecard" class="font-type-2 onlyNumber" maxlength="4" style="width:80%;"/>
	      <button class="icon" tabindex="-1"><i class="fa fa-lock"></i></button>
	    </div>
  	</div>
	</ons-col>
</ons-row>
<!-- <ons-row>
	<ons-col>
    <label class="checkbox">
		  <input type="checkbox" class="checkbox__input" name="remembercard" id="remembercard">
		  <div class="checkbox__checkmark"></div> <span class="font-type-1 text-normal">Quiero guardar esta tarjeta para usarla en otros pedidos</span>
		</label>
	</ons-col>
</ons-row>
 -->
</div>

@push ('scripts')
<script>
	jQuery(document).ready(function($) {
		$(".onlyNumber").numeric({decimal:false,negative:false});
		$('a.security-code-info').webuiPopover({animation:'pop',multi:true,placement:'vertical'});
	});


</script>
@endpush