@if(count($cards)>0)
	<span class="text-desc" id="cards_title">Seleccione una tarjeta de su lista</span>
@else
	<span class="text-desc" id="cards_title">Agregue una tarjeta a su lista</span>
@endif
<div class="clearfix">
  <input type="hidden" name="listcard_label" id="listcard_label" /><button class="no-display"></button>
</div>
<ul class="list" id="my_cards_list">
  @foreach($cards as $card)
	<li class="list__item list__item--tappable center-block item-card-list" id="row_card_{{$card['id']}}">
		<div class="no-display item-card-options">
	    <ul class="list-circle-icons">
	      <li>
	        <a href="#" class="btnDelete link-button" cardid="{{$card['id']}}" nitem="{{$card['number']}}" title="Eliminar">
	          <i class="fa fa-trash-o hover-red"></i>
	        </a>
	      </li>
	    </ul>
	  </div>
	  <div class="item-card-info">
	    <i class="icon-{{$card['type']}}"></i>
	    <label class="radio-button radio-button--list-item">
	      <input type="radio" class="radio-button__input" name="item_card" value="{{$card['id']}}">
	      <div class="radio-button__checkmark
	        radio-button--list-item__checkmark"></div>
	      <span class="text-subtitle font-type-1">{{ sprintf("**** - **** - **** - %s",substr($card['number'],-4) )}}</span>
	    </label>
	  </div>
	</li>
	@endforeach
</ul>