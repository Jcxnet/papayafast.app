<div id="add_card_div" class="no-display">
<ons-row >
	<ons-col >
		<!--
		<div class="text-center">
			<i class="icon-visa"></i>&nbsp;<i class="icon-mastercard"></i>&nbsp;<i class="icon-discover"></i>&nbsp;<i class="icon-americanexpress"></i>
		</div>
		-->
		<div class="clearfix">
		  <input type="hidden" name="cc_label" id="cc_label" /><button class="no-display"></button>
		</div>
	</ons-col>
</ons-row>
<ons-row>
	<ons-col>
		<div class="inputbox">
    	<div class="input-container center-block">
	      <label for="nameoncard">Nombre en la tarjeta</label>
	      <input type="text" id="nameoncard" name="nameoncard" class="font-type-2" required />
	      <button class="icon" tabindex="-1"><i class="fa fa-user"></i></button>
	    </div>
		</div>
	</ons-col>
</ons-row>
<ons-row>
	<ons-col>
		<div class="inputbox">
	    <div class="input-container center-block">
	      <label for="numbercard">Número de tarjeta</label>
	      <input type="text" id="numbercard" name="numbercard" class="font-type-2 onlyNumber" maxlength="16" required />
	      <button class="icon" tabindex="-1"><i class="fa fa-keyboard-o"></i></button>
	    </div>
		</div>
	</ons-col>
</ons-row>
<ons-row>
	<ons-col width="50%">
		<div class="inputbox">
	    <div class="input-container center-block">
	      <label for="expirecard">Fecha de expiración</label>
	      <input type="text" id="expirecard" name="expirecard" placeholder="MM/YYYY" class="font-type-2" maxlength="7" style="width:80%;" />
	      <button class="icon" tabindex="-1"><i class="fa fa-calendar"></i></button>
	    </div>
  	</div>
	</ons-col>
	<ons-col width="50%">
		<div class="inputbox">
	    <div class="input-container center-block">
	      <label for="securecard">Código de seguridad &nbsp;<a href="#" class="security-code-info"><i class="fa fa-question-circle text-info"></i></a>
	      	<div class="webui-popover-content text-normal">
	      		<span class="clearfix">Para Visa, Mastercard, y Discover (izquierda),<br/> los 3 dígitos en el reverso de su tarjeta.</span>
	      		<span class="clearfix">Para American Express (derecha), <br/>los 4 dígitos en la parte frontal de su tarjeta</span>
	      		<div style="with:48%;float:left;"><img src="{{asset('css/img/card-back.png')}}" alt="" class="img-responsive" /></div>
	      		<div style="with:48%;float:right;"><img src="{{asset('css/img/card-front.png')}}" alt="" class="img-responsive" /></div>
	      	</div>
	      </label>
	      <input type="text" id="securecard" name="securecard" class="font-type-2 onlyNumber" maxlength="4" style="width:80%;"/>
	      <button class="icon" tabindex="-1"><i class="fa fa-lock"></i></button>
	    </div>
  	</div>
	</ons-col>
</ons-row>
<ons-row>
<ons-row>
	<ons-col>
    <label class="checkbox">
		  <input type="checkbox" class="checkbox__input" name="remembercard" id="remembercard">
		  <div class="checkbox__checkmark"></div> <span class="font-type-1 text-normal">Quiero guardar esta tarjeta para usarla en otros pedidos</span>
		</label>
	</ons-col>
</ons-row>
	<ons-col width="50%">
		<div class="text-center">
        <a href="#" id="add_card_cancel" class="link-button text-muted text-desc" modifier="tappable" ><i class="fa fa-ban"></i> Cancelar</a>
    </div>
	</ons-col>
	<ons-col width="50%">
		<div class="text-center">
        <a href="#" id="add_card_ok" 		class="link-button text-success text-desc" modifier="tappable" ><i class="fa fa-lock"></i> Pagar con esta tarjeta</a>
    </div>
	</ons-col>
</ons-row>
</div>

@push ('scripts')
<script>
	jQuery(document).ready(function($) {
		$(".onlyNumber").numeric({decimal:false,negative:false});
		$('a.security-code-info').webuiPopover({animation:'pop',multi:true,placement:'vertical'});
		$('#add_card_cancel').on('click', function(){
			$('#add_card_div').toggleClass('no-display');
			$('#list_cards').toggleClass('no-display');
		});
	});

	$('#add_card_ok').on('click',function(){
		$('body').find('.label-error').remove();
    $('body').find('.font-type-2').removeClass('input-error');
    $('#myModal').show();
		var data = {
	    type    		: 'credit',
	    nameoncard	: $('#nameoncard').val(),
	    numbercard 	: $('#numbercard').val(),
	    expirecard	: $('#expirecard').val(),
	    securecard	: $('#securecard').val(),
	    storecard		: $("#remembercard").is(':checked'),
	    inputid 		: ['nameoncard','numbercard','expirecard','securecard'],
	    billing 		: dataBilling(),
	    token 			: '{{$token}}'
	  };
		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('payments.validate') }}",
	    data: {data: data},
	    beforeSend: function(){
	    	showNotification('Verificando','<i class="fa fa-spinner fa-spin"></i> Estamos validando los datos de la tarjeta...')
	    },
	    success: function(data){
	    	switch(data.meta.status){
	    		case 'ok'			: updateNotification('Verificado','Datos de la tarjeta verificados','success');
	    										generatePayment('credit',data.data.payment);
	    										break;
	    		case 'error'	: updateNotification('Error','Algún dato de la tarjeta es incorrecto','error');
	    										showErrors(data.data.message);
	    										break;
	    		case 'storage': updateNotification('Verificado','Los datos fueron verificados','success');
                          setLocalData(orderPayment,'data',data.data.storage);
                          generatePayment('credit',data.data.payment);
                          break;
        	case 'login'	: updateNotification('Aviso','Credenciales incorrectas','warning');
        									window.location = "{{route('users.login')}}";
        									break;
	    		default 			: updateNotification('Aviso','Mensaje inesperado','warning');
	    										showAlert('Aviso','Se obtuvo una respuesta inesperada, intente nuevamente');
	    	}
	    }
	  });
	});

	function ccForm(){
		return {
	    nameoncard	: $('#nameoncard').val(),
	    numbercard 	: $('#numbercard').val(),
	    expirecard	: $('#expirecard').val(),
	    securecard	: $('#securecard').val(),
	    storecard		: $("#remembercard").is(':checked'),
	  };
	}

	function verifyCreditCard(method){
		 var cardsCount = $('input[name=item_card]').length
		 if(cardsCount == 0){ //no cards
		 		showAlert('Aviso','Debes agregar una tarjeta para usar este método de pago');
		    if($('#add_card_div').hasClass('no-display')){
		    	$('#list_cards').toggleClass('no-display');
					$('#add_card_div').toggleClass('no-display');
		    }
		 		return false;
		 }
		 var cardOfList = $('input[name=item_card]:checked').val();
		 if(cardsCount>0 && cardOfList == null){ //if cards but none selected
		 		showAlert('Aviso','Debes seleccionar una tarjeta de tu lista o agregar una nueva para usar este método de pago');
		 		return false;
		 }

		 if(!$('#add_card_div').hasClass('no-display')){
		 	showAlert('Aviso','Debes agregar la tarjeta o cancelar el proceso para seleccionar una tarjeta de tu lista');
		 	return false;
		 }

		 ons.notification.confirm({
	      messageHTML: '<table width="100%"><tr><td width="50%"><span class="pull-right">CVC:</span></td><td width="50%"><input type="text" id="cvccard" name="cvccard" class="onlyNumber text-center pull-left" maxlength="4" style="width=10%"/></td></tr></table><label for="cvccard"><span class="clearfix">Para Visa, Mastercard, y Discover (izquierda),<br/> los 3 dígitos en el reverso de su tarjeta.</span><span class="clearfix">Para American Express (derecha), <br/>los 4 dígitos en la parte frontal de su tarjeta</span><div style="with:48%;float:left;"><img src="{{asset('css/img/card-back.png')}}" alt="" class="img-responsive" /></div><div style="with:48%;float:right;"><img src="{{asset('css/img/card-front.png')}}" alt="" class="img-responsive" /></div></label>',
	      title: 'Código de seguridad',
	      buttonLabels: ['Ok', 'Cancelar'],
	      animation: 'fade',
	      primaryButtonIndex: 0,
	      cancelable: false,
	      callback: function(index) {
	        if(index == 0) {
	        	var cvcNumber = $('#cvccard').val();
	        	if (cvcNumber== ''){
	        		showAlert('Código de seguridad','Debe ingresar el <strong>Código de seguridad</strong> de la tarjeta seleccionada para poder continuar.');
	        		return false;
	        	}
	        	var data = {
					    type   		: 'cclist',
					    cardid 		: cardOfList,
					    cvcnumber : cvcNumber,
					    inputid 	: ['listcard_label'],
					    billing 	: dataBilling(),
					    token 		: '{{$token}}'
				  	};
				  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
					  $.ajax({
					    method: "POST",
					    url: "{{ route('payments.validate') }}",
					    data: {data:data},
					    beforeSend: function(){
					    	showNotification('Verificando','<i class="fa fa-spinner fa-spin"></i> Estamos verificando tu tarjeta...')
					    },
					    success: function(data){
					    	validateResponse(data,'cclist');
					    }
					  });
	        }
	        if(index == 1){
	        	showAlert('Código de seguridad','Debe ingresar el <strong>Código de seguridad</strong> de la tarjeta seleccionada para poder continuar.');
	        }
	      }
	    });
	}

</script>
@endpush