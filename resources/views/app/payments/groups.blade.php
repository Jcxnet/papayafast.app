@if (count($groups)>0)

<?php
	if(!is_array($groups))
		$groups = collect(json_decode($groups,true))->sortBy('order')->toArray();
	else
		$groups = collect($groups)->sortBy('order')->toArray();
 ?>
<div class="div_product_groups">
		@foreach($groups as $group)
				@include ('app.payments.properties',['properties' => $group['properties'], 'group_id' => $group['id']])
		@endforeach
</div>
@endif