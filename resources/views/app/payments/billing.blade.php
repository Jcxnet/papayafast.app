<div class="alert alert-white" role="alert">
   <ons-row align="center">
    <ons-col>
        <span class="text-normal text-info">&iquest;Qué tipo de comprobante de pago deseas?</span>
        <div class="button-bar" style="width:100%;">
          <div class="button-bar__item">
            <input type="radio" name="segment-billing" itemtype="boleta" value="boleta" checked>
            <button class="button-bar__button">Boleta</button>
          </div>
          <div class="button-bar__item">
            <input type="radio" name="segment-billing" itemtype="factura" value="factura">
            <button class="button-bar__button">Factura</button>
          </div>
        </div>
        <div id="billing_boleta" class="">
          <div class="inputbox">
            <div class="input-container center-block">
              <label for="nameuser">Nombre</label>
              <input type="text" id="nameuser" name="nameuser" value="" class="font-type-2" required />
              <button class="icon" tabindex="-1"><i class="fa fa-user"></i></button>
            </div>
          </div>
        </div>
        <div id="billing_factura" class="no-display">
          <div id="ruc_list"></div>
          <div class="inputbox">
            <div class="input-container center-block">
              <label for="ruc">Número de RUC</label>
              <input type="text" id="ruc" name="ruc" placeholder="RUC" class="font-type-2 numberRUC" maxlength="11" required />
              <button class="icon" tabindex="-1"><i class="fa fa-keyboard-o"></i></button>
            </div>
            <div class="input-container center-block">
              <label for="razon">Razón social</label>
              <input type="text" id="razon" name="razon" placeholder="Razón social" class="font-type-2" required />
              <button class="icon" tabindex="-1"><i class="fa fa-user"></i></button>
            </div>
            <div class="input-container center-block">
              <label for="address">Dirección fiscal</label>
              <input type="text" id="address" name="address" placeholder="Dirección fiscal" class="font-type-2" />
              <button class="icon" tabindex="-1"><i class="fa fa-map-marker"></i></button>
            </div>
          </div>
        </div>
    </ons-col>
   </ons-row>
 </div>

 <ons-row>
  <ons-col >
    <div class="alert alert-warning text-normal" role="alert" id="address_div">
      <i class="fa fa-spinner fa-spin fa-fw"></i>
    </div>
  </ons-col>
 </ons-row>

 <ons-row>
  <ons-col >
    <div class="alert alert-white text-normal" role="alert" id="observation_div">
       <ons-row align="center">
    		<ons-col>
        	<span class="text-info">Agrega un comentario acerca de tu pedido</span>
        	<div class="inputbox">
					  <div class="input-container center-block">
					    <textarea class="font-type-2 textarea" id="observation_order" name="observation_order" placeholder="" style="width: 100%; height: 100px;"></textarea>
					  </div>
					</div>
        </ons-col>
       </ons-row>
    </div>
  </ons-col>
 </ons-row>

 @push('scripts')
 <script>

   		$(".numberRUC , .userDNI").numeric({decimal:false,negative:false});
      $('a.list-ruc').webuiPopover({animation:'pop',multi:true,placement:'vertical'});
      $('a.select-ruc').on('click',function(){
        var rucnumber    = $(this).attr('rucnumber');
        var rucname      = $(this).attr('rucname');
        var rucaddress   = $(this).attr('rucaddress');
        $('#ruc').val(rucnumber);
        $('#razon').val(rucname);
        $('#address').val(rucaddress);
        $('a.list-ruc').webuiPopover('hide');
      });

      $('.button-bar__item input').on('click',function(){
        if($(this).attr('itemtype')=='factura'){
          $('#billing_boleta').addClass( 'no-display' ).fadeOut('normal', 'linear');
          $('#billing_factura').fadeIn( 'normal', 'linear' ).removeClass('no-display');
        }else{
          $('#billing_factura').addClass('no-display').fadeOut( 'normal', 'linear' );
          $('#billing_boleta').fadeIn( 'normal', 'linear' ).removeClass('no-display');
        }
      });

 	function showBilling(){
    $.ajax({
      method: "POST",
      url: "{{ route('payments.billing') }}",
      data: {'address': JSON.stringify(getLocalStorage(orderAddress)),'ruc': JSON.stringify(getLocalData(orderRuc,'list')),'token':'{{$token}}'},
      success: function(data){
      	switch(data.meta.status){
      		case 'html'	: $('#nameuser').val(data.data.name);
      									$('#address_div').html(data.data.address);
      									$('#ruc_list').html(data.data.ruc);
      									break;
      		case 'error' :showMessage('Error',data.data.message);
      									break;
      		default		: showMessage('',data.data.message);

      	}
			},
      error: function (){
      	showMessage('Error','Ocurrió un error al intentar mostrar los datos de envío');
      }
    });
  }

  function dataBilling(){
    var type = $('input[name=segment-billing]:checked').val();
    if(type == 'boleta'){
      var data = {
        type    : 'boleta',
        name    : $('#nameuser').val(),
        inputid : '#nameuser',
        observation: $('#observation_order').val()
      };
    }
    if(type == 'factura'){
      var data = {
        type    : 'factura',
        ruc     : $('#ruc').val(),
        razon   : $('#razon').val(),
        address : $('#address').val(),
        inputid : ['ruc','razon','address'],
        observation: $('#observation_order').val()
      };
    }
    return data;
  }

  function validateBilling(carousel){
  	$.ajax({
      method: "POST",
      url: "{{ route('checkouts.billing') }}",
      data: {'billing': dataBilling(),'checkout':getLocalData(orderPayment,'data'),'token':'{{$token}}'},
      beforeSend: function(){
      	modalValidation(true);
      	showNotification('Datos de envío', '<i class="fa fa-spinner fa-spin"></i>Validando tus datos de envío...');
      },
      success: function(data){
      	switch(data.meta.status){
      		case 'ok'		 : 	updateNotification('Datos de envío','Datos de envío correctos','success');
      										carousel.next();
      										closeNotification();
      										break;
      		case 'error' : 	updateNotification('Datos de envío','Ocurrió un error','error');
      										showMessage('Error',data.data.message);
      										showErrors(data.data.errors);
      										break;
      		default			 : 	updateNotification('Datos de envío','Evento inesperado','warning');
      										showMessage('',data.data.message);

      	}
      	modalValidation(false);
			},
      error: function (){
      	updateNotification('Datos de envío','Ocurrió un error','error');
      	modalValidation(false);
      	showMessage('Aviso','Ocurrió un error al intentar validar los datos de envío');
      }
    });
  }

  jQuery(document).ready(function($) {
      showBilling();
  });
 </script>
 @endpush