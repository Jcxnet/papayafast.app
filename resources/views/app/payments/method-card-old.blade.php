<div id="list_cards" class="">
	<ons-row>
		<ons-col>
			<div id="cards-list-div"></div>
		</ons-col>
	</ons-row>
	<ons-row>
		<ons-col>
			<div class="text-center clearfix">
	       <a href="#" id="add_card" class="button button--large--quiet text-desc text-danger text-uppercase" modifier="tappable" ><i class="fa fa-plus-circle"></i> Agregar tarjeta de crédito</a>
	    </div>
		</ons-col>
	</ons-row>
</div>

@include ('app.payments.addcard',['token'=>$token])

@push ('scripts')
<script>

	jQuery(document).ready(function($) {
       showCards();
    });

	$('#add_card').on('click', function(){
		$('#list_cards').toggleClass('no-display');
		$('#add_card_div').toggleClass('no-display');
	});

	function cardDeleteButton(){
		$('.btnDelete').on('click',function(e){
			e.preventDefault();
	    var ntitle  = $(this).attr('nitem');
	    var cardid  = $(this).attr('cardid');
	    ons.notification.confirm({
	      messageHTML: '¿Deseas eliminar la tarjeta <br/><strong>'+ntitle+'</strong><br/>de tu lista ?',
	      title: 'Eliminar tarjeta',
	      buttonLabels: ['Eliminar', 'Cancelar'],
	      animation: 'fade',
	      primaryButtonIndex: 0,
	      cancelable: false,
	      callback: function(index) {
	        if(index == 0) {
	          $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	          $.ajax({
	            method: "POST",
	            url: "{{route('payments.card.remove')}}",
	            data: {cardid: cardid, token: '{{$token}}'},
	            success: function(data){
	              if(data.meta.status == 'ok'){
	                 $('#row_card_'+cardid).fadeTo("normal", 0.01, function(){
	                     $(this).slideUp("normal", function() {
	                         $(this).remove();
	                     });
	                 });
	                 if(data.total == 0){
	                 	$('#cards_title').html ('Agregue una tarjeta a su lista');
	                 }
	              }
	              if(data.meta.status == 'error'){
	              	showAlert('Error',data.data.message);
	              }
	            }
	          });
	        }
	      }
	    });
	    return false;
		});
	}


	function swipeCards(){
		$(".item-card-list").swipe({
			swipeRight:function(event, direction, distance, duration, fingerCount) {
				$(this).animate({ "left": "50px" }, "normal" );
				$(this).find('.item-card-options').removeClass('no-display').animate({ "left": "-50px" }, "fast");
		  },
		  swipeLeft:function(event, direction, distance, duration, fingerCount) {
		  	$(this).find('.item-card-options').animate({ "left": "0px" }, "fast").addClass('no-display');
		  	$(this).animate({ "left": "0px" }, "normal" );
	    },
	    threshold:10
	  });
	}

	function showCards(){
	    $.ajax({
	      method: "POST",
	      url: "{{ route('payments.mycards') }}",
	      data: {token:'{{$token}}'},
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'html'	: $('#cards-list-div').html(data.data.html);
	      									cardDeleteButton();
	      									swipeCards();
	      									break;
	      		default: showAlert('Aviso',data.data.message);
	      	}
				},
	      error: function (){
	      	showAlert('Error','Ocurrió un error al intentar mostrar la lista de tarjetas');
	      }
	    });
    }

</script>
@endpush