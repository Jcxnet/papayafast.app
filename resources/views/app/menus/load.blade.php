@extends('layouts.app')
@section('content')
<ons-page>
	<ons-row align="center" class="height-100">
		<ons-col >
			<div class="logo-xs">&nbsp;</div>
			<div class="alert alert-success" role="alert">
	      <div class="font-type-2 text-high">
	        <p><i class="fa fa-spinner fa-spin fa-fw"></i>&nbsp;Verificando el local y la dirección...</p>
	      </div>
	    </div>
		</ons-col>
	</ons-row>
</ons-page>
@endsection
@push('scripts')
	<script>
		jQuery(document).ready(function($) {
    	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{csrf_token()}}"} });
	    $.ajax({
	      method: "POST",
	      url: "{{ route('locals.delivery') }}",
	      data: {'id': '{{$local}}', 'storage':getLocalData(localsStorage,'data'),'token':'{{$token}}'},
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'ok' 	: if (data.data.delivery != '' ){
										     		selectAddress("{{$address}}",data.data.url);
										     	}else{
										     		window.location = data.data.url;
										     	}
	      									break;
	      		case 'error': showMessage('',data.data.message);
	      									window.location = data.data.url;
	      									break;
	      		default 		: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
	      									window.location = "{{route('menus.view',['address'=>$address,'local'=>$local,'token'=>$token])}}";
	      	}
	      },
	      error: function (){
	      	showMessage('Error','Ocurrió un error al intentar mostrar el menú');
	      	window.location = "{{route('menus.view',['address'=>$address,'local'=>$local,'token'=>$token])}}";
	      }
	    });

	    function selectAddress(address, url){
	    	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{csrf_token()}}"} });
		    $.ajax({
		      method: "POST",
		      url: "{{ route('addresses.select') }}",
		      data: {'id': address, 'storage':getLocalData(addressesStorage,'data')},
		      success: function(data){
		      	switch(data.meta.status){
		      		case 'ok'	: setArrayLocalData(addressesStorage,data.data.storage);
		      								setArrayLocalData(orderAddress,data.data.address);
		      								window.location = url; break;
		      		default		: window.location = "{{route('menus.view',['address'=>$address,'local'=>$local,'token'=>$token])}}";
		      	}
 					},
		      error: function (){
		      	showMessage('Error','Ocurrió un error al intentar mostrar el menú');
	      		window.location = "{{route('menus.view',['address'=>$address,'local'=>$local,'token'=>$token])}}";
		      }
		    });
	    }

 		});
	</script>
@endpush