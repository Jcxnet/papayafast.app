@if(count($properties)>0)

	<?php

		if(!isset($group_id)) $group_id = 0;
		$item = 0;
		if(!is_array($properties))
			$properties = collect(json_decode($properties,true))->sortBy('order')->toArray();
		else
			$properties = collect($properties)->sortBy('order')->toArray();

	?>

	<div class="div_product_{{$pos}}_props">
	@if ($group_id==0)

		@foreach($properties as $property)
			<span class="product-property-selection product-property-unique text-muted" id="product_{{$product_id}}_prop_{{$property['id']}}" productid="{{$product_id}}" propertyid="{{$property['id']}}" code="{{$property['code']}}" >{{$property['title']}}</span>
		@endforeach

	@else

				@foreach($properties as $property)
					@if ($item==0)
						<a href="#" class="link-menu product-property-selection product-{{$pos}}-property" id="product_{{$pos}}_group_{{$group_id}}_prop_{{$property['id']}}" productid="{{$product_id}}" propertyid="{{$property['id']}}" posid="{{$pos}}" groupid="{{$group_id}}" actualid="{{$property['id']}}" code="{{$property['code']}}" >{{$property['title']}} @if($property['price']>0) + S/.{!!sprintf("%.2f",$property['price'])!!} @endif</a>
						<?php $actualid = $property['id'] ?>
						<div class="webui-popover-content">
						<ul class="list-properties" id="product_{{$pos}}_group_{{$group_id}}_props_{{$product['id']}}">
					@else
					 	<li >
			      	<a href="#" class="link-menu product-property-selectable product-{{$pos}}-property" id="product_{{$pos}}_group_{{$group_id}}_prop_{{$property['id']}}" productid="{{$product_id}}" propertyid="{{$property['id']}}" posid="{{$pos}}" groupid="{{$group_id}}" actualid="{{$actualid}}" code="{{$property['code']}}">{{ $property['title']}} @if($property['price']>0) + S/.{!!sprintf("%.2f",$property['price'])!!} @endif</a>
			    	</li>
			    @endif
					<?php $item++; ?>
				@endforeach
						</ul>
						</div>
	@endif
	</div>

@endif
