@if (isset($buttons))
	@foreach($buttons as $button)

		@if ($button['style'] == 'square')
		<ons-row class="menu-button-square">
			<ons-col class="text-center" >
				<a href="{{ route('menus.view.page',['page'=>$button['page'],'token'=>$token]) }}" class="text-white text-uppercase menu-button">
					<div class="label inside middle left " data-label="{{$button['title']}}">
						<img  src="{{ $button['image'][0] }}" class="ri"  alt="" />
					</div>
				</a>
			</ons-col>
		</ons-row>

		@elseif ($button['style'] == 'circle')

		<ons-row class="menu-button-circle">
			<ons-col width="100px">
		   	<img src="{{ $button['image'][0] }}" class="circle-image">
			</ons-col>
			<ons-col align="center">
		 		<a href="{{ route('menus.view.page',['page'=>$button['page'],'token'=>$token]) }}" class="pull-right"><i class="fa fa-chevron-right fa-2x text-danger"></i></a>
		  	<div class="text-subtitle font-type-1 text-uppercase clearfix"> <a href="{{ route('menus.view.page',['page'=>$button['page'],'token'=>$token]) }}" class="text-black link-menu">{{ $button['title'] }}	</a></div>
			</ons-col>
		</ons-row>
		@endif

	@endforeach
@endif