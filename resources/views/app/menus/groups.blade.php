@if (count($groups)>0)

<?php
	if(!is_array($groups))
		$groups = collect(json_decode($groups,true))->sortBy('order')->toArray();
	else
		$groups = collect($groups)->sortBy('order')->toArray();
 ?>
<div class="div_product_{{$product_id}}_groups">
		@foreach($groups as $group)
		<span class="text-normal sublist">{{$group['title']}}</span>
			<div class="sublist" id="div_product_{{$product_id}}_group_{{$group['id']}}">
				@include ('app.menus.properties',['properties' => $group['properties'], 'product_id' => $product_id, 'group_id' => $group['id'],'pos' => $pos])
			</div>
		@endforeach
</div>
@endif