@extends('layouts.app')
@section('content')
<ons-page>
	<ons-row align="center" class="height-100">
		<ons-col >
			<div class="logo-xs">&nbsp;</div>
			<div class="alert alert-info" role="alert">
	      <div class="font-type-2 text-high text-center">
	        <p><i class="fa fa-spinner fa-spin fa-fw"></i>&nbsp;<span id="menu-status">Consultando el menú para <br/><b>{!!$name!!}</b>...</span></p>
	      </div>
	    </div>
		</ons-col>
	</ons-row>
</ons-page>
@endsection
@push('scripts')
<script src="{{asset('lib/js/order.js')}}"></script>
<script >
	jQuery(document).ready(function($) {
		function selectMenu(){
    	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{csrf_token()}}"} });
	    $.ajax({
	      method: "POST",
	      url: "{{ route('menus.select') }}",
	      data: {'menu': '{{$menu}}', 'storage':getLocalData(menusStorage,'data'),'token':'{{$token}}'},
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'ok'	: $('#menu-status').html('Consultando los productos...');
	      								setArrayLocalData(orderMenu,data.data.menu);
	      								verifyActualOrderProducts();
	      								window.location = data.data.url;
	      								break;
	      		default		: showMessage('',data.data.message);
	      								window.location = "{{route('main.menu',['token'=>$token])}}";
	      	}
					},
	      error: function (){
	      	showMessage('Error','Ocurrió un error al intentar mostrar el menú');
      		window.location = "{{route('main.menu',['token'=>$token])}}";
	      }
	    });
    }
    selectMenu();
	});
</script>
@endpush