<div id="div-products">
	<ons-row align="center" class="height-100">
		<ons-col >
			<div class="logo-xs">&nbsp;</div>
			<div class="alert alert-info" role="alert">
	      <div class="font-type-2 text-high text-center">
	        <p><i class="fa fa-spinner fa-spin fa-fw"></i>&nbsp;Preparando los elementos de la página...</p>
	      </div>
	    </div>
		</ons-col>
	</ons-row>
</div>

<script >
	jQuery(document).ready(function($) {

		function showProducts(products){
	    $.ajax({
	      method: "POST",
	      url: "{{ route('menus.product.html') }}",
	      data: {'products': JSON.stringify(getStorageArray(orderMenu,'products',products)),'token':'{{$token}}'},
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'html'	: $('#div-products').html(data.data.html);
	      								break;
	      		default		: showMessage('',data.data.message);
	      	}
				},
	      error: function (){
	      	showMessage('Error','Ocurrió un error al intentar mostrar los productos');
	      }
	    });
    }

		showProducts({!!$products!!});
	});

</script>
