@if (isset($products))
	<?php  $pos = 1; 	?>
	@foreach($products as $product)

	<div class="flipcard" id="card-{{$pos}}">
			<div class="face front">
					<ons-row class="item-product-row" id="product-row-{{$pos}}" posid="{{$pos}}" productid="{{$product['id']}}" orderid="{{$product['orderid']}}">
					 	<ons-row class="item-product-row-info">
						 	<ons-col width="98px" align="center">
						    @if( count($product['image'])>0 AND $product['image'] !== FALSE )
						      <img src="{{ $product['image'][0] }}" class="circle-image">
						    @else
						      <img src="{{ asset('img/menu/0.jpg') }}" class="circle-image">
						    @endif
						    <br/>
						    <div class="text-center clearfix">
						    	<a href="#" class="select-product-text" productid="{{$product['id']}}" orderid="{{$product['orderid']}}" posid="{{$pos}}"><i class="fa fa-circle-thin text-blue" id="product-radio-{{$pos}}"></i></a>
						    </div>
						  </ons-col >
						  <ons-col >
						  		<div class="text-uppercase clearfix">
						      	<a href="#" class="link-menu text-black font-type-1 select-product-text" productid="{{$product['id']}}" orderid="{{$product['orderid']}}" posid="{{$pos}}"> {{ $product['title'] }} </a>
						      </div>
						      <span class="text-normal text-muted clearfix"> {{ $product['detail'] }} </span>
						      @include ('app.menus.properties',['properties' => $product['properties'],'product_id'=>$product['id'],'pos'=>$pos])
						      @include ('app.menus.groups',['groups' => $product['groups'],'product_id'=>$product['id'],'pos'=>$pos])
						  </ons-col>
						 </ons-row>
						 <ons-row align="center" class="spin-row" posid="{{$pos}}">
							 	<ons-col width="50px">
						  		<a href="#" class="spin-sub"><i class="fa fa-minus"></i></a>
							  </ons-col>
							  <ons-col align="center">
							  	<div class="text-center">
							  		<input type="hidden" min="1" max="10" value="{{$product['cantidad']}}" id="product-total-{{$pos}}" />
							  		<input type="hidden" value="{{$product['price']}}" id="product-price-{{$pos}}" />
							  		@if($order)
							  			<span class="text-black pull-right product-price" id="product_{{$pos}}_price"><span class="notification">{{$product['cantidad']}} </span> S/. {{sprintf("%.2f",$product['price'])}}</span>
							  			<a href="#" class="btn-product-options" id="btn-row-options-{{$pos}}" posid="{{$pos}}"><i class="fa fa-bars"></i></a>
							  		@else
							  			<span class="text-black pull-right product-price" id="product_{{$pos}}_price">S/. {{sprintf("%.2f",$product['price'])}}</span>
							  		@endif
							  	</div>
							  </ons-col>
							  <ons-col width="50px">
							  		<a href="#" class="spin-add pull-right"><i class="fa fa-plus"></i></a>
							  </ons-col>
						 </ons-row>
					</ons-row>

	      </div>
	      <div class="face back product-back-side" posid="{{$pos}}" id="back-side-{{$pos}}">
					@if($order)
					<a href="#" class="btn-product-options btn-return-front" id="btn-back-options-{{$pos}}" posid="{{$pos}}"><i class="fa fa-reply"></i></a>
					<ons-row>
						<ons-col width="98px" align="center">
					    <br/>
					    @if( count($product['image'])>0 AND $product['image'] !== FALSE )
					      <img src="{{ $product['image'][0] }}" class="circle-image">
					    @else
					      <img src="{{ asset('img/menu/0.jpg') }}" class="circle-image">
					    @endif
					  </ons-col >
					  <ons-col align="center">
					  		<br/>
					  		<div class="text-uppercase clearfix">
					      	<a href="#" class="link-menu text-black font-type-1 clearfix" productid="{{$product['id']}}" orderid="{{$product['orderid']}}" posid="{{$pos}}">
					      		<span class="notification">{{$product['cantidad']}}</span>&nbsp; {{ $product['title'] }}
					      	</a>
					      	<span class="text-black product-price" >S/. {{sprintf("%.2f",$product['price'])}}</span>
					      </div>
							  <ul class="list-circle-icons">
						      <!-- <li class="option-divide">
						        <a href="#" class="link-button" posid="{{$pos}}" orderid="{{$product['orderid']}}" nitem="{{$product['title']}}" title="Dividir">
						          <i class="fa fa-scissors hover-yellow"></i>
						        </a>
						      </li> -->
						      <li>
						        <a href="#" class="btnDuplicate link-button" posid="{{$pos}}" orderid="{{$product['orderid']}}" nitem="{{$product['title']}}" title="Duplicar">
						          <i class="fa fa-clone hover-blue"></i>
						        </a>
						      </li>
						      <li>
						        <a href="#" class="btnDelete link-button" posid="{{$pos}}" orderid="{{$product['orderid']}}" nitem="{{$product['title']}}" title="Eliminar">
						          <i class="fa fa-trash-o hover-red"></i>
						        </a>
						      </li>
						    </ul>
					  </ons-col>
					</ons-row>
					@endif
	      </div>

	</div>

	<ons-row id="product-row-border-{{$pos}}"><div class="item-product-border">&nbsp;</div></ons-row>

	<?php $pos++;	?>
	@endforeach

	@if(!$order)
		<ons-bottom-toolbar >
			<div class="text-center">
				<button class="toolbar-button toolbar-button--outline no-display" id="btn-add-selected-products">
				</button>
				<button class="toolbar-button toolbar-button--outline" id="btn-view-cart" >
		  		<i class="fa fa-cutlery"></i> <span class="notification">0</span>
				</button>
			</div>
		</ons-bottom-toolbar>
	@endif

@endif

<script>

	function productPropertySelect(){

		$('a.product-property-selectable').unbind();

		$('a.product-property-selection').webuiPopover({
        animation:'pop',
        multi:true,
        backdrop:false,
        placement:'vertical',
        dismissible: true,
        trigger:'hover',
        delay:{
        	show: null, hide:300
        }
     });

		$('a.product-property-selectable').on('click',function(e){
	  	e.preventDefault();
	  	WebuiPopovers.hideAll();
	    var productid   = $(this).attr('productid');
	    var posid       = $(this).attr('posid');
	    var groupid     = $(this).attr('groupid');
	    var actualid    = $(this).attr('actualid');
	    var newactualid = $(this).attr('propertyid');
	    var linkid      = '#product_'+posid+'_group_'+groupid+'_prop_'+actualid;
	    var listid      = '#product_'+posid+'_group_'+groupid+'_props_'+productid;
	    var row       	= $('#product-row-'+posid);
	    var orderid 		= $(row).attr('orderid');
	    var cantidad 		= parseInt($('#product-total-'+posid).val());

	    var propertyid = $(linkid).attr('propertyid');
	    $(linkid).attr('propertyid',$(this).attr('propertyid'));
	    $(this).attr('propertyid',propertyid);
	    var code = $(linkid).attr('code');
	    $(linkid).attr('code',$(this).attr('code'));
	    $(this).attr('code',code);
	    var title = $(linkid).html();
	    $(linkid).html($(this).html());
	    $(this).html(title);
	    var itemid = $(linkid).attr('id');
	    $(linkid).attr('id',$(this).attr('id'));
	    $(this).attr('id',itemid);

	    $(listid).find('[actualid="'+ actualid + '"]').attr('actualid',newactualid);

	    var data = {
	        productid  : productid,
	        orderid 	 : orderid,
	        properties : [],
	        cantidad 	 : cantidad,
	      };
	    row.find('.product-property-selection').each( function( index, element ){
	        data.properties.push( $(this).attr('propertyid') );
	    });
	    $('#product_'+posid+'_price').html('<i class="fa fa-refresh fa-spin"></i>');
	    //$('#product_'+posid+'_price').html( 'S/. '+productPrice(data) );
	    $('#product-price-'+posid).val(productPrice(data));
	    showPriceLabel(posid);

	    @if($order)
	    	updateProduct(data);
	    @endif
	    return false;
  	});
  }


  function flipListener(){
  	$('.item-product-row-info').unbind();
  	$('.item-product-row-info').on('click',function(e){
      e.preventDefault();
      var posid = $(this).attr('posid');
      toggleProduct(posid);
			showAddProductsSelected();
      return false;
    });
    $('.select-product-text').unbind();
    $('.select-product-text').on('click',function(e){
      e.preventDefault();
      var posid = $(this).attr('posid');
      toggleProduct(posid);
			showAddProductsSelected();
      return false;
    });
    $('.btn-product-options,.btn-return-front').unbind();
    $('.btn-product-options,.btn-return-front').on('click',function (e) {
    	e.preventDefault();
    	var pos = $(this).attr('posid');
    	var card = $('#card-'+pos);
    	var frontHeight = card.find('.front').outerHeight();
    	var btnback = $('#btn-back-options-'+pos);

			card.find('.back').height(frontHeight);
    	if (card.hasClass('flipped')) {
        card.removeClass('flipped');
        btnback.hide();
    	} else {
        card.addClass('flipped');
        btnback.show();
    	}
    	updateBackSide(pos);
    	return false;
		});
	}

   // check if the product is available from menu
    function selectedProduct(item){
   		var product = getStorageItem(orderMenu,'products',item.attr('productid'));
   		var posid 	= item.attr('posid');
    	$.ajax({
	      method: "POST",
	      url: "{{ route('menus.product.available') }}",
	      data: {'product': JSON.stringify(product), 'address': JSON.stringify(getLocalStorage(orderAddress))},
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'ok'	: if(!data.data.available){
	      									toggleProduct(posid);
	      									showMessage('Aviso','El producto seleccionado no se encuentra disponible por el momento');
	      								}else{
	      									addSelectedProduct(item);
	      								}
	      								break;
	      		default		: showAlert('',data.data.message);
	      	}
				},
	      error: function (){
	      	showAlert('Error','Ocurrió un error al intentar verificar el producto');
	      }
	    });
    }

    function addSelectedProduct(item){
    	var productid = item.attr('productid');
      var posid 		= item.attr('posid');
      var orderid 	= item.attr('orderid');
      var row 			= $('#product-row-'+posid);
			var cantidad 	= parseInt($('#product-total-'+posid).val());

      row.addClass('add-product-cart').delay(2000).queue(function(){
          item.removeClass('add-product-cart').dequeue();
      });
      toggleProduct(posid);
      var data = {
        productid  : productid,
        orderid 	 : orderid,
        properties : [],
        cantidad 	 : cantidad,
      };
      row.find('.product-property-selection').each( function( index, element ){
          data.properties.push( $(element).attr('propertyid') );
      });
     	addProduct(data);
    }

    function toggleProduct(posid){
      //var linkid = '.product-link-'+posid;
      var icon = $('#product-radio-'+posid);
      var row  = $('#product-row-'+posid);
      var btnoptions = $('#btn-row-options-'+posid);
      var bg = $(row.find('.item-product-row-info'));
      //$(linkid).toggle();

      if(icon.hasClass('fa-dot-circle-o')){
        icon.removeClass('fa-dot-circle-o').addClass('fa-circle-thin');
        row.removeClass('product-row-selected');
        row.find('.product-price').addClass('pull-right');
        row.find('.spin-row').removeClass('spin-row-active');
        row.find('.spin-add,.spin-sub').hide();
        btnoptions.hide();
        bg.removeClass('bg-white');
      }else{
        icon.removeClass('fa-circle-thin').addClass('fa-dot-circle-o');
        row.addClass('product-row-selected');
        row.find('.product-price').removeClass('pull-right');
        row.find('.spin-row').addClass('spin-row-active');
        row.find('.spin-add,.spin-sub').show();
        btnoptions.show();
        bg.addClass('bg-white');
      }
      showPriceLabel(posid);
    }

    function showPriceLabel(pos){
    	var row = $('#product-row-'+pos).find('.spin-row');
    	var num = parseInt($('#product-total-'+pos).val());
			var price = parseFloat($('#product-price-'+pos).val());
			var lblprice = $('#product_'+pos+'_price');
			if(row.hasClass('spin-row-active'))
				lblprice.html('<span class="notification">'+num+'</span> S/. '+parseFloat(num*price).toFixed(2));
			else{
				@if($order)
					lblprice.html('<span class="notification">'+num+'</span> S/. '+parseFloat(price).toFixed(2));
				@else
					lblprice.html('S/. '+parseFloat(price).toFixed(2));
				@endif
			}
    }

    function addProduct(data){
    	if(addToCart(data)){
    		showToast('Ok','Producto agregado');
    		//window.location = "{{ route('orders.actual',['token'=>$token]) }}";
    	}else{
    		showMessage('','No se pudo agregar el producto a tu orden, intenta nuevamente');
    	}
    	return false;
    }
    function updateProduct(data){
    	if(updateCart(data)){
    		showToast('','Producto actualizado',1);
    	}else{
    		showMessage('','No se pudo actualizar el producto, intenta nuevamente');
    	}
    	return false;
    }

    @if(!$order)
    /*$('.item-product-row').swipe({ //add product to cart when swipe to right
      swipeRight:function(event, direction, distance, duration, fingerCount) {
      	var posid = $(this).attr('posid');
        toggleProduct(posid);
        //selectedProduct($(this));
        addSelectedProduct($(this));
      },
      threshold:5
    });*/
    @endif

    (function($) {
			$.fn.spinner = function() {
				this.each(function() {
					var el = $(this);
					var pos = el.attr('posid');
					var num = $('#product-total-'+pos);
					var price = parseFloat($('#product-price-'+pos).val());
					var lblprice = $('#product_'+pos+'_price');
					// substract
					el.find('.spin-sub').on('click', function () {
						if (parseInt(num.val()) > parseInt(num.attr('min'))){
							val = parseInt(num.val());
							num.val( --val );
							showPriceLabel(pos);
							@if($order)
								updateProductSelected(pos);
							@else
								showAddProductsSelected();
							@endif
						}
					});

					// increment
					el.find('.spin-add').on('click', function () {
						if (parseInt(num.val()) < parseInt(num.attr('max'))){
							val = parseInt(num.val());
							num.val( ++val );
							showPriceLabel(pos);
							@if($order)
								updateProductSelected(pos);
							@else
								showAddProductsSelected();
							@endif
						}
					});
			  });
			};
		})(jQuery);

		$('.spin-row').spinner();

		$('#btn-add-selected-products').on('click',function(){
			$('.product-row-selected').each(function(){
				addSelectedProduct($(this));
			});
			showOrderTotal('btn-view-cart');
			showAddProductsSelected();
		});

		$('#btn-view-cart').on('click',function(){
			window.location = "{{ route('orders.actual',['token'=>$token]) }}";
		});

		function showAddProductsSelected(){
			var total = 0;
			var btn = $('#btn-add-selected-products');
			$('.product-row-selected').each(function(){
				var row = $(this);
				var pos = row.attr('posid');
				var val = parseInt($('#product-total-'+pos).val());
				total+= val;
			});
			if (total>0){
				var txt = (total>1)?'productos':'producto';
				btn.html('<i class="fa fa-plus"></i> Agregar <span class="notification">'+total+'</span> '+txt);
				btn.show();
			}else{
				btn.hide()
			}
			showOrderTotal('btn-view-cart');
		}

		function updateProductSelected(pos){
			var cantidad 	= parseInt($('#product-total-'+pos).val());
			var orderid 	= $('#product-row-'+pos).attr('orderid');
			updateCartProductAmount(orderid,cantidad);
			updateAmount();
		}

		function updateBackSide(pos){
			var back = $('#back-side-'+pos);
			var cantidad = parseInt($('#product-total-'+pos).val());
			back.find('.notification').html(cantidad);
			/*if(cantidad>1)
				back.find('.option-divide').show();
			else
				back.find('.option-divide').hide();*/
		}

		showOrderTotal('btn-view-cart');
		flipListener();
		productPropertySelect();
</script>