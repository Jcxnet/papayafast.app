@extends('layouts.app')

@push('styles')
  <link rel="stylesheet" href="{{ asset('css/label.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/jquery.webui-popover.css') }}" />
@endpush

@section('content')

<ons-page>
   @include ('app.includes.toolbar')
   <div id="page-content">
	   <ons-row align="center" class="height-100">
			<ons-col >
				<div class="logo-xs">&nbsp;</div>
				<div class="alert alert-info" role="alert">
		      <div class="font-type-2 text-high text-center">
		        <p><i class="fa fa-spinner fa-spin fa-fw"></i>&nbsp;Preparando la página...</p>
		      </div>
		    </div>
			</ons-col>
		</ons-row>
	</div>
</ons-page>

@endsection

@push('scripts')
<script src="{{ asset('lib/jquery/jquery.webui-popover.js') }}"></script>
<script src="{{ asset('lib/js/order.js') }}"></script>
<script >
	jQuery(document).ready(function($) {
    showPage({{$page}});
	});

function showPage(page){
	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{csrf_token()}}"} });
  $.ajax({
    method: "POST",
    url: "{{ route('menus.page.html') }}",
    data: {'page': getStorageItem(orderMenu,'pages',page),'token':'{{$token}}'},
    success: function(data){
    	switch(data.meta.status){
    		case 'html'	: $('#page-content').html(data.data.html);
    									break;
    		default		: showMessage('',data.data.message);
    	}
			},
    error: function (){
    	showMessage('Error','Ocurrió un error al intentar mostrar la página');
    }
  });
}

</script>
@endpush