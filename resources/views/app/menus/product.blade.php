@if (isset($products))
	<?php  $pos = 1; 	?>

	@if($order)
	<div class="alert alert-info alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="pulse-group font-type-2 text-normal">
      <i class="fa fa-chevron-right icon-pulse "></i>
      <i class="fa fa-chevron-right icon-pulse "></i>
      <i class="fa fa-chevron-right icon-pulse "></i>
      &nbsp;Desliza los ítems a la derecha para ver más opciones.
    </div>
  </div>
	@endif

	@foreach($products as $product)

	<ons-row class="{{($order)?'swipe-main':''}} item-product-row" id="product-row-{{$pos}}" posid="{{$pos}}" productid="{{$product['id']}}" orderid="{{$product['orderid']}}">
	  @if($order)
	  <div class="swipe-menu">
	    <ul class="list-circle-icons">
	      <li>
	        <a href="#" class="btnDuplicate link-button" posid="{{$pos}}" orderid="{{$product['orderid']}}" nitem="{{$product['title']}}" title="Duplicar">
	          <i class="fa fa-clone hover-blue"></i>
	        </a>
	      </li>
	      <li>
	        <a href="#" class="btnDelete link-button" posid="{{$pos}}" orderid="{{$product['orderid']}}" nitem="{{$product['title']}}" title="Eliminar">
	          <i class="fa fa-trash-o hover-red"></i>
	        </a>
	      </li>
	    </ul>
	  </div>
		@endif

	  <ons-col width="120px" class="text-center" align="center">
	    @if( count($product['image'])>0 AND $product['image'] !== FALSE )
	      <img src="{{ $product['image'][0] }}" class="circle-image">
	    @else
	      <img src="{{ asset('img/menu/0.jpg') }}" class="circle-image">
	    @endif
	  </ons-col >
	  <ons-col >
	      <div class="text-uppercase clearfix"><a href="#" class="link-menu text-black font-type-1 select-product-text" productid="{{$product['id']}}" orderid="{{$product['orderid']}}" posid="{{$pos}}"> {{ $product['title'] }} </a></div>
	      <span class="text-normal text-muted clearfix"> {{ $product['detail'] }} </span>
	      @include ('app.menus.properties',['properties' => $product['properties'],'product_id'=>$product['id'],'pos'=>$pos])
	      @include ('app.menus.groups',['groups' => $product['groups'],'product_id'=>$product['id'],'pos'=>$pos])
	      <span class="text-normal text-black clearfix" id="product_{{$pos}}_price">S/. {{sprintf("%.2f",$product['price'])}}</span>
	      <input type="number" min="1" max="10" value="4" class="input-spinner" />
	      @if(!$order)
	      <a href="#" class="select-product-link product-link-{{$pos}}" posid="{{$pos}}" productid="{{$product['id']}}" orderid="{{$product['orderid']}}"><i class="fa fa-plus-square"></i>&nbsp;Agregar a mi lista</a>
	      @endif
	  </ons-col>
	  @if(!$order)
	  <ons-col width="16px" align="center">
	    <a href="#" class="select-product-text" productid="{{$product['id']}}" orderid="{{$product['orderid']}}" posid="{{$pos}}"><i class="fa fa-circle-thin text-success" id="product-radio-{{$pos}}"></i></a>
	  </ons-col>
	  @endif
	</ons-row>
	<ons-row id="product-row-border-{{$pos}}"><div class="item-product-border">&nbsp;</div></ons-row>
	<?php
		$pos++;
	?>
	@endforeach
@endif

<script>

	$('a.product-property-selection').webuiPopover({
        animation:'pop',
        multi:true,
        backdrop:false,
        placement:'vertical'
      });

  $('a.product-property-selectable').on('click',function(e){
  	e.preventDefault();
    var productid   = $(this).attr('productid');
    var posid       = $(this).attr('posid');
    var groupid     = $(this).attr('groupid');
    var actualid    = $(this).attr('actualid');
    var newactualid = $(this).attr('propertyid');
    var linkid      = '#product_'+posid+'_group_'+groupid+'_prop_'+actualid;
    var listid      = '#product_'+posid+'_group_'+groupid+'_props_'+productid;
    var row       	= $('#product-row-'+posid);
    var orderid 		= $(row).attr('orderid');

    $('a.product-property-selection').webuiPopover('hide');

    var propertyid = $(linkid).attr('propertyid');
    $(linkid).attr('propertyid',$(this).attr('propertyid'));
    $(this).attr('propertyid',propertyid);
    var code = $(linkid).attr('code');
    $(linkid).attr('code',$(this).attr('code'));
    $(this).attr('code',code);
    var title = $(linkid).html();
    $(linkid).html($(this).html());
    $(this).html(title);
    var itemid = $(linkid).attr('id');
    $(linkid).attr('id',$(this).attr('id'));
    $(this).attr('id',itemid);

    $(listid).find('[actualid="'+ actualid + '"]').attr('actualid',newactualid);

    var data = {
        productid  : productid,
        orderid 	 : orderid,
        properties : [],
      };
    row.find('.product-property-selection').each( function( index, element ){
        data.properties.push( $(this).attr('propertyid') );
    });
    $('#product_'+posid+'_price').html('<i class="fa fa-refresh fa-spin"></i>');
    $('#product_'+posid+'_price').html( 'S/. '+productPrice(data) );
    @if($order)
    	updateProduct(data);
    @endif
    return false;
  });

  $('.select-product-text').on('click',function(e){
      @if($order)
      	return false;
      @endif
      e.preventDefault();
      var posid = $(this).attr('posid');
      toggleProduct(posid);
      return false;
    });

  $('.select-product-link').on('click',function(e){
      e.preventDefault();
      //selectedProduct($(this));
      addSelectedProduct($(this));
      return false;
    });

    // check if the product is available from menu
    function selectedProduct(item){
   		var product = getStorageItem(orderMenu,'products',item.attr('productid'));
   		var posid 	= item.attr('posid');
    	$.ajax({
	      method: "POST",
	      url: "{{ route('menus.product.available') }}",
	      data: {'product': JSON.stringify(product), 'address': JSON.stringify(getLocalStorage(orderAddress))},
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'ok'	: if(!data.data.available){
	      									toggleProduct(posid);
	      									showMessage('Aviso','El producto seleccionado no se encuentra disponible por el momento');
	      								}else{
	      									addSelectedProduct(item);
	      								}
	      								break;
	      		default		: showAlert('',data.data.message);
	      	}
				},
	      error: function (){
	      	showAlert('Error','Ocurrió un error al intentar verificar el producto');
	      }
	    });
    }

    function addSelectedProduct(item){
    	var productid = item.attr('productid');
      var posid 		= item.attr('posid');
      var orderid 	= item.attr('orderid');
      var row = $('#product-row-'+posid);

      row.addClass('add-product-cart').delay(2000).queue(function(){
          item.removeClass('add-product-cart').dequeue();
      });
      toggleProduct(posid);
      var data = {
        productid  : productid,
        orderid 	 : orderid,
        properties : [],
      };
      row.find('.product-property-selection').each( function( index, element ){
          data.properties.push( $(element).attr('propertyid') );
      });
     	addProduct(data);
    }

    function toggleProduct(posid){
      var linkid = '.product-link-'+posid;
      var icon = $('#product-radio-'+posid);
      $(linkid).toggle();

      if(icon.hasClass('fa-dot-circle-o')){
        icon.removeClass('fa-dot-circle-o').addClass('fa-circle-thin');
      }else{
        icon.removeClass('fa-circle-thin').addClass('fa-dot-circle-o');
      }
    }

    function addProduct(data){
    	if(addToCart(data)){
    		showToast('Ok','Producto agregado');
    		//window.location = "{{ route('orders.actual',['token'=>$token]) }}";
    	}else{
    		showMessage('','No se pudo agregar el producto a tu orden, intenta nuevamente');
    	}
    	return false;
    }
    function updateProduct(data){
    	if(updateCart(data)){
    		showToast('','Producto actualizado',1);
    	}else{
    		showMessage('','No se pudo actualizar el producto, intenta nuevamente');
    	}
    	return false;
    }

    @if(!$order)
    $('.item-product-row').swipe({ //add product to cart when swipe to right
      swipeRight:function(event, direction, distance, duration, fingerCount) {
      	var posid = $(this).attr('posid');
        toggleProduct(posid);
        //selectedProduct($(this));
        addSelectedProduct($(this));
      },
      threshold:5
    });
    @endif

    (function($) {
			$.fn.spinner = function() {
				this.each(function() {
					var el = $(this);

					// add elements
					el.wrap('<span class="spinner"></span>');
					el.before('<span class="sub">-</span>');
					el.after('<span class="add">+</span>');

					// substract
					el.parent().on('click', '.sub', function () {
						if (el.val() > parseInt(el.attr('min')))
							el.val( function(i, oldval) { return --oldval; });
					});

					// increment
					el.parent().on('click', '.add', function () {
						if (el.val() < parseInt(el.attr('max')))
							el.val( function(i, oldval) { return ++oldval; });
					});
			    });
			};
		})(jQuery);

		$('.input-spinner').spinner();
</script>