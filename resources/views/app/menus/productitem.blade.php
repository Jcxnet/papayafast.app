@if (isset($product))
	<div class="flipcard" id="card-{{$pos}}">
			<div class="face front">
					<ons-row class="item-product-row" id="product-row-{{$pos}}" posid="{{$pos}}" productid="{{$product['id']}}" orderid="{{$product['orderid']}}">
					 	<ons-row class="item-product-row-info">
						 	<ons-col width="98px" align="center">
						    @if( count($product['image'])>0 AND $product['image'] !== FALSE )
						      <img src="{{ $product['image'][0] }}" class="circle-image">
						    @else
						      <img src="{{ asset('img/menu/0.jpg') }}" class="circle-image">
						    @endif
						    <br/>
						    <div class="text-center clearfix">
						    	<a href="#" class="select-product-text" productid="{{$product['id']}}" orderid="{{$product['orderid']}}" posid="{{$pos}}"><i class="fa fa-circle-thin text-blue" id="product-radio-{{$pos}}"></i></a>
						    </div>
						  </ons-col >
						  <ons-col >
						  		<div class="text-uppercase clearfix">
						      	<a href="#" class="link-menu text-black font-type-1 select-product-text" productid="{{$product['id']}}" orderid="{{$product['orderid']}}" posid="{{$pos}}"> {{ $product['title'] }} </a>
						      </div>
						      <span class="text-normal text-muted clearfix"> {{ $product['detail'] }} </span>
						      @include ('app.menus.properties',['properties' => $product['properties'],'product_id'=>$product['id'],'pos'=>$pos])
						      @include ('app.menus.groups',['groups' => $product['groups'],'product_id'=>$product['id'],'pos'=>$pos])
						  </ons-col>
						 </ons-row>
						 <ons-row align="center" class="spin-row" posid="{{$pos}}">
							 	<ons-col width="50px">
						  		<a href="#" class="spin-sub"><i class="fa fa-minus"></i></a>
							  </ons-col>
							  <ons-col align="center">
							  	<div class="text-center">
							  		<input type="hidden" min="1" max="10" value="{{$product['cantidad']}}" id="product-total-{{$pos}}" />
							  		<input type="hidden" value="{{$product['price']}}" id="product-price-{{$pos}}" />
							  		@if($order)
							  			<span class="text-black pull-right product-price" id="product_{{$pos}}_price"><span class="notification">{{$product['cantidad']}} </span> S/. {{sprintf("%.2f",$product['price'])}}</span>
							  			<a href="#" class="btn-product-options" id="btn-row-options-{{$pos}}" posid="{{$pos}}"><i class="fa fa-bars"></i></a>
							  		@else
							  			<span class="text-black pull-right product-price" id="product_{{$pos}}_price">S/. {{sprintf("%.2f",$product['price'])}}</span>
							  		@endif
							  	</div>
							  </ons-col>
							  <ons-col width="50px">
							  		<a href="#" class="spin-add pull-right"><i class="fa fa-plus"></i></a>
							  </ons-col>
						 </ons-row>
					</ons-row>

	      </div>
	      <div class="face back product-back-side" posid="{{$pos}}" id="back-side-{{$pos}}">
					@if($order)
					<a href="#" class="btn-product-options btn-return-front" id="btn-back-options-{{$pos}}" posid="{{$pos}}"><i class="fa fa-reply"></i></a>
					<ons-row>
						<ons-col width="98px" align="center">
					    <br/>
					    @if( count($product['image'])>0 AND $product['image'] !== FALSE )
					      <img src="{{ $product['image'][0] }}" class="circle-image">
					    @else
					      <img src="{{ asset('img/menu/0.jpg') }}" class="circle-image">
					    @endif
					  </ons-col >
					  <ons-col align="center">
					  		<br/>
					  		<div class="text-uppercase clearfix">
					      	<a href="#" class="link-menu text-black font-type-1" productid="{{$product['id']}}" orderid="{{$product['orderid']}}" posid="{{$pos}}">
					      		<span class="notification">{{$product['cantidad']}}</span>&nbsp; {{ $product['title'] }}
					      	</a>
					      </div>
							  <ul class="list-circle-icons">
						      <!-- <li class="option-divide">
						        <a href="#" class="link-button" posid="{{$pos}}" orderid="{{$product['orderid']}}" nitem="{{$product['title']}}" title="Dividir">
						          <i class="fa fa-scissors hover-yellow"></i>
						        </a>
						      </li> -->
						      <li>
						        <a href="#" class="btnDuplicate link-button" posid="{{$pos}}" orderid="{{$product['orderid']}}" nitem="{{$product['title']}}" title="Duplicar">
						          <i class="fa fa-clone hover-blue"></i>
						        </a>
						      </li>
						      <li>
						        <a href="#" class="btnDelete link-button" posid="{{$pos}}" orderid="{{$product['orderid']}}" nitem="{{$product['title']}}" title="Eliminar">
						          <i class="fa fa-trash-o hover-red"></i>
						        </a>
						      </li>
						    </ul>
					  </ons-col>
					</ons-row>
					@endif
	      </div>

	</div>

	<ons-row id="product-row-border-{{$pos}}"><div class="item-product-border">&nbsp;</div></ons-row>
@endif

<script>

	/*$('a.product-property-selection').webuiPopover({
        animation:'pop',
        multi:true,
        backdrop:false,
        placement:'vertical'
      });

  $('a.product-property-selectable').on('click',function(e){
  	e.preventDefault();
    var productid   = $(this).attr('productid');
    var posid       = $(this).attr('posid');
    var groupid     = $(this).attr('groupid');
    var actualid    = $(this).attr('actualid');
    var newactualid = $(this).attr('propertyid');
    var linkid      = '#product_'+posid+'_group_'+groupid+'_prop_'+actualid;
    var listid      = '#product_'+posid+'_group_'+groupid+'_props_'+productid;
    var row       	= $('#product-row-'+posid);
    var orderid 		= $(row).attr('orderid');
    var cantidad 		= parseInt($('#product-total-'+posid).val());

    $('a.product-property-selection').webuiPopover('hide');

    var propertyid = $(linkid).attr('propertyid');
    $(linkid).attr('propertyid',$(this).attr('propertyid'));
    $(this).attr('propertyid',propertyid);
    var code = $(linkid).attr('code');
    $(linkid).attr('code',$(this).attr('code'));
    $(this).attr('code',code);
    var title = $(linkid).html();
    $(linkid).html($(this).html());
    $(this).html(title);
    var itemid = $(linkid).attr('id');
    $(linkid).attr('id',$(this).attr('id'));
    $(this).attr('id',itemid);

    $(listid).find('[actualid="'+ actualid + '"]').attr('actualid',newactualid);

    var data = {
        productid  : productid,
        orderid 	 : orderid,
        properties : [],
        cantidad 	 : cantidad,
      };
    row.find('.product-property-selection').each( function( index, element ){
        data.properties.push( $(this).attr('propertyid') );
    });
    $('#product_'+posid+'_price').html('<i class="fa fa-refresh fa-spin"></i>');
    //$('#product_'+posid+'_price').html( 'S/. '+productPrice(data) );
    $('#product-price-'+posid).val(productPrice(data));
    showPriceLabel(posid);

    @if($order)
    	updateProduct(data);
    @endif
    return false;
  });*/


</script>