<!--
@if (count($page['image'])>0)
<ons-row >
  <ons-col>
      <img src="{{ $page['image'][0] }}" class="ri text-center"  alt="" />
  </ons-col>
</ons-row>
@endif
-->

@if($page['buttons'] !== false )
	@include ('app.menus.buttonload',['buttons'=>$page['buttons'],'token'=>$token])
@endif

@if($page['products'] !== false )
 	@include ('app.menus.productload',['products'=>$page['products'],'token'=>$token])
@endif