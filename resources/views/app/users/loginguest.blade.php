@extends('layouts.app')

@section('content')
<ons-page>
   	@include ('app.includes.toolbar')
   <ons-row class="height-80">
     <ons-col width="80%" align="center" class="center-block">
      <div class="logo-xs"></div>

        <form class="form-horizontal" role="form" name="frmGuest" id="frmGuest" method="POST" action="#">
        <input type="hidden" name="remember" id="remember" value="1" />
          <div class="inputbox">
            <div class="input-container center-block" >
              <input type="text" id="name" name="name" placeholder="Escribe tu nombre" class="font-type-2" required/>
              <button class="icon" tabindex="-1"><i class="fa fa-user"></i></button>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="inputbox">
            <div class="input-container center-block">
              <input type="email" id="email" name="email" placeholder="Escribe tu email" class="font-type-2" required/>
              <button class="icon" tabindex="-1"><i class="fa fa-envelope-o"></i></button>
            </div>
          </div>
          <div class="menu-item-border clearfix"></div>
          <div class="center-block" >
             <button type="submit" class="button button--large" style="margin-left:8px;">
              <i class="fa fa-btn fa-sign-in"></i> &nbsp; Continuar como invitado
             </button>
          </div>
          <a href="{{ route('users.login.form') }}" class="button button--large--quiet text-desc clearfix" >Ya tienes una cuenta?</a>
          <a href="{{ route('users.register.form') }}" class="button button--large--quiet text-desc clearfix" >Olvidaste tu contraseña?</a>
        </form>
      </ons-col>
    </ons-row>
</ons-page>
<ons-modal id="myModal">
  <ons-icon icon="ion-load-c" spin="true"></ons-icon>
  <br>
  <br> Espere un momento...
</ons-modal>
@endsection

@push('scripts')
<script>
	jQuery(document).ready(function($) {

		$('#frmGuest').on('submit',function(e){
			e.preventDefault();
			guestUser(this);
			return false;
		});

		function guestUser(form){
			var modal = $('#myModal');
      $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	    $.ajax({
			  method: "POST",
			  url: "{{ route('users.guest') }}",
			  data:  $(form).serialize() ,
			  beforeSend:function(){
			  	modal.show();
			  },
	    	success:function(data){
	    		modal.hide();
	    		switch(data.meta.status){
	    			case 'update'	: setLocalData(userStorage,'data',data.data.userdata);
	    											setLocalData(tokenStorage,'id',data.data.token);
	    											window.location = data.data.url;
	    											break;
						case 'ok'			: window.location = data.data.url;
														break;
	    			case 'error'	: showMessage('Error',data.data.message);
	    											break;
	    			default: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
	    		}
				},
				error: function(){
					modal.hide();
					showMessage('Error','El servicio de asignación de usuarios se encuentra ocupado, intente nuevamente');
				}
			});
		}

	});
</script>
@endpush