@extends('layouts.app')

@section('content')
<ons-page>
   	@include ('app.includes.toolbar')

   	<ons-row align="center">
   		<ons-col align="center" width="180px">
   			<img src="{{$image}}" alt="" class="circle-image-user" id="user-image" />
   		</ons-col>
   		<ons-col>
   			<span class="text-subtitle text-white text-left">{{$name}}</span><br>
   			<span class="text-left"><a class="link-menu text-normal" href="{{route('users.profile',['token'=>$token])}}">Editar perfil</a></span>
   		</ons-col>
   	</ons-row>
   	<br/>
   	<a href="{{route('main.menu',['token'=>$token])}}" class="no-underline text-white text-subtitle">
   		<ons-row >
   			<ons-col width="50px">
	   			<div class="text-center">
	   				<i class="fa fa-home"></i>
	   			</div>
	   		</ons-col>
	   		<ons-col align="left">
	   			<span class="">Inicio</span>
	   		</ons-col>
   			<ons-col align="right">
   				<i class="fa fa-chevron-right"></i>
   			</ons-col>
   		</ons-row>
   	</a>
   	<br/>
   	<a href="{{route('orders.history',['token'=>$token])}}" class="no-underline text-white text-desc">
   		<ons-row >
   			<ons-col width="50px">
	   			<div class="text-center">
	   				<i class="fa fa-list-alt"></i>
	   			</div>
	   		</ons-col>
	   		<ons-col align="left">
	   			<span class="">Mis pedidos</span>
	   		</ons-col>
   			<ons-col align="right">
   				<i class="fa fa-chevron-right"></i>
   			</ons-col>
   		</ons-row>
   	</a>
   	<br/>
   	<a href="{{ route('addresses.view',['token'=>$token,'delivery'=>0]) }}" class="no-underline text-white text-desc">
   		<ons-row >
   			<ons-col width="50px">
	   			<div class="text-center">
	   				<i class="fa fa-map-signs"></i>
	   			</div>
	   		</ons-col>
	   		<ons-col align="left">
	   			<span class="">Mis direcciones</span>
	   		</ons-col>
   			<ons-col align="right">
   				<i class="fa fa-chevron-right"></i>
   			</ons-col>
   		</ons-row>
   	</a>
   	<br/>
   	<a href="{{ route('locals.user',['token'=>$token]) }}" class="no-underline text-white text-desc">
   		<ons-row >
   			<ons-col width="50px">
	   			<div class="text-center">
	   				<i class="fa fa-star-o"></i>
	   			</div>
	   		</ons-col>
	   		<ons-col align="left">
	   			<span class="">Mis locales favoritos</span>
	   		</ons-col>
   			<ons-col align="right" width="50px">
   				<i class="fa fa-chevron-right"></i>
   			</ons-col>
   		</ons-row>
   	</a>
   	<ons-row align="center" >
			<ons-col >
				<div class="logo-xs">&nbsp;</div>
			</ons-col>
		</ons-row>
</ons-page>
@endsection
