@extends('layouts.app')

@section('content')
<ons-page>
   	@include ('app.includes.toolbar')
   	<form name="user-profile" id="user-profile" method="post" enctype="multipart/form-data" >
   		<input type="hidden" name="token" id="token" value="{{$token}}" />
   	<ons-row align="center">
   		<ons-col align="center">
   			<img src="{{$image}}" alt="" class="circle-image-user" id="user-image" />
   		</ons-col>
   		<ons-col align="center">

 				<input type="file" name="image" id="image" class="inputfile" data-multiple-caption="{count} files selected" />
				<label for="image" ><i class="fa fa-picture-o"></i>&nbsp;<span>Cambia tu imagen&hellip;</span></label>

 			</ons-col>
   	</ons-row>
   	<ons-row>
   		<ons-col>
   			<div class="inputbox">
          <div class="input-container center-block">
            <input type="text" id="name" name="name" value="{{ $name }}" class="font-type-2" required/>
            <button class="icon" tabindex="-1"><i class="fa fa-user"></i></button>
          </div>
        </div>
   		</ons-col>
   	</ons-row>
   	<ons-row>
   		<ons-col>
   			<div class="inputbox">
          <div class="input-container center-block">
            <input type="email" id="email" name="email" value="{{ $email }}" class="font-type-2" required/>
            <button class="icon" tabindex="-1"><i class="fa fa-envelope-o"></i></button>
          </div>
        </div>
        <div class="menu-item-border clearfix"></div>
   		</ons-col>
   	</ons-row>
   	<ons-row >
   		<ons-col>
   			<div class="text-center">
   				<input type="submit" class="button" value="Listo">
   			</div>
   		</ons-col>
   	</ons-row>
   	</form>
</ons-page>
<ons-modal id="myModal">
  <ons-icon icon="ion-load-c" spin="true"></ons-icon>
  <br>
  <br> Actualizando información...
</ons-modal>
@endsection

@push('scripts')
 <script>

	jQuery(document).ready(function($) {
	 	$('.inputfile' ).each( function(){
			var $input	 = $( this ),
				$label	 = $input.next( 'label' ),
				labelVal = $label.html();

			$input.on( 'change', function( e ){
				var fileName = '';
				if( this.files && this.files.length > 1 )
					fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
				else if( e.target.value )
					fileName = e.target.value.split( '\\' ).pop();

				if( fileName )
					$label.find( 'span' ).html( fileName );
				else
					$label.html( labelVal );
			});
			$input
			.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
			.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
		});
	});

 $("#user-profile").on("submit", function(e){
    e.preventDefault();
    var f = $(this);
    var formData = new FormData(document.getElementById("user-profile"));
    var modal = $('#myModal');
   	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      url: "{{route('users.update')}}",
      method: "POST",
      data: formData,
      //dataType: "html",
      cache: false,
      contentType: false,
			processData: false,
			beforeSend:function(){
			  modal.show();
			},
			success: function(data){
				modal.hide();
				if(data.meta.status=='ok'){
					$('#user-image').attr('src',data.data.image+'?v='+Math.round(new Date().getTime()/1000));
					window.location = "{{route('users.menu',['token'=>$token])}}";
				}else
					showMessage('Aviso',data.data.message);
			},
			error: function(){
				modal.hide();
				showMessage('Error','El servicio de datos de usuario se encuentra ocupado, intente nuevamente');
			}
   	});
    return false;
  });
</script>
@endpush