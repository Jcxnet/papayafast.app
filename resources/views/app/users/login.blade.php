@extends('layouts.app')

@section('content')
<ons-page>
   	@include ('app.includes.toolbar')
   <ons-row class="height-80">
     <ons-col width="80%" align="center" class="center-block">
      <div class="logo-xs"></div>

        <form class="form-horizontal" role="form" name="frmLogin" id="frmLogin" method="POST" action="#">
        <input type="hidden" name="remember" id="remember" value="1" />
          <div class="inputbox">
            <div class="input-container center-block">
              <input type="email" id="email" name="email" placeholder="Escribe tu email" value="{{ old('email') }}" class="font-type-2" required/>
              <button class="icon" tabindex="-1"><i class="fa fa-envelope-o"></i></button>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="inputbox">
            <div class="input-container center-block" >
              <input type="password" id="password" name="password" placeholder="Escribe tu contraseña" class="font-type-2" required/>
              <button class="icon" tabindex="-1"><i class="fa fa-lock"></i></button>
            </div>
          </div>
          <div class="menu-item-border clearfix"></div>
          <div class="center-block" >
             <button type="submit" class="button button--large" style="margin-left:8px;">
              <i class="fa fa-btn fa-sign-in"></i> &nbsp; Iniciar sesión
             </button>
          </div>
          <a href="{{ route('users.register.form') }}" class="button button--large--quiet text-desc clearfix" >Olvidaste tu contraseña?</a>
          <a href="{{ route('users.register.form') }}" class="button button--large--quiet text-desc clearfix" >No estás registrado?</a>
          <a href="{{ route('users.guest.form') }}" 	 class="button button--large--quiet text-desc clearfix" >Accede como invitado</a>
        </form>
      </ons-col>
    </ons-row>
</ons-page>
<ons-modal id="myModal">
  <ons-icon icon="ion-load-c" spin="true"></ons-icon>
  <br>
  <br> Espere un momento...
</ons-modal>
@endsection

@push('scripts')
<script>
	jQuery(document).ready(function($) {

		$('#frmLogin').on('submit',function(e){
			e.preventDefault();
			loginUser(this);
			return false;
		});

		function loginUser(form){
			var modal = $('#myModal');
      $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	    $.ajax({
			  method: "POST",
			  url: "{{ route('users.login') }}",
			  data:  $(form).serialize() ,
			  beforeSend:function(){
			  	modal.show();
			  },
	    	success:function(data){
	    		modal.hide();
	    		console.log(data);
	    		switch(data.meta.status){
	    			case 'update'	: setLocalData(userStorage,'data',data.data.userdata);
	    											setLocalData(tokenStorage,'id',data.data.token);
	    											window.location = data.data.url;
	    											break;
						case 'ok'			: window.location = data.data.url;
														break;
	    			case 'error'	: showMessage('Error',data.data.message);
	    											break;
	    			default: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
	    		}
				},
				error: function(){
					modal.hide();
					showMessage('Error','El servicio de verificación de usuarios se encuentra ocupado, intente nuevamente');
				}
			});
		}

	});
</script>
@endpush
