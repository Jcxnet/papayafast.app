@extends('layouts.app')

@section('content')
<ons-page>
   	@include ('app.includes.toolbar')

   <ons-row class="height-80">
     <ons-col width="80%" align="center" class="center-block">
      <div class="logo-xs"></div>
        <form class="form-horizontal" role="form" name="frmRegister" id="frmRegister" method="POST" action="#">
          <div class="inputbox">
            <div class="input-container center-block">
              <input type="text" id="name" name="name" placeholder="Escribe tu nombre" value="{{ old('name') }}" class="font-type-2" required/>
              <button class="icon" tabindex="-1"><i class="fa fa-user"></i></button>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="inputbox">
            <div class="input-container center-block">
              <input type="email" id="email" name="email" placeholder="Escribe tu email" value="{{ old('email') }}" class="font-type-2" required/>
              <button class="icon" tabindex="-1"><i class="fa fa-envelope-o"></i></button>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="inputbox">
            <div class="input-container center-block" >
              <input type="password" id="password" name="password" placeholder="Escribe tu contraseña" class="font-type-2" required/>
              <button class="icon" tabindex="-1"><i class="fa fa-lock"></i></button>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="inputbox">
            <div class="input-container center-block" >
              <input type="password" id="password_confirmation" name="password_confirmation" placeholder="Repite tu contraseña" class="font-type-2" required/>
              <button class="icon" tabindex="-1"><i class="fa fa-lock"></i></button>
            </div>
          </div>
          <div class="menu-item-border clearfix"></div>
          <div class="center-block" >
             <button type="submit" class="button button--large" style="margin-left:8px;">
              <i class="fa fa-btn fa-check"></i> &nbsp;Listo
             </button>
          </div>
          <a href="{{ route('users.login') }}" class="button button--large--quiet text-desc clearfix" >Ya estás registrado?</a>
        </form>
      </ons-col>
    </ons-row>
</ons-page>
@endsection

@push('scripts')
<script>
	jQuery(document).ready(function($) {

		$('#frmRegister').on('submit',function(e){
			e.preventDefault();
			registerUser(this);
			return false;
		});

		function registerUser(form){
      $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	    $.ajax({
			  method: "POST",
			  url: "{{ route('users.register') }}",
			  data:  $(form).serialize() ,
	    	beforeSend:function(){
	    		$('body').addClass('fulloverlay');
	    	},
	    	success:function(data){
	    		$('body').removeClass('fulloverlay');
	    		switch(data.meta.status){
	    			case 'ok'			: window.location = data.data.url;
	    											break;
	    			case 'error'	: showMessage('Error',data.data.message);
	    											break;
	    			default: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
	    		}
				},
				error: function(){
					$('body').removeClass('fulloverlay');
					showMessage('Error','El servicio de registro de usuarios se encuentra ocupado, intente nuevamente');
				}
			});
		}

	});
</script>
@endpush