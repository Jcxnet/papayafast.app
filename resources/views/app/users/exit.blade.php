<html>
	<head>
		<script src="{{ asset('lib/jquery/jquery-2.2.0.min.js') }}"></script>
		<script src="{{ asset('lib/jquery/js.cookie.js') }}"></script>
		<script src="{{ asset('lib/jquery/jquery.storageapi.js') }}"></script>
    <script src="{{ asset('lib/js/common.js') }}"></script>
		<script>
			deleteAllLocalData(userStorage);
			deleteAllLocalData(tokenStorage);
			window.location = "{{$url}}";
		</script>
	</head>
</html>