<?php $delivery = isset($delivery)?$delivery:false; ?>
@extends('layouts.app')

@push('styles')
  <link rel="stylesheet" href="{{ asset('css/magic.css') }}" />
@endpush

@section('content')

<ons-page>
   @include ('app.includes.toolbar')
    <ons-row>
	    <ons-col>
	    	@if(!$delivery)
		    <div class="alert alert-info alert-dismissible" role="alert">
		      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		      <div class="pulse-group font-type-2 text-normal">
		        <i class="fa fa-chevron-right icon-pulse "></i>
		        <i class="fa fa-chevron-right icon-pulse "></i>
		        <i class="fa fa-chevron-right icon-pulse "></i>
		        &nbsp;Desliza los ítems a la derecha para ver más opciones.
		      </div>
		    </div>
				@else
				<div class="alert alert-success" role="alert">
		      <div class="pulse-group font-type-2 text-normal">
		        <i class="fa fa-chevron-right icon-pulse "></i>
		        <i class="fa fa-chevron-right icon-pulse "></i>
		        <i class="fa fa-chevron-right icon-pulse "></i>
		        &nbsp;Selecciona la dirección a donde enviaremos tu pedido.
		      </div>
		    </div>
		    @endif
	    </ons-col>
    </ons-row>
    <ons-row>
    	<ons-col>
    		<ul class="list" id="address-list-div" ></ul>
    	</ons-col>
    </ons-row>
    @if(!$delivery)
    <ons-row class="border-top">
      <ons-col>
        <div class="text-center">
        	<a href="{{ route('addresses.add.form',['token'=>$token]) }}" class="button" id="btnReady"><i class="fa fa-plus-circle"></i> Añadir dirección</i></a>
        </div>
      </ons-col>
    </ons-row>
    @endif
</ons-page>

@endsection

@push('scripts')
  <script >

  $('.link-address-menu').on('click',function(){
    var data = {
        addressid : $(this).attr('addressid'),
        localid   : $(this).attr('localid'),
      };
    var dataurl = $(this).attr('dataurl');
    $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('addresses.list') }}",
      data: data,
      success: function(data){
          if(data.meta.status == 'ok'){
             window.location = dataurl;
          }
          if(data.meta.status == 'alert'){
            ons.notification.confirm({
              message: 'Si cambias de dirección algunos productos de tu pedido actual pueden variar, ¿Deseas cambiar la dirección de tu pedido actual?',
              title: '¿Cambio de Dirección?',
              buttonLabels: ['Cambiar', 'Ver mi pedido'],
              animation: 'fade',
              primaryButtonIndex: 0,
              cancelable: false,
              callback: function(index) {
                if(index == -1 || index == 1) window.location = '{{ route("addresses.list") }}';
                if(index == 0) window.location = dataurl;
              }
            });

          }
        }
    });
  });

  @if(!$delivery)
  function buttonEdit(){
  	$('.btnEdit').on('click',function(e){
	    e.preventDefault();
	    var idaddress = $(this).attr('iditem');
	    var dataurl   = $(this).attr('dataurl');

	    $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	    $.ajax({
	      method: "POST",
	      url: "{{ route('addresses.inuse') }}",
	      data: {'id': idaddress, 'storage':getLocalData(addressesStorage,'data')},
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'inuse': showMessage('Dirección en uso','Parece que hay un pedido pendiente para esta dirección, por ahora no puedes editarla');
	      									break;
	      		case 'ok' 	: window.location = dataurl;
	      									break;
	      		default 		: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
	      	}
	      },
	      error: function (){
	      	showMessage('Error','Ocurrió un error al intentar editar la dirección');
	      }
	    });
	    return false;
	  });
  }


  function buttonDelete(){
  	$('.btnDelete').on('click',function(e){
	    e.preventDefault();
	    var idaddress = $(this).attr('iditem');
	    var naddress  = $(this).attr('nitem');
	    $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	    $.ajax({
	      method: "POST",
	      url: "{{ route('addresses.inuse') }}",
	      data: {'id': idaddress, 'storage':getLocalData(addressesStorage,'data')},
	      success: function(data){
	      	switch (data.meta.status){
	      		case 'inuse': showMessage('Dirección en uso','Parece que hay un pedido pendiente para esta dirección, por ahora no puedes eliminarla');
	      									break;
	      		case 'ok'		: ons.notification.confirm({
												    messageHTML: '¿Deseas eliminar la dirección <br/><strong>'+naddress+'</strong><br/>de la lista ?',
												    title: 'Eliminar dirección',
												    animation: 'fade',
												    buttonLabels: ['Eliminar', 'Cancelar'],
												    primaryButtonIndex: 0,
												    cancelable: false,
												    callback: function(index) {
												      if(index == 0)
												      	removeAddress(idaddress);
												    }
												  });
	      									break;
	      		default 		: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
	      	}

	      	}
	    });
	    return false;
  	});
  }


  function swipeAddress(){
    $(".swipe-main").swipe( {
      swipeRight:function(event, direction, distance, duration, fingerCount) {
        $(this).addClass('swipe-main-move');
        $(this).find(".swipe-menu").addClass('swipe-menu-active').toggleClass('magictime tinLeftIn');
      },
      swipeLeft:function(event, direction, distance, duration, fingerCount) {
        $(this).removeClass('swipe-main-move');
        $(this).find(".swipe-menu").removeClass('swipe-menu-active').toggleClass('magictime tinLeftIn');
      },
      threshold:10
    });

    $(".swipe-menu").swipe( {
      swipeLeft:function(event, direction, distance, duration, fingerCount) {
        $(this).removeClass('swipe-menu-active').toggleClass('magictime tinLeftIn');
        $(this).parent(".swipe-main").removeClass('swipe-main-move');
      },
      threshold:10
    });
  }

  function removeAddress(id){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('addresses.delete') }}",
      data: {"id": id, 'storage': getLocalData(addressesStorage,'data'),'token':'{{$token}}'},
      success: function(data){
        if(data.meta.status == 'ok'){
        		setArrayLocalData(addressesStorage,data.data.storage);
        		showToast('Dirección eliminada');
           	$('#address_item_'+id).fadeTo("slow", 0.01, function(){ //fade
               $(this).slideUp("slow", function() { //slide up
                   $(this).remove(); //then remove from the DOM
               });
           });
        }
      }
    });
  }
  @endif

  function showAddress(){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('addresses.list') }}",
      data: {'data': getLocalData(addressesStorage,'data'),'token':'{{$token}}','delivery':'{{($delivery)?1:0}}'},
      success: function(data){
      	switch(data.meta.status){
      		case 'html'	: $('#address-list-div').html(data.data.html);
      									@if (!$delivery)
      									swipeAddress();
      									buttonEdit();
      									buttonDelete();
      									@endif
      									break;
      		case 'error': showMessage('Error',data.data.message);
	    								 	break;
					case 'redirect': window.location = data.data.url;
													 break;
	    		default: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
      	}
      },
      error: function(){
      	showMessage('','Ocurrió un error al intentar mostrar las direcciones');
      }
    });
  }

    jQuery(document).ready(function($) {
      showAddress();
   });

  </script>
@endpush