@extends('layouts.app')

@section('content')

<ons-page>
  @include ('app.includes.toolbar')

  @inject('map', 'App\Http\Controllers\MapsController')
  {!! $map->address( (isset($address['point']['lat']))?$address['point']['lat']:false, (isset($address['point']['lng']))?$address['point']['lng']:false ) !!}

  <ons-row  >
    <ons-col width="98%">
      <input type="hidden" name="idaddress" id="idaddress" value="{{isset($address['id'])?$address['id']:''}}" />

      <div class="inputbox">
        <div class="input-container center-block">
          <input type="text" id="name" name="name" placeholder="Nombre para la dirección" value="{{ isset($address['name'])?$address['name']:'' }}" class="font-type-2" required />
          <button class="icon" tabindex="-1"><i class="fa fa-tag"></i></button>
        </div>
        <div class="input-container center-block">
          <input type="text" id="phone" name="phone" placeholder="Teléfono" value="{{ isset($address['phone'])?$address['phone']:'' }}" class="font-type-2" required />
          <button class="icon" tabindex="-1"><i class="fa fa-phone"></i></button>
        </div>
      </div>
      <div class="clearfix"></div>
    </ons-col>
  </ons-row>

  <ons-row>
    <ons-col>
    	<div class="text-center">
        <a href="#" class="button" id="btnSave" >
        		@if(isset($id))
        				<i class="fa fa-pencil-square-o"></i> Guardar cambios
        		@else
        				<i class="fa fa-plus-circle"></i> Añadir dirección
        		@endif
        </a>
      </div>
    </ons-col>
  </ons-row>

  <ons-row>
    <ons-col>
      <div id="local_results"></div>
    </ons-col>
  </ons-row>

</ons-page>

@endsection

@push('scripts')
  <script >
  jQuery(document).ready(function($) {

	  $("#btnSave").on("click",function(e){
	    e.preventDefault();
	    $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	    $.ajax({
	      method: "POST",
	      url: "{{ route('addresses.add') }}",
	      data: {"id": $('#idaddress').val(), "txtlat":$('#txtlat').val(), "txtlng":$('#txtlng').val(), "address":$('#address').val(), "name":$('#name').val(), "phone":$('#phone').val(),'token':'{{$token}}','storage':getLocalData(addressesStorage,'data')},
	      beforeSend: function(){
	      	$('#local_results').html('');
	      },
	      success: function(data) {
	        switch(data.meta.status){
	        	case 'ok'			: window.location = data.data.url;
	          								break;
	        	case 'nocover': showMessage('Aviso','La dirección que ingresaste se encuentra fuera de cobertura; sin embargo puedes acercarte a alguno de los locales que te mostramos a continuación');
	        									/* la dirección no se encuentra en cobertura -> muestra locales más cercanos*/
	            							$('#local_results').html(data.data.locals);
	            							break;
	          case 'error'	: showMessage('Error',data.data.message);
	          								break;
	          case 'storage': setArrayLocalData(addressesStorage,data.data.storage);
	          								window.location = data.data.url;
	          								break;
	          case 'login'	: window.location = data.data.url;
	          								break;
	          default: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
	        }
	      },
	      error: function(){
					showMessage('Error','Ocurrió un error al intentar guardar la dirección');
					}
			});
	  	return false;
	  });

	});

	function updateMarker(){
			marker.setPosition({lat:parseFloat($('#txtlat').val()),lng:parseFloat($('#txtlng').val())});
			map.setCenter({lat:parseFloat($('#txtlat').val()),lng:parseFloat($('#txtlng').val())});
			map.panTo({lat:parseFloat($('#txtlat').val()),lng:parseFloat($('#txtlng').val())});
		}

</script>

<script>
  @if (isset($id) AND !isset($address['id']))
		jQuery(document).ready(function($) {
		  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
		  $.ajax({
		    method: "POST",
		    url: "{{ route('addresses.get') }}",
		    data: {'id':"{{$id}}",'data': getLocalData(addressesStorage,'data') },
		    success: function(data){
		    	if(data.meta.status=='ok'){
		    		$('#idaddress').val(data.data.id);
		    		$('#name').val(data.data.name);
		    		$('#phone').val(data.data.phone);
		    		$('#address').val(data.data.address);
		    		$('#txtlat').val(data.data.point.lat);
		    		$('#txtlng').val(data.data.point.lng);
		    		updateMarker();
		    	}
		    }
		  });
		});
	@endif
</script>
@endpush