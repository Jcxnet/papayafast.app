<ons-row>
<ons-col >
  <div class="text-center clearfix">
      <a href="{{ route('menus.view.page',['page'=>$page,'token'=>$token]) }}" class="button button--large--quiet text-desc text-danger text-uppercase">Añadir productos</a>
  </div>
</ons-col>
</ons-row>

<ons-row>
  <ons-col >
    <div class="alert alert-warning text-normal" role="alert">
      Enviar mi pedido a <br/><strong>{{$address['address']}}</strong><br/> y confirmar al número <br/><strong>{{$address['phone']}}</strong>
    </div>
    <div class="text-center clearfix">
      <a href="{{route('addresses.delivery',['token'=>$token])}}" class=" text-desc text-info text-uppercase no-underline">Cambiar de direcci&oacute;n</a>
    </div>
    <br/>
  </ons-col>
 </ons-row>