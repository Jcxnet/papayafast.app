<?php $delivery = isset($delivery)?$delivery:false; ?>
@if(count($addresses)>0)
	@foreach($addresses as $address)
		@if(!$address['deleted'])
		@if (!$delivery)
		<ons-row id="address_item_{{$address['id']}}" class="swipe-main border-top">
		@else
		<ons-row id="address_item_{{$address['id']}}" class="border-top">
		@endif
		  @if(!$delivery)
		  <div class="swipe-menu">
	      <ul class="list-circle-icons">
	        <li>
	          <a href="#" dataurl="{{ route('addresses.add.form',['token'=>$token,'id'=>$address['id']]) }}" class="btnEdit link-button" iditem="{{$address['id']}}" title="Modificar">
	            <i class="fa fa-pencil hover-blue"></i>
	          </a>
	        </li>
	        <li>
	          <a href="#" class="btnDelete link-button" iditem="{{$address['id']}}" nitem="{{$address['address']}}" title="Eliminar">
	            <i class="fa fa-trash-o hover-red"></i>
	          </a>
	        </li>
	      </ul>
		  </div>
		  @endif
		  <ons-col>
		    <li class="list-item-container">
		      <div class="menu-item-border">
		        <div class="text-title font-type-1 text-uppercase clearfix">
		        @if($delivery)
		        <a href="{{ route('menus.load',['address'=>$address['id'],'local'=>$address['local']['id'],'token'=>$token]) }}" class="link-address-menu text-black link-menu" addressid="{{ $address['id'] }}" localid="{{ $address['local']['id'] }}">{{ $address['name'] }}</a>
		        @else
		        <a href="#" class="link-address-menu text-black link-menu" addressid="{{ $address['id'] }}" localid="{{ $address['local']['id'] }}" dataurl="{{ route('main.menu',['id'=>$address['local']['id'], 'address' => $address['id']]) }}">{{ $address['name'] }}</a>
		        @endif
		        </div>
		        <span class="text-subtitle text-muted clearfix">{{ $address['address'] }}</span>
		        <span class="text-subtitle text-muted clearfix">{{ $address['phone'] }}</span>
		      </div>
		    </li>
		  </ons-col>
		</ons-row>
		@endif
	@endforeach
@endif