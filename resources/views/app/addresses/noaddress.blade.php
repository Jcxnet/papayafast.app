<ons-row>
  <ons-col >
    <div class="alert alert-danger text-normal" role="alert">
      Debes seleccionar una dirección para enviar tu pedido.
    </div>
    <div class="text-center clearfix">
      <a href="{{route('addresses.delivery',['token'=>$token])}}" class="text-desc text-info text-uppercase no-underline">Seleccionar dirección</a>
    </div>
    <br/>
  </ons-col>
 </ons-row>