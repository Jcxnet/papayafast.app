<section>
	<nav>
		<ol class="cd-multi-steps text-top count text-high">
			@foreach($steps as $step => $status)
			<li class="{{$status}}"><a href="#">{{$step}}</a></li>
			@endforeach
		</ol>
	</nav>
</section>