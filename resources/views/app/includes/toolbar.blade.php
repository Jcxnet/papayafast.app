<?php
  if (isset($body_class)){
    $title_color = ($body_class=='body-bg-white')?'text-black':'text-white';
  }else{
    $title_color = 'text-white';
  }
  $title_color = 'text-black';
  $url = isset($back_link)?$back_link:URL::previous();
  $token = isset($token)?$token:null;
?>

<ons-toolbar>
  <div class="left">
  	<ons-toolbar-button onclick="showSlideMenu();" id="slide_button"><ons-icon icon="md-menu"></ons-icon></ons-toolbar-button>
  </div>
  <div class="center">
  	<div class="font-type-1 text-desc">{!! isset($page_title)?$page_title:'Pardos Chicken' !!}</div>
  </div>
  <div class="right">
 		<ons-toolbar-button onclick="window.location='{{$url}}';" style="margin-right:65px;"><ons-icon icon="chevron-circle-left"></ons-icon></ons-toolbar-button>
 		<ons-speed-dial position="right top" direction="down">
	    <ons-toolbar-button><ons-icon icon="gear"></ons-icon></ons-toolbar-button>
	    <ons-speed-dial-item>
	    	<a href="{{route('main.menu',['token'=>$token])}}"><ons-icon icon="home" class="text-white"></ons-icon></a>
	    </ons-speed-dial-item>
	    <ons-speed-dial-item>
	    	<a href="{{route('orders.actual',['token'=>$token])}}"><ons-icon icon="list-alt" class="text-white"></ons-icon></a>
	    </ons-speed-dial-item>
	    @if ($token)
	    	<ons-speed-dial-item>
	    		<a href="{{route('users.menu',['token'=>$token])}}"><ons-icon icon="user" class="text-white"></ons-icon></a>
	    	</ons-speed-dial-item>
	    	<ons-speed-dial-item>
  				<a href="{{route('users.logout',['token'=>$token])}}"><ons-icon icon="power-off" class="text-white"></ons-icon></a>
  			</ons-speed-dial-item>
	    @else
	    	<ons-speed-dial-item>
	    		<a href="{{ route('users.login.form') }}"><ons-icon icon="user" class="text-white"></ons-icon></a>
	    	</ons-speed-dial-item>
	    @endif
	  </ons-speed-dial>
  </div>

</ons-toolbar>
@push('scripts')
  <script>

  function showSlideMenu(){
  	$('#slide_menu').css({"margin-left": "0", "z-index": "100" });
  }

  </script>
@endpush
