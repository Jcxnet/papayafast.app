<?php
	$isToken  = isset($token);
	if(!$isToken)
		$token = null;
?>

<nav id="slide_menu">
  <ul>
  	<li class="clearfix"><a href="#" id="close_slide" class="text-unbold no-border pull-right"><ons-icon icon="md-menu"></ons-icon></a></li>
    @if ($isToken)
			<li><a href="{{ route('main.menu',['token'=>$token]) }}" class="text-unbold" ><i class="fa fa-home"></i> Inicio</a></li>
		@else
			<li><a href="{{ route('main.menu') }}" class="text-unbold"><i class="fa fa-home"></i> Inicio</a></li>
		@endif
			<li><a href="{{route('orders.actual',['token'=>$token])}}"><i class="fa fa-list-alt"></i> Pedido actual</a></li>
		@if ($isToken)
			<li><a href="{{ route('addresses.view',['token'=>$token,'delivery'=>0]) }}" ><i class="fa fa-map-signs"></i> Mis direcciones</a></li>
			<li><a href="{{ route('users.menu',['token'=>$token])}}" ><i class="fa fa-user"></i> Mis preferencias</a></li>
			<li><a href="{{ route('orders.history',['token'=>$token])}}" ><i class="fa fa-history"></i> Mis pedidos</a></li>
			<li><a href="{{ route('users.logout',['token'=>$token]) }}" ><i class="fa fa-power-off"></i> Salir</a></li>
		@else
			<li><a href="{{ route('addresses.view') }}" ><i class="fa fa-map-signs"></i></i> Direcciones</a></li>
			<li><a href="{{ route('users.login.form') }}" ><i class="fa fa-lock"></i> Inicia sesión</a></li>
			<li><a href="{{ route('users.register') }}"><i class="fa fa-user-plus"></i> Crea tu cuenta</a></li>
		@endif
		<li><a href="#" id="updateLocation"><i class="fa fa-map-marker"></i> Actualizar ubicación</a></li>
  </ul>
</nav>

@push('scripts')
	<script>
		$('#updateLocation').on('click', function(){
			$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	    $.ajax({
			  method: "POST",
			  url: "{{ route('maps.geolocation.update') }}",
			  data: {data:getLocalData(geoStorage,'data'),token:getLocalData(userStorage,'data')},
			  success:function(data){
			  	switch(data.meta.status){
	    			case 'redirect'	: window.location = data.data.url;
	    			case 'error'		: showMessage('Error',data.data.message);
	    											 	break;
	    			default: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
	    		}
			  },
			  error:function(){
			  	showMessage('Error','Ocurrió un error al intentar actualizar la ubicación, intente nuevamente.');
			  }
			});
		});

		$('#close_slide').on('click',function(){
			$('#slide_menu').css({"margin-left": "-200px", "z-index": "0" });
		});
	</script>
@endpush