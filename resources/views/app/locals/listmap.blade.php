<ons-row align="center">
	<ons-col >
		<div class="text-center clearfix">
      	<div id="map_canvas" style="height:200px;"></div>
      </div>
	</ons-col>
</ons-row>
@push('scripts')
@include ('app.maps.mapjs',['lat'=>false,'lng'=>false,'localsmap'=>true])
<script>
	  var zones = [];
	  var infowindow = null;

	  function showZone(map,points,line,bg,id){
	  	if( zones[id] == undefined ){ //add zone to collection
		  	if(points.length<2) return false;
		  	var path = Array();
		  	for(var i=0;i<=points.length-1;i+=2){
		  		path.push([points[i],points[i+1]]);
		  	}
	  		zones[id] = [path,true];
	  	}else{
	  		zones[id][1] = !(zones[id][1]);
	  	}
	  	if(zones[id][1]){
	  		zones[id][2] = map.drawPolygon({
			    paths: zones[id][0],
			    strokeColor: line,
			    strokeOpacity: 0.8,
			    strokeWeight: 2,
			    fillColor: bg,
			    fillOpacity: 0.25
			  });
	  	}else{
	  		map.removePolygon(zones[id][2]);
	  	}
	  }

		function localsMap(){
      $.ajaxSetup({ headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'} });
      $.ajax({
        method: 'POST',
        url: "{{ route('locals.map') }}",
        data:  {'locals':getLocalData(localsStorage,'data'),'point':getLocalData(geoStorage,'data')},
        success:function(data){
          switch(data.meta.status){
            case 'ok'   :  updateMap(data.data.locals,data.data.point);
            							 break;
            case 'error'  : showMessage('Error',data.data.message);
                            break;
            default: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
          }
        },
        error: function(){
          showMessage('Error','Ocurrió un error al intentar mostrar el mapa');
        }
      });
    }

    function updateMap(locals, point){
      var latlng = {lat: parseFloat(point['lat']), lng:parseFloat(point['lng'])};
      map.setCenter(latlng);
      for (var i = 0, len = locals.length; i < len; i++) {
        addMarker(locals[i],map);
      }
    }

    function addMarker(local,map){
      var marker_local = map.addMarker({
        position: {lat: parseFloat(local['point']['lat']), lng: parseFloat(local['point']['lng'])},
        title: '<strong>'+local['name']+'</strong><br/>'+local['address'],
        animation:google.maps.Animation.DROP,
        icon: "{{ asset('css/img/marker.png') }}",
        click:function(){
        	var contentwindow = marker_local.title;
					if (infowindow){
						infowindow.close();
						infowindow = null;
					}
					infowindow = new google.maps.InfoWindow({
	    				content: contentwindow
					});
	    		map.panTo({lat: marker_local.position.lat(), lng: marker_local.position.lng() });
	    		infowindow.open(map, marker_local);
	    		showZone(map,local['zone'],"#FF0000","#FF0000",local['id']);
        }
      });
    }


	</script>
@endpush