@extends('layouts.app')

@section('content')

<ons-page>
		<ons-row>
			<ons-col>
				<p class="text-center text-uppercase text-bold">{!! $page_title !!}</p>
			</ons-col>
		</ons-row>
  <ons-row align="center">
		<ons-col >
  		<div class="text-center clearfix">
        	<div id="map_canvas"></div>
        </div>
  	</ons-col>
	</ons-row>
	<ons-row>
		<ons-col>
			<div class="text-center">
      	<a href="{{URL::previous()}}" class="button"><i class="fa fa-chevron-left"></i> Regresar </a>
    	</div>
      <br/>
		</ons-col>
	</ons-row>
</ons-page>

@endsection

@push('scripts')
	@include ('app.maps.mapjs',['lat'=>$lat,'lng'=>$lng,'routemap'=>true])
	<script>

		jQuery(document).ready(function($) {
			$('#map_canvas').height($( document ).height()-150);

			$( window ).resize(function() {
				  $('#map_canvas').height($( document ).height()-150);
			});
		});

		function routeMap(){
			$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	    $.ajax({
			  method: "POST",
			  url: "{{ route('maps.geolocation.info') }}",
			  data: {data:getLocalData(geoStorage,'data')},
			  success:function(data){
			  	switch(data.meta.status){
	    			case 'ok'	: showRoute([parseFloat(data.data.lat),parseFloat(data.data.lng)],data.data.local); break;
	    			default		: return false;
	    		}
			  },
			  error:function(){
			  	showMessage('Aviso','Ocurrió un error al intentar detectar su ubicación, intente nuevamente.');
			  	return false;
			  }
			});
		}

		function showRoute(origin, local){

			marker = map.addMarker({
			  lat: origin[0],
      	lng: origin[1],
			  draggable: false,
	  		animation: google.maps.Animation.DROP,
	  		infoWindow: {
          content: 'Usted se encuentra aquí'
        }
			});

			marker = map.addMarker({
			  lat: parseFloat({{$lat}}),
      	lng: parseFloat({{$lng}}),
			  draggable: false,
	  		animation: google.maps.Animation.DROP,
	  		infoWindow: {
          content: '{{$name}}'
        }
			});

			map.travelRoute({
        origin: origin,
        destination: [parseFloat({{$lat}}), parseFloat({{$lng}})],
        travelMode: 'driving',
        step: function(e){
          $('#map_canvas').delay(150*e.step_number).fadeIn(200, function(){
            map.setCenter(e.end_location.lat(), e.end_location.lng());
            map.drawPolyline({
              path: e.path,
              strokeColor: '#131540',
              strokeOpacity: 0.6,
              strokeWeight: 6
            });
          });
        }
      });
		}

	</script>
@endpush