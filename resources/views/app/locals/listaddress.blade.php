@if(count($locals)>0)
<ul class="list" >
  @foreach($locals as $local)
    <li class="list__item list-item-container" id="item_{{$local['id']}}">
      <div class="left">
        @if (isset($local['image']) AND count($local['image'])>0)
          <img src="{{ $local['image'][0] }}" class="circle-image">
        @else
          <img src="{{ asset('img/local/0.jpg') }}" class="circle-image">
        @endif
      </div>
      <div class="list-item-right">
        <div class="list-item-content">
          <div class="text-title font-type-1 text-uppercase clearfix">{{ $local['name'] }}</div>
          @if ( isset($local['address']) )
          <span class="text-subtitle text-muted clearfix">{{ $local['address'] }}</span>
          @else
          <span class="text-subtitle text-muted clearfix"> - </span>
          @endif
        </div>
      </div>
    </li>
  @endforeach
</ul>
@endif