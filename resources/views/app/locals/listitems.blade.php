<ons-row >
	<ons-col width="99%">
	  <div class="center-block">
	    <input id="txtsearch" name="txtsearch" class="font-type-2" style="width:93%;" required/>
	  </div>
	</ons-col>
</ons-row>

  </ons-col>
</ons-row>
<ons-row>
  <ons-col>
    <p>Nuestros locales</p>
    <div id="locals-list-div"></div>
  </ons-col>
</ons-row>

@push('styles')
	<link rel="stylesheet" href="{{ asset('css/easy-autocomplete.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/easy-autocomplete.themes.min.css') }}" />
@endpush

@push('scripts')
<script src="{{ asset('lib/jquery/jquery.easy-autocomplete.min.js') }}"></script>
<script>
  jQuery(document).ready(function($) {
    localsList();
  });

    function localsList(){
      $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
      $.ajax({
        method: "POST",
        url: "{{ route('locals.list') }}",
        data:  {data :getLocalData(localsStorage,'data'),token:"{{$token}}"} ,
        success:function(data){
          switch(data.meta.status){
            case 'show'   : $('#locals-list-div').html(data.data.html);
            								searchBox(data.data.locals);
            								buttonFavorite();
                            break;
            case 'error'  : showMessage('Error',data.data.message);
                            break;
            default: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
          }
        },
        error: function(){
          showMessage('Error','Ocurrió un error al intentar recuperar la lista de locales');
        }
      });
    }

  function buttonFavorite(){
		$('.favlocal').unbind();
		$('.favlocal').on('click',function(e){
  		e.preventDefault();
  		addfavorite($(this).attr('localid'));
  		return false;
  	});
  }

  function addfavorite(id){
    $.ajax({
      method: "POST",
      url: "{{ route('locals.favorite') }}",
      data: {local: id,token:"{{$token}}"},
      success: function(data){
      	switch(data.meta.status){
      		case 'ok'	: var star = $('#star-'+id);
      								if(star.hasClass('fa-star')){
      									star.removeClass('fa-star').addClass('fa-star-o');
      									showToast('','Local retirado de tu lista de favoritos');
      								}else{
      									star.removeClass('fa-star-o').addClass('fa-star');
      									showToast('','Local agregado a tu lista de favoritos');
      								}
      								break;
      		default		: showMessage('Aviso',data.data.message);
      	}
			},
      error: function (){
      	showMessage('Aviso','Ocurrió un error inesperado, intente nuevamente');
      }
    });
  }

    function searchBox(locals){
			var names = Array(), addresses = Array();
			for(var i=0;i<locals.length;i++){
	  		names.push({value: locals[i]['id'],label:locals[i]['name'],detail:locals[i]['address']});
	  		addresses.push({value: locals[i]['id'],label:locals[i]['address'],detail:locals[i]['name']});
	  	}
	  	var data = {'names': names, 'addresses': addresses};
			var options = {
    		data: data,
    		placeholder: "Escribe el nombre del local o su dirección",
		    getValue: "label",
		    list: {
		    	maxNumberOfElements: locals.length*2,
		    	match: {enabled: true},
		    	sort: {enabled: true},
		    	onChooseEvent: function() {
						var item = $("#txtsearch").getSelectedItemData();
						showLocalSelected(item);
					},
					onHideListEvent: function(){
						verifyLocalSelected();
					},
		  	},
		 		template: {
					type: "description",
					fields: {
						description: "detail"
					}
				},
		 	 	categories: [
	        {   listLocation: "names",
	            header: "-- Locales --"
	        },
	        {  listLocation: "addresses",
	            header: "-- Direcciones --"
	        }
    		],
    		theme: "aquare",
		   };
			$("#txtsearch").easyAutocomplete(options);
    }

    function verifyLocalSelected(){
    	var value = $("#txtsearch").val();
    	if(value == '')
    		showLocalSelected(null);
    }

    function showLocalSelected(local){
    	if(local == null){
    		$('.list-item-container').show();
    		return false;
    	}
    	var id = '#item_'+local.value;
    	$('.list-item-container').hide();
    	$(id).show();
    }


</script>
@endpush
