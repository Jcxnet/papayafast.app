<?php  $day = date('w'); ?>
<ons-row>
	<ons-col>
	<span class="text-desc clearfix" id="txt-horario">Horario de atención de hoy</span>
	</ons-col>
	<ons-row align="center">
		<ons-col width="4%">
			<div class="text-left" >
				<a href="#" id="day-prev" class="text-danger"><ons-icon icon="chevron-left"></ons-icon></a>
			</div>
		</ons-col>
		<ons-col width="92%">
			<div class="text-center">
			<ons-carousel  swipeable auto-scroll overscrollable id="carousel" style="height:40px; width:100%"  initial-index="{{$day}}">
			 @for( $i = 0 ; $i < 7; $i++ )
			  <ons-carousel-item >
			    <div class="text-center">
			    	<span class="text-high text-uppercase {{ ($i == $day)?'text-success':'text-muted' }}">{{ $horario[$i][$i] }}</span>
			    	<span class="text-desc {{ ($i == $day)?'text-success':'text-muted' }}">{{ $horario[$i]['open']}} - {{ $horario[$i]['close'] }}</span>
			    </div>
			  </ons-carousel-item>
			 @endfor
			 </ons-carousel>
			 </div>
		</ons-col>
		<ons-col width="4%">
		 	<div class="text-right">
				<a href="#" id="day-next" class="text-danger"><ons-icon icon="chevron-right"></ons-icon></a>
		 	</div>
		</ons-col>
	 </ons-row>
</ons-row>
<div class="item-product-border">&nbsp;</div>

<script>
	var first = 0; var last=0;
	$('#day-prev').on('click',function(){
		document.getElementById('carousel').prev().then(function(event) {
			if(event.getActiveIndex()==0){
				if(first >0){
					event.last(); first = 0;
				}else{
				first++;
				}
			}
		});
	});
	$('#day-next').on('click',function(){
		document.getElementById('carousel').next().then(function(event){
			if(event.getActiveIndex()==6){
				if(last >0){
					event.first(); last = 0;
				}else{
				last++;
				}
			}
		});
	});

	var carousel_swipe = document.addEventListener('overscroll',function(event){
		if(event.activeIndex == 0){
			if(first >0){
				event.carousel.last(); first = 0;
			}else{
			first++;
			}
		}else if(event.activeIndex == 6){
			if(last >0){
				event.carousel.first(); last = 0;
			}else{
			last++;
			}
		}
	});
	var carousel_change = document.addEventListener('postchange', function(event) {
    if(event.activeIndex != {{$day}} ){
			$('#txt-horario').html('Horario del día');
		}else{
			$('#txt-horario').html('Horario de atención de hoy');
		}
  });

</script>