@extends('layouts.app')

@section('content')

<ons-page>
@include ('app.includes.toolbar')
<ons-row>
  <ons-col>
@if(count($locals)>0)
	<ul class="list" >
  @foreach($locals as $local)
    <li class="list__item list-item-container" id="item_{{$local['id']}}">
      <div class="left">
        @if (isset($local['image']) AND count($local['image'])>0)
          <img src="{{ $local['image'][0] }}" class="circle-image">
        @else
          <img src="{{ asset('img/local/0.jpg') }}" class="circle-image">
        @endif
      </div>
      <div class="left">
        <div class="list-item-content">

          <div class="text-title font-type-1 text-uppercase clearfix"> <a href="{{ route('locals.show',['id'=>$local['id'],'token'=>$token]) }}" class="text-black link-menu">{{ $local['name'] }}</a></div>
          @if ( isset($local['address']) )
          <span class="text-subtitle text-muted clearfix">{{ $local['address'] }}</span>
          @else
          <span class="text-subtitle text-muted clearfix"> - </span>
          @endif
          <strong class="text-subtitle {{ ($local['open'])?'text-success':'text-danger' }} clearfix">{{ $local['status'] }}</strong>
        </div>
      </div>
      @if($token)
      <div class="right">
        <i id="star-{{$local['id']}}" class="fa fa-star fa-2x text-ambar pull-right"></i>
      </div>
      @endif
    </li>
  @endforeach
</ul>
@else
	<div class="alert alert-info" role="alert">
    <h4>No tienes locales favoritos</h4>
    <p class="text-center">
    	<a href="{{ route('locals.view',['token'=>$token]) }}" class="button"> <i class="fa fa-star" aria-hidden="true"></i> Seleccionar algún local</a>
    </p>
  </div>
@endif
	</ons-col>
</ons-row>

</ons-page>

@endsection