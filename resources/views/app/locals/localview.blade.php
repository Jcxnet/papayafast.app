@extends('layouts.app')

@section('content')
<ons-page>
   	@include ('app.includes.toolbar')
   <div id="local-view-div"></div>
</ons-page>
@endsection

@push('scripts')
<script>
	jQuery(document).ready(function($) {

		function localShow(){
      $.ajaxSetup({ headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'} });
      $.ajax({
        method: 'POST',
        url: "{{ route('locals.show.info') }}",
        data:  {'locals':getLocalData(localsStorage,'data'),'id':"{{$id}}"},
        success:function(data){
          switch(data.meta.status){
            case 'html'   : $('#local-view-div').html(data.data.html);
                            break;
            case 'error'  : showMessage('Error',data.data.message);
                            break;
            default: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
          }
        },
        error: function(){
          showMessage('Error','Ocurrió un error al intentar mostrar el local');
        }
      });
    }

    localShow();

	});
</script>
@endpush