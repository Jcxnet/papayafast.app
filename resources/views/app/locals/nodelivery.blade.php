@extends('layouts.app')

@section('content')
<ons-page>
   <ons-row align="center" class="height-80">
     <ons-col align="center">
        <div class="logo-xs clearfix">&nbsp;</div>
        <div class="text-center text-subtitle text-white clearfix">{{ $name }}</div>
        <div class="text-center text-danger clearfix"><ons-icon icon="fa-exclamation-triangle" size="3x"></ons-icon></div>
        <div class="text-center text-title text-danger">Por ahora no contamos con el servicio de Delivery.</div>
        <div class="text-center clearfix">
          <p class="text-center"><a href="{{ route('main.menu',['token'=>$token]) }}" class="btn btn-danger btn-lg link-button"><strong>Continuar <i class="fa fa-chevron-right"></i></strong></a></p>
        </div>
      </ons-col>
  </ons-row>
</ons-page>
@endsection