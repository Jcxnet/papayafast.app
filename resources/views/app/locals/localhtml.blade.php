  <ons-row>
    <ons-col>
      <div class="center-block">
      	<h3 class="text-center">{{$local['name']}}</h3>
      	@if (isset($local['image']) AND count($local['image'])>0)
          <img src="{{ $local['image'][0] }}" class="ri fade-in" alt="">
        @else
          <img src="{{ asset('img/local/0.jpg') }}" class="ri fade-in" alt="">
        @endif
      </div>
    </ons-col>
  </ons-row>
  <div class="item-product-border">&nbsp;</div>
  @include ('app.locals.timetable',['horario'=>$local['horario'],'open'=>$local['open']])
  <ons-row align="center">
    <ons-col>
      <span class="text-desc clearfix">Dirección</span>
      <span class="text-normal">{{ $local['address'] }}</span>
    </ons-col>
    <ons-col align="right" width="30px">
    	<a href="{{ route('locals.route',['lat'=>$local['point']['lat'],'lng'=>$local['point']['lng'], 'name'=>$local['name']])}}" class="pull-right" style="padding:5px;"><i class="fa fa-map-marker fa-2x text-danger"></i></a>
    </ons-col>
  </ons-row>
  <div class="item-product-border">&nbsp;</div>
  @foreach ($local['contacto'] as $contacto)
	  @if($contacto['phone'] != '' || $contacto['email'] != '')
	  <ons-row>
	    <ons-col>
	    	@if(isset($contacto['name']) AND isset($contacto['phone'][0]) )
	      <span class="text-desc clearfix">Teléfono {{$contacto['name']}}</span>
	      <span class="text-normal">{{ $contacto['phone'][0] }}</span>
	      @endif
	    </ons-col>
	    <ons-col>
	      @if(isset($contacto['name']) AND isset($contacto['email'][0]) )
	      <span class="text-desc clearfix">Email {{$contacto['name']}}</span>
	      <span class="text-normal">{{ $contacto['email'][0] }}</span>
	      @endif
	    </ons-col>
	  </ons-row>
	  <div class="item-product-border">&nbsp;</div>
	  @endif
  @endforeach