@extends('layouts.app')

@section('content')

<ons-page>
   	@include ('app.includes.toolbar')
 		@include ('app.locals.listmap')
 		@include ('app.locals.listitems',['token'=>$token])
</ons-page>

@endsection