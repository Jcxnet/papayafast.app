@extends('layouts.app')

@push('styles')
	<link rel="stylesheet" href="{{ asset('css/loaders.css') }}" />
@endpush

@section('content')

	<ons-page>
		<ons-row align="center" class="height-100">
			<ons-col>
		   <div class="logo" id="logo">&nbsp;</div>
	    	<br/>
	    	<div class="center-block" style="width:1.5em; height:1.5em;">
	    		<div class="loader" style="padding-left: 15px; position: absolute; top:48%;">
	    			<div class="loader-inner ball-scale-multiple">
	    				<div></div><div></div><div></div>
	    			</div>
	    		</div>
	    	</div>
	   	</ons-col>
  	</ons-row>
	</ons-page>

@endsection


@push('scripts')

<script>
  jQuery(document).ready(function($) {

  	function loadValues($data, $url, storage, callback){
      $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	    $.ajax({
			  method: "POST",
			  url: $url,
			  data:  $data ,
	    	success:function(data){
	    		switch(data.meta.status){
	    			case 'nochange'	: showToast('Verificado',data.meta.message);
	    											 	break;
	    			case 'update'	 	: showToast('Actualización',data.meta.message);
	    												callback(storage, data.data);
	    											 	break;
	    			case 'error'	 	: showMessage('Error',data.data.message);
	    											 	break;
	    			case 'redirect' : window.location = data.data.url;
	    												break;
	    			case 'remove'		: deleteAllLocalData(storage);
	    												showToast('Aviso',data.meta.message);
	    												break;
	    			default: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
	    		}
				},
				error: function(){
					showMessage('Error','Ocurrió un error al intentar actualizar la información');
				}
			});
		}

		if(storageSupported()){
			var logo = $('#logo');
			logo.queue('start',function(next){
				loadValues ( {'data':getLocalData(appStorage,'data')}, 	"{{ route('main.load.app') }}" , appStorage,  setArrayLocalData );
				next();
			});
			logo.delay(getRandom(250,500),'start');
			logo.queue('start',function(next){
				loadValues ( {'data':getLocalData(menusStorage,'data')}, 	"{{ route('main.load.menus') }}" , menusStorage,  setArrayLocalData );
				next();
			});
			logo.delay(getRandom(500,750),'start');
			logo.queue('start',function(next){
				loadValues ( {'data':getLocalData(localsStorage,'data')}, 	"{{ route('main.load.locals') }}" , localsStorage,  setArrayLocalData );
				next();
			});

			logo.delay(getRandom(750,1000),'start');
			logo.queue('start',function(next){
				loadValues ( {'data':getLocalData(tokenStorage,'id')}, "{{ route('main.load.user') }}", tokenStorage, setArrayLocalData );
				next();
			});
			logo.delay(getRandom(1250,1500),'start');
			logo.queue('start',function(next){
				loadValues ( {'data':getLocalData(addressesStorage,'data'),'token':getLocalData(tokenStorage,'id')}, "{{ route('main.load.address') }}", addressesStorage, setArrayLocalData );
				next();
			});
			logo.delay(getRandom(1500,1750),'start');
			logo.queue('start',function(next){
				loadValues ( {'data':getLocalData(geoStorage,'data'),'token':getLocalData(tokenStorage,'id')},"{{ route('main.islocated') }}" , geoStorage,  setArrayLocalData );
				next();
			});
			logo.dequeue('start');
		}
		else{
			showMessage('Alerta','El almacenamiento local de su dispositivo es incompatible con la aplicación');
		}

  });
 </script>
 @endpush

