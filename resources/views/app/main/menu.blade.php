<?php
	$isToken  = isset($token);
?>
@extends('layouts.app')

@section('content')
<ons-page >
   	@include ('app.includes.toolbar')
    <div style="margin-top:50%" >&nbsp;</div>
   <ons-row align="center">
    <ons-col width="80%">
        <a href="{{ route('addresses.delivery',['token'=>$token]) }}" class="button button--large--quiet" modifier="tappable" ><span class="text-title text-uppercase text-white pull-left">Pedir delivery</span><br><span class="text-desc text-muted pull-left">Llevamos el pedido a tu casa</span></a>
    </ons-col>
    <ons-col width="15%">
      <a href="{{ route('addresses.delivery',['token'=>$token]) }}" class="button button--large--quiet" modifier="tappable">
        <span><i class="fa fa-chevron-right fa-2x text-danger pull-left"></i></span>
      </a>
    </ons-col>
    <ons-col width="5%">&nbsp;</ons-col>
    <div class="menu-item-border"></div>
   </ons-row>

   <ons-row align="center">
    <ons-col width="80%">
        <a href="{{route('orders.actual',['token'=>$token])}}" class="button button--large--quiet" modifier="tappable" ><span class="text-title text-uppercase text-white pull-left">Mi pedido actual</span></a>
    </ons-col>
    <ons-col width="15%">
      <a href="{{route('orders.actual',['token'=>$token])}}" class="button button--large--quiet" modifier="tappable" style="padding-left: 0;">
        <i class="fa fa-list-alt fa-2x text-white pull-left"></i>
      </a>
    </ons-col>
    <ons-col width="5%">&nbsp;</ons-col>
    <div class="menu-item-border"></div>
   </ons-row>

   <ons-row align="center">
    <ons-col width="80%">
    	<a href="#" class="button button--large--quiet" modifier="tappable" ><span class="text-title text-uppercase text-muted pull-left">Pedir para llevar</span><br><span class="text-desc text-muted pull-left">&nbsp;<span class="text-ambar">Muy pronto</span></span></a>
    </ons-col>
    <ons-col width="15%">&nbsp;</ons-col >
    <ons-col width="5%">&nbsp;</ons-col>
    <div class="menu-item-border"></div>
   </ons-row>

   <ons-row align="center">
    <ons-col width="80%">
        <a href="#" class="button button--large--quiet" ><span class="text-title text-uppercase text-muted pull-left">Reservar mesa</span><br><span class="text-desc text-muted pull-left">&nbsp;<span class="text-ambar">Muy pronto</span></span></a>
    </ons-col>
    <ons-col width="15%">&nbsp;</ons-col >
    <ons-col width="5%">&nbsp;</ons-col>
    <div class="line-border"></div>
   </ons-row>


   <ons-row align="center" >
      <ons-col width="80%">
          <a href="{{ route('locals.view',['token'=>$token]) }}" class="button button--large--quiet" id="locals-list-btn" modifier="tappable" ><span class="text-title text-uppercase text-white pull-left">Lista de locales</span></a>
      </ons-col>
      <ons-col width="15%">
        <a href="{{ route('locals.view',['token'=>$token]) }}" class="button button--large--quiet" modifier="tappable">
          <span><i class="fa fa-chevron-right fa-2x text-danger pull-left"></i></span>
        </a>
      </ons-col >
      <ons-col width="5%">&nbsp;</ons-col>
      <div class="menu-item-border"></div>
   </ons-row>
  @if ($isToken)
    <ons-row align="center">
    <ons-col width="80%">
        <a href="{{route('users.menu',['token'=>$token])}}" class="button button--large--quiet btn_user_pref" modifier="tappable" ><span class="text-title text-uppercase text-white pull-left">Editar preferencias</span>
        </a>
    </ons-col>
    <ons-col width="15%">
      <a href="#" class="button button--large--quiet btn_user_pref" modifier="tappable" style="padding-left: 0;">
        <i class="fa fa-cog fa-2x text-white pull-left"></i>
      </a>
    </ons-col>
    <ons-col width="5%">&nbsp;</ons-col>
    <div class="menu-item-border"></div>
   </ons-row>
   <!--
   <ons-row>
    <ons-col>
      <div id="user_prefs" style="display:none;$">
       <ons-row>
        <ons-col width="10%">&nbsp;</ons-col>
        <ons-col width="90%">
           <a href="{{route('orders.history',['token'=>$token])}}" class="button button--large--quiet" modifier="tappable" ><span class="text-subtitle text-white pull-left">Historial de pedidos</span></a>
           <div class="menu-item-border"></div>
        </ons-col>
       </ons-row>
       <ons-row>
        <ons-col width="10%">&nbsp;</ons-col>
        <ons-col width="90%">
           <ons-button class="button button--large--quiet" modifier="tappable" ><span class="text-subtitle text-white pull-left">Mis locales favoritos</span></ons-button>
           <div class="menu-item-border"></div>
        </ons-col>
       </ons-row>
       <!--
       <ons-row>
        <ons-col width="10%">&nbsp;</ons-col>
        <ons-col width="90%">
           <ons-button class="button button--large--quiet" modifier="tappable" ><span class="text-subtitle text-white pull-left">Mis métodos de pago</span></ons-button>
           <div class="menu-item-border"></div>
        </ons-col>
       </ons-row>
       -->
       <!--
       <ons-row>
        <ons-col width="10%">&nbsp;</ons-col>
        <ons-col width="90%">
           <a href="{{ route('addresses.view',['token'=>$token,'delivery'=>0]) }}" class="button button--large--quiet" modifier="tappable" ><span class="text-subtitle text-white pull-left">Mis direcciones de delivery</span></a>
           <div class="menu-item-border"></div>
        </ons-col>
       </ons-row>
       <ons-row>
        <ons-col width="10%">&nbsp;</ons-col>
        <ons-col width="90%">
           <a href="{{route('users.logout',['token'=>$token])}}" class="button button--large--quiet" modifier="tappable" ><span class="text-subtitle text-white pull-left">Cerrar sesión</span></a>
           <div class="menu-item-border"></div>
           <br/>
        </ons-col>
       </ons-row>
     </div>
    </ons-col>
   </ons-row>
   -->
   @endif
   <!--
    <ons-row>
      <ons-col width="25%" class="text-center">
        <a href="#" class="link-menu">FAQ</a>
      </ons-col>
      <ons-col width="25%" class="text-center">
        <a href="#" class="link-menu">Feedback</a>
      </ons-col>
      <ons-col width="25%" class="text-center">
        <a href="#" class="link-menu">Acerca de</a>
      </ons-col>
      <ons-col width="25%" class="text-center">
        <a href="#" class="link-menu">Contacto</a>
      </ons-col>
    </ons-row>
    -->
    <br/>
 </ons-page>
@endsection

@push('scripts')
  <script >
  /*jQuery(document).ready(function($) {

    $('.btn_user_pref').on('click',function(e){
      e.preventDefault();
      $('#user_prefs').toggle('slow');
      return false;
    });

   });*/
  </script>

@endpush