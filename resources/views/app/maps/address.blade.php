	<ons-row>
		<ons-col>
			<p class="text-center text-uppercase text-bold">{{ $page_title }}</p>
		</ons-col>
	</ons-row>
  <ons-row align="center">
  	<ons-col width="99%">
  		<div class="searchbox">
			  <div class="searchbox-container center-block">
			    <input type="search" id="address" name="address" placeholder="Buscar calles, avenidas, barrios..." class="font-type-2" required/>
			    <button class="icon" id="btn_address"><i class="fa fa-search"></i></button>
			  </div>
			</div>
  	</ons-col>
  </ons-row>
  <ons-row align="center">
		<ons-col >
  		<div class="text-center clearfix">
        	<div id="map_canvas" style="height:300px;"></div>
      </div>
      <input type="hidden" name="txtlat" id="txtlat" value="" />
			<input type="hidden" name="txtlng" id="txtlng" value="" />
  	</ons-col>
	</ons-row>

@push('scripts')
	@include ('app.maps.mapjs',['lat'=>$lat,'lng'=>$lng,'localsmap'=>false])
	<script>
		jQuery(document).ready(function($) {

			$( "#address" ).keypress(function( event ) {
			  if ( event.which == 13 ) {
			     event.preventDefault();
			     searchAddress();
			     return false;
			  }
			});

			$('#btn_address').on('click',function(){
				searchAddress();
			});

			marker = map.addMarker({
			  lat: {{($lat)?$lat:-12.04644}},
      	lng: {{($lng)?$lng:-77.04277}},
			  draggable: true,
	  		animation: google.maps.Animation.DROP,
			});

			google.maps.event.addListener(marker, 'dragend', function(e){
				map.setCenter(marker.position.lat(),marker.position.lng());
				updateLatLng(marker.position.lat(),marker.position.lng());
			});

		});

	</script>
@endpush
