@extends('layouts.app')

@section('content')

<ons-page>
		<ons-row>
			<ons-col>
				<p class="text-center text-uppercase text-bold">{{ $page_title }}</p>
			</ons-col>
		</ons-row>
  	<ons-row align="center">
  	<ons-col width="99%">
  		<div class="searchbox">
			  <div class="searchbox-container center-block">
			    <input type="search" id="address" name="address" placeholder="Buscar calles, avenidas, barrios..." class="font-type-2" required/>
			    <button class="icon" id="btn_address"><i class="fa fa-search"></i></button>
			  </div>
			</div>
  	</ons-col>
  </ons-row>
  <ons-row align="center">
		<ons-col >
  		<div class="text-center clearfix">
        	<div id="map_canvas"></div>
        </div>
  	</ons-col>
	</ons-row>
	<ons-row>
		<ons-col>
			<div class="text-center">
      	<ons-button class="button" id="btnReady" modifier="tappable" >Listo  <i class="fa fa-chevron-right"></i></ons-button>
      	<input type="hidden" name="txtlat" id="txtlat" value="" />
				<input type="hidden" name="txtlng" id="txtlng" value="" />
    	</div>
      <br/>
		</ons-col>
	</ons-row>
</ons-page>

@endsection

@push('scripts')
	@include ('app.maps.mapjs',['lat'=>$lat,'lng'=>$lng,'localsmap'=>false])
	<script>

	 function gmapsGeolocation(){
			GMaps.geolocate({
			  success: function(position) {
			    map.setCenter(position.coords.latitude, position.coords.longitude);
    			marker.setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
    			updateLatLng(position.coords.latitude, position.coords.longitude);
			  },
			  error: function(error) {
			    showMessage('Aviso','El servicio de ubicación ha respondido: '+error.message);
			  },
			  not_supported: function() {
			    showMessage('Aviso','Su equipo no soporta el servicio de ubicación');
			  }
			});
		}

		function detectGeolocation(){
			if(navigator.geolocation) {
				var optn = {
					enableHighAccuracy : true,
					timeout : Infinity,
					maximumAge : 0
				};
				navigator.geolocation.getCurrentPosition(getPosition, errPosition, optn);
			} else {
			    gmapsGeolocation();
			}
		}

		function getPosition(position){
		    var lat = position.coords.latitude;
				var lng = position.coords.longitude;
				map.setCenter(lat,lng);
				marker.setPosition(new google.maps.LatLng(lat, lng));
				updateLatLng(lat,lng);
		}

		function errPosition(error) {
			return false;
			switch(error.code) {
				case error.PERMISSION_DENIED:
					ShowMessage("Aviso","User denied the request for Geolocation.");
					break;
				case error.POSITION_UNAVAILABLE:
					ShowMessage("Aviso","Location information is unavailable.");
					break;
				case error.TIMEOUT:
					ShowMessage("Aviso","The request to get user location timed out.");
					break;
				case error.UNKNOWN_ERROR:
					ShowMessage("Error","An unknown error occurred.");
					break;
			}
		}

		$('#btnReady').on('click',function(e){
				e.preventDefault();
			  window.location = "{{ route('main.menu',['token'=>$token]) }}";
				return false;
		});

		jQuery(document).ready(function($) {
			$('#map_canvas').height($( document ).height()-210);

			$( window ).resize(function() {
				  $('#map_canvas').height($( document ).height()-210);
			});

			$( "#address" ).keypress(function( event ) {
			  if ( event.which == 13 ) {
			     event.preventDefault();
			     searchAddress();
			     return false;
			  }
			});

			$('#btn_address').on('click',function(){
				searchAddress();
			});

			marker = map.addMarker({
			  lat: {{($lat)?$lat:-12.04644}},
      	lng: {{($lng)?$lng:-77.04277}},
			  draggable: true,
	  		animation: google.maps.Animation.DROP,
			});

			google.maps.event.addListener(marker, 'dragend', function(e){
				map.setCenter(marker.position.lat(),marker.position.lng());
				updateLatLng(marker.position.lat(),marker.position.lng());
			});

			//detectGeolocation();
			gmapsGeolocation();
		});


	</script>
@endpush