<?php
	$localsmap 	= isset($localsmap)?$localsmap:false;
	$routemap 	= isset($routemap)?$routemap:false;
?>
@push('scripts')
<script type="text/javascript" src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDsAn3nOIS1Q7_RrMcLIzoPJT-Np3PyXkU'></script>
<script type="text/javascript" src="{{asset('lib/js/gmaps.min.js')}}"></script>
<script >

		var marker;
		var map = new GMaps({
      div: '#map_canvas',
      lat: {{($lat)?$lat:-12.04644}},
      lng: {{($lng)?$lng:-77.04277}},
      zoom: 16,
      streetViewControl: false,
      mapTypeControl: false,
      @if(!$localsmap AND !$routemap)
      click: function(point){
      	map.setCenter(point.latLng.lat(), point.latLng.lng());
      	marker.setPosition(new google.maps.LatLng(point.latLng.lat(), point.latLng.lng()));
      	updateLatLng(point.latLng.lat(), point.latLng.lng());
      },
      @endif
      @if($localsmap)
      tilesloaded: function(map){
      	google.maps.event.clearListeners(map, 'tilesloaded');
      	localsMap();
      },
      @endif
      @if($routemap)
      tilesloaded: function(map){
      	google.maps.event.clearListeners(map, 'tilesloaded');
      	routeMap();
      }
      @endif
    });


		function getAddress(lat, lng){
			var latlng = {lat: lat, lng:lng};
	  	var geocoder = new google.maps.Geocoder;
	  	geocoder.geocode({"location": latlng}, function(results, status){
	  		if (status === google.maps.GeocoderStatus.OK) {
	      	if (results[0]) {
		        $("#address").focusin().val(results[0].formatted_address).trigger("change");
		        sendValues(results[0].formatted_address, lat, lng );
		      } else {
		      	showMessage('Resultado','No encontramos esa drección, intenta nuevamente');
		      }
		    } else {
		      showMessage('Error','El servicio de ubicación ha fallado, intenta nuevamente');
		    }
	  	});
  	}

  	function searchAddress() {
  		var address = $("#address").val();
  		if (address == '') return false;
  		var geocoder = new google.maps.Geocoder;
	    geocoder.geocode( { 'address': address,componentRestrictions: {country: 'PE',locality: 'Lima'}}, function(results, status) {
	      if (status == google.maps.GeocoderStatus.OK) {
	      		var location = results[0].geometry.location;
	        	map.setCenter(location.lat(),location.lng());
	        	marker.setPosition(new google.maps.LatLng(location.lat(), location.lng()));
	        	var latlng = {lat: location.lat(), lng: location.lng()};
	        	geocoder.geocode({"location": latlng}, function(results, status){
	        		if (status === google.maps.GeocoderStatus.OK) {
	      				if (results[0]) {
		        			$("#address").focusin().val(results[0].formatted_address).trigger("change");
		        			sendValues (results[0].formatted_address,location.lat(),location.lng());
		        		}
		        	}
	        	});
	      } else {
	      	showMessage('Error','El servicio de ubicación ha fallado, intenta nuevamente');
	      }
	    });
	  }

	  function sendValues(address,lat,lng){
	    $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	    $.ajax({
			  method: "POST",
			  url: "{{ route('maps.geolocation.save') }}",
			  data: {"address": address, "lat": lat, "lng": lng },
			  success:function(data){
			  	switch(data.meta.status){
	    			case 'update'	: showToast('Actualización',data.meta.message,0.5);
	    												setArrayLocalData(geoStorage, data.data);
	    											 	break;
	    			case 'error'	: showMessage('Error',data.data.message);
	    											 	break;
	    			default: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
	    		}
			  },
			  error:function(){
			  	showMessage('Error','Ocurrió un error al intentar actualizar la ubicación, intente nuevamente.');
			  }
				});

	    if($('#txtlat')) $('#txtlat').val(lat);
	    if($('#txtlng')) $('#txtlng').val(lng);
		}

		function updateLatLng(lat,lng){
			$('#txtlat').val(lat);
			$('#txtlng').val(lng);
			getAddress(parseFloat(lat),parseFloat(lng));
		}

</script>
@endpush