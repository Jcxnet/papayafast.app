@extends('layouts.app')

@section('content')
<ons-page>
   <ons-row align="center" class="height-80">
     <ons-col align="center">
        <div class="logo-xs">&nbsp;</div>
        <br/>
        <div class="text-center text-danger"><ons-icon icon="fa-map-marker" size="3x"></ons-icon></div>
        <br/>
        <div class="text-center text-white">Para brindarte una mejor experiencia necesitamos conocer tu ubicación.</div>
        <br/>
        <div class="text-center clearfix">
          <ons-button class="button" id="btnLocation" modifier="tappable" >Compartir mi ubicación <i class="fa fa-chevron-right"></i></ons-button>
        </div>
      </ons-col>
  </ons-row>
</ons-page>


@endsection

@push('scripts')

<script>
jQuery(document).ready(function($) {

  $('#btnLocation').on('click',function(){
    ons.notification.confirm({
      message: 'Tu ubicación se utilizará para el servicio de delivery y mostrar locales cercanos',
      title: '¿Permitir a Pardos Chicken utilizar tu ubicación cuando utilices la aplicación?',
      buttonLabels: ['Permitir', 'No permitir'],
      animation: 'fade',
      primaryButtonIndex: 0,
      cancelable: false,
      callback: function(index) {
        if(index == -1 || index == 1) window.location = "{{ route('main.menu',['token'=>$token]) }}";
        if(index == 0) window.location = "{{ route('maps.geolocation',['token'=>$token]) }}";
      }
    });
  });

});

</script>

@endpush