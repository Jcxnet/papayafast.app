<?php setlocale(LC_TIME,"es_ES@euro","es_ES","esp"); ?>
@extends('layouts.app')

@section('content')
<ons-page>
	@include ('app.includes.toolbar')
	<ons-row>
		<ons-col>
			{{strftime('%d de %B de %Y - %H:%M', strtotime($time))}}
		</ons-col>
	</ons-row>
	<ons-row>
  	<ons-col>
      <div id="order_products_list">
        @foreach($products as $product)
				<ons-row class="item-product-row" id="product-row-{{$product['orderid']}}" >
				  <ons-col width="120px" class="text-center" align="center">
				    @if( count($product['image'])>0 AND $product['image'] !== FALSE )
				      <img src="{{ $product['image'][0] }}" class="circle-image">
				    @else
				      <img src="{{ asset('img/menu/0.jpg') }}" class="circle-image">
				    @endif
				  </ons-col >
				  <ons-col >
				      <div class="text-uppercase clearfix"><span class="notification">{{$product['cantidad']}}</span>&nbsp;<span class="link-menu text-black font-type-1 select-product-text" > {{ $product['title'] }} </span></div>
				      <span class="text-normal text-muted clearfix"> {{ $product['detail'] }} </span>
				      @include ('app.payments.properties',['properties' => $product['properties']])
				      @include ('app.payments.groups',['groups' => $product['groups']])
				      <span class="text-normal text-black clearfix" id="product_{{$product['orderid']}}_price">S/. {{sprintf("%.2f",$product['priceorder'])}}</span>
				  </ons-col>
				</ons-row>
				<ons-row id="product-row-border-{{$product['orderid']}}"><div class="item-product-border">&nbsp;</div></ons-row>

				@endforeach
        @foreach($payments as $payment)
				<ons-row class="text-normal">
			    <ons-col width="65%" class="text-right">
			      <span class="detail-subtotal">{{$payment['name']}}</span>
			    </ons-col>
			    <ons-col class="text-left">
			      <div class="price-subtotal"><span >{{sprintf("S/. %0.2f",$payment['amount'])}}</span></div>
			    </ons-col>
				</ons-row>
			  @endforeach
			  <ons-row class="text-normal">
			    <ons-col width="65%" class="text-right">
			      <span class="detail-total">Total</span>
			    </ons-col>
			    <ons-col class="text-left">
			      <div class="price-total"><span >{{sprintf("S/. %0.2f",$total)}}</span></div>
			    </ons-col>
				</ons-row>
        <ons-row>
          <ons-col width="100%" class="border-top text-left text-normal">
            <div class="text-center clearfix">
		         <a href="#" id="retryOrder" class="button button--large--quiet text-desc text-danger text-uppercase" modifier="tappable" ><i class="fa fa-plus-circle"></i>&nbsp;Añadir al pedido actual</a>
		      </div>
		      <br/>
          </ons-col>
        </ons-row>
       </div>
  	</ons-col>
</ons-row>
</ons-page>
@endsection

@push('scripts')
<script src="{{asset('lib/js/order.js')}}"></script>
<script>
	jQuery(document).ready(function($) {
    $('#retryOrder').on('click',function(e){
     		e.preventDefault();
     		if(!getLocalData(orderAddress,'id')){
     			showMessage('','Debe seleccionar una dirección para agregar el pedido.');
     			return true;
     		}
     		var products = JSON.parse('{!!json_encode($products)!!}');
				var order = getLocalData(orderProducts,'list');
				var total = products.length;
				var added = 0;
				if(!order)
					order = new Array();
				for(var j in products){
					var product = products[j];
					product = existProductMenuActual(product);
					if(product != false){
						product.orderid = getLatestPosition(order)+1;
						order.push(product);
						added++;
					}
				}
				setLocalData(orderProducts,'list',order);
				if(added==0)
					showMessage('','Los productos de este pedido no se encuentran disponibles en el menú actual. No se agregó ningún producto.');
				else
					showMessage('','Se agregaron '+added+' de '+total+' productos de su pedido anterior a su pedido actual.<br><a href="{{route('orders.actual',['token'=>$token])}}" class="button">Ver mi pedido</a>');
				return true;
    });
  });
</script>
@endpush