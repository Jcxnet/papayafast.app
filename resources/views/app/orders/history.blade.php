<?php setlocale(LC_TIME,"es_ES@euro","es_ES","esp"); ?>
@extends('layouts.app')

@section('content')
<ons-page>
	@include ('app.includes.toolbar')
	@foreach ($orders as $order)

	<?php
		$detail = '';
		foreach($order['products'] as $product){
			$detail .= (($detail!='')?'<br>':'').'<i class="fa fa-dot-circle-o"></i>&nbsp;<span class="text-high">('.$product['cantidad'].')</span>&nbsp;<span class="text-high text-uppercase text-black">'.$product['title'].'</span>';
			/*foreach ($product['properties'] as $property) {
				$detail .= "<br> - <span class='text-muted'>{$property['title']}</span>";
			}
			foreach ($product['groups'] as $group) {
				foreach($group['properties'] as $property){
					if($property['order'] == 1){
						$detail .= "<br> - <span class='text-muted'>{$property['title']}</span>";
						break;
					}
				}
			}*/
		}
		$updateStatus = false;
		if($order['status']!='RECIBIDO' && $order['status']!='ANULADO' && $order['status']!='ERROR DATOS' && $order['status']!='ERROR CONEXION' && $order['status']!='NO STATUS'){
			$urlStatus = route('orders.status',['id'=>$order['id'],'token'=>$token]);
			$updateStatus = true;
		}
		$url = route('orders.detail',['id'=>$order['id'],'token' =>$token]);
		$titleTime = strftime('%d de %B de %Y', strtotime($order['created']['date']));
	?>
	<div class="order-history-item">
	<ons-row  class="order-status-{{strtolower(str_replace(' ','-',$order['status']))}}" id="row-{{$order['id']}}">
		<ons-col>
			<ons-row>
				<ons-col>
					<a href="{{$url}}" class="no-underline"><span class="text-desc text-black text-uppercase">{{ $titleTime }} - #{{$order['number']}} </span></a>
				</ons-col>
			</ons-row>
			<ons-row>
				<ons-col>
					<ons-row><ons-col><span class="text-normal text-muted">{!! $detail !!}</span></ons-col></ons-row>
					<ons-row><ons-col><span id="text-{{$order['id']}}" class="text-normal text-uppercase order-status-{{strtolower(str_replace(' ','-',$order['status']))}}-text">{{$order['status']}}</span> @if ($updateStatus) <a href="#" dataurl="{{$urlStatus}}" id="{{$order['id']}}" class="link-menu text-uppercase status-update"> <i class="fa fa-refresh"></i>&nbsp;Consultar</a>  @endif</ons-col></ons-row>
				</ons-col>
				<ons-col align="right">
					<span class="text-normal text-info">S/. {{ sprintf('%0.2f',($order['total'])) }}</span>
				</ons-col>
			</ons-row>
		</ons-col>
	</ons-row>
	</div>
	@endforeach

<ons-bottom-toolbar>
	<div class="text-center">
  	@if($paginator['first'])
			<a href="{{$paginator['first']}}"><ons-button><ons-icon icon="step-backward"></ons-icon></ons-button></a>
		@else
			<ons-button disabled="true"><ons-icon icon="step-backward"></ons-icon></ons-button>
		@endif
  	@if($paginator['previous'])
			<a href="{{$paginator['previous']}}"><ons-button><ons-icon icon="chevron-left"></ons-icon></ons-button></a>
		@else
			<ons-button disabled="true"><ons-icon icon="chevron-left"></ons-icon></ons-button>
		@endif
		<span class="text-uppercase text-desc font-type-1">{{$paginator['title']}}</span>
		@if($paginator['next'])
			<a href="{{$paginator['next']}}"><ons-button><ons-icon icon="chevron-right"></ons-icon></ons-button></a>
		@else
			<ons-button disabled="true"><ons-icon icon="chevron-right"></ons-icon></ons-button>
		@endif
		@if($paginator['last'])
			<a href="{{$paginator['last']}}"><ons-button><ons-icon icon="step-forward"></ons-icon></ons-button></a>
		@else
			<ons-button disabled="true"><ons-icon icon="step-forward"></ons-icon></ons-button>
		@endif</div>
</ons-bottom-toolbar>
<br>
</ons-page>
<ons-modal id="myModal">
  <ons-icon icon="ion-load-c" spin="true"></ons-icon>
  <br>
  <br> Consultando el estado del pedido...
</ons-modal>
@endsection

@push('scripts')
 	<script>
		$('.status-update').on('click',function(e){
			e.preventDefault();
			var modal = $('#myModal');
			var id = $(this).attr('id');
			$.ajax({
		    method: "POST",
		    url: $(this).attr('dataurl'),
		    beforeSend: function(){
		    	$('#'+id).html('<i class="fa fa-spin fa-refresh"></i>');
		    	modal.show();
		    },
		    success: function(data){
		    	modal.hide();
		    	$('#'+id).html('<i class="fa fa-refresh"></i> Consultar');
		      	switch(data.meta.status){
		      		case 'ok'			: updateOrderStatus(id,data.data.changed,data.data.status,data.data.removelink); break;
		      		default				: showMessage('Aviso', data.data.message);
		      	}
				},
		    error: function (){
		    	modal.hide();
		    	$('#'+id).html('<i class="fa fa-refresh"></i> Consultar');
		    	showMessage('Error','Ocurrió un error al intentar verificar el estado del pedido');
		    }
	    });
	    return false;
		});

		function updateOrderStatus(id,changed,status,nolink){
			if(changed){
				var row = $('#row-'+id);
				var txt = $('#text-'+id);
				row.removeClass().addClass('order-status-'+status);
				txt.removeClass().addClass('text-normal text-uppercase order-status-'+status+'-text').html(status);
				if(nolink){
					$('#'+id).remove();
				}
			}
		}
	</script>
@endpush