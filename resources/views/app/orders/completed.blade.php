@extends('layouts.app')

@section('content')
<ons-page>
	@include ('app.includes.toolbar')
	<ons-row >
		<ons-col class="text-center">
			<br/>
			<i class="fa fa-check text-green fa-4x"></i>
		</ons-col>
	</ons-row>
	<ons-row>
		<ons-col class="text-center">
			<br/>
			<span class="text-uppercase text-title text-green">Tu pedido fue realizado</span><br/>
			<span class="text-uppercase text-high text-success ">pedido {{$number}}</span>
		</ons-col>
	</ons-row>
	<ons-row>
		<ons-col class="text-center text-normal">
			<br>
			<span class="text-muted text-high font-type-2">Hemos enviado la confirmación de tu pedido a tu correo electrónico {{$email}}</span><br/>
			<span class="text-high text-info">Tu pedido llegará en aproximadamente 45 minutos</span>
		</ons-col>
	</ons-row>
	<ons-row>
		<ons-col class="text-center">
			<br/>
			<span class="text-muted text-normal clearfix">&iquest; No has recibido la confirmación?</span>
			<a href="#" class="text-info text-normal no-underline" id="resend">Reenviar la confirmación</a>
		</ons-col>
	</ons-row>
	<ons-row>
		<ons-col class="text-center">
			<br/><br/>
			<a href="{{route('orders.history',['token'=>$token])}}" class="button text-uppercase text-title no-underline">Ver mis pedidos <i class="fa fa-chevron-right"></i></a>
		</ons-col>
	</ons-row>
	<ons-row>
</ons-page>
@endsection

@push('scripts')
<script>
	$('#resend').on('click',function(e){
		e.preventDefault();
		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{route('orders.confirmation')}}",
      data: {'number':'{{$encoded}}','token':'{{$token}}'},
      beforeSend: function(){
      	 $('#resend').html('<i class="fa fa-refresh fa-spin"></i>');
      },
      success: function(data){
      		$('#resend').html('Reenviar la confirmación');
          ons.notification.alert({
			        messageHTML: data.data.message,
			        title: (data.meta.status=='ok')?'Aviso':'Error',
			        animation: 'fade',
			        cancelable: true,
			      });
        }
    });
    return false;
	});
</script>
@endpush