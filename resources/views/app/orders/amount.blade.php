<div class="alert alert-white text-normal" role="alert">
	@foreach($payments as $payment)
	<ons-row>
    <ons-col width="65%" class="text-right">
      <span class="detail-subtotal">{{$payment['name']}}</span>
    </ons-col>
    <ons-col class="text-left">
      <div class="price-subtotal"><span >{{sprintf("S/. %0.2f",$payment['amount'])}}</span></div>
    </ons-col>
	</ons-row>
  @endforeach
<ons-row>
  <ons-col>
    <hr/>
    <div class="text-right clearfix">
      <span class="text-info text-uppercase" ><span class="text-desc">Total S/.</span> <span class="text-title text-blue payment-total">{{sprintf("%0.2f",$total)}}</span></span>
    </div>
  </ons-col>
</ons-row>
</div>