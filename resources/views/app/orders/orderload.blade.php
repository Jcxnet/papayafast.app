@extends('layouts.app')

@push('styles')
  <link rel="stylesheet" href="{{ asset('css/label.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/jquery.webui-popover.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/magic.css') }}" />
@endpush

@section('content')

<ons-page>
   @include ('app.includes.toolbar')

   <div id="products_list">
   	<ons-row align="center" class="height-100">
			<ons-col >
				<div class="logo-xs">&nbsp;</div>
				<div class="alert alert-info" role="alert">
		      <div class="font-type-2 text-high text-center">
		        <p><i class="fa fa-spinner fa-spin fa-fw"></i>&nbsp;Preparando la página...</p>
		      </div>
		    </div>
			</ons-col>
		</ons-row>
   </div>
   <div id="payment_detail"></div>
   <div id="address_detail"></div>

	<ons-bottom-toolbar >
		<div class="text-center" id="div-pay-order" class="no-display">
			<ons-button modifier="large" id="btn-pay-order" >
	  		<span class="text-uppercase"> Continuar </span> <i class="fa fa-chevron-right"></i>
			</ons-button>
		</div>
	</ons-bottom-toolbar>

</ons-page>
<ons-modal id="myModal">
  <ons-icon icon="ion-load-c" spin="true"></ons-icon>
  <br> Verificando la lista de productos...
</ons-modal>
@endsection

@push('scripts')
	<script src="{{ asset('lib/jquery/jquery.webui-popover.js') }}"></script>
	<script src="{{ asset('lib/js/order.js') }}"></script>
  <script>

  jQuery(document).ready(function($) {
  	var tmp = $('#products_list');
		tmp.queue('order',function(next){
				showProducts(tmp);
				next();
			});
		tmp.queue('order',function(next){
				showAddress(tmp);
				next();
			});
		tmp.queue('order',function(next){
				showAmount(tmp);
				next();
			});
		tmp.dequeue('order');

		$('#products_list').scroll(function(){
			WebuiPopovers.hideAll();
		});

	});

		function addListener(){
			$('ul.list-properties > li > a.product-property-selectable').on('click',function(e){
  	 		e.preventDefault();
  	 		updateAmount();
  	 		return false;
  	 });
		}

		function btnPayNow(){
			$('#btn-pay-order').unbind();
			$('#btn-pay-order').on('click',function(e){
				e.preventDefault();
				var next = "{{route('payments.method',['token'=>$token])}}";
				var modal = $('#myModal');
	      $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
		    $.ajax({
				  method: "POST",
				  url: "{{ route('orders.create') }}",
				  data: {'products': JSON.stringify(getLocalData(orderProducts,'list')), 'address': JSON.stringify(getLocalStorage(orderAddress)),'delivery': JSON.stringify(getLocalData(orderMenu,'id')),'token':"{{$token}}"},
				  beforeSend:function(){
				  	styleProducts();
				  	modal.show();
				  },
		    	success:function(data){
		    		modal.hide();
		    		switch(data.meta.status){
							case 'ok'			: if(data.data.total == 0){
																setLocalData(orderPayment,'data',data.data.payment);
																window.location = data.data.url;
															}else{
																showMessage('Aviso',data.data.message);
																showUnavailableProducts(data.data.unavailables);
															}
															break;
		    			case 'error'	: showMessage('Error',data.data.message);
		    											break;
		    			default: showMessage('Alerta','Ocurrió un error inesperado, intente nuevamente');
		    		}
					},
					error: function(){
						modal.hide();
						showMessage('Error','El servicio de verificación de productos se encuentra ocupado, intente nuevamente');
					}
				});
				return false;
			});
		}

		function styleProducts(){
			$("#main_section").find('.item-product-row').removeClass('swipe-main-move');
			$("#main_section").find('.border-pulsate').removeClass('border-pulsate');
			$("#main_section").find('.product-row-remove').removeClass('product-row-remove');
		}

		function showUnavailableProducts(ids){
			for(var i=0;i<ids.length;i++){
				var row = $("#main_section").find(".item-product-row[orderid='"+ids[i]+"']");
				//row.addClass('swipe-main-move');
				row.addClass('product-row-remove');
				row.find('.circle-image').addClass('border-pulsate');
			}
		}

		function showProducts(tmp){
	    $.ajax({
	      method: "POST",
	      url: "{{ route('menus.product.html') }}",
	      data: {'products': JSON.stringify(getLocalData(orderProducts,'list')),'token':'{{$token}}','order':true},
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'html'	: $('#products_list').html('&nbsp;').after(data.data.html);
	      									addListener();
	      									//swipeMenu();
	      									buttonDelete();
	      									buttonDuplicate();
	      									break;
	      		default		: 	tmp.clearQueue();
	      									$('#products_list').html('&nbsp;')
	      									//showMessage('',data.data.message);
	      									//window.location = "{{ route('main.menu',['token'=>$token])}}";
	      	}
				},
	      error: function (){
	      	showMessage('Error','Ocurrió un error al intentar mostrar los productos');
	      	tmp.clearQueue();
	      }
	    });
    }

    function showAddress(tmp){
	    $.ajax({
	      method: "POST",
	      url: "{{ route('addresses.order') }}",
	      data: {'address': JSON.stringify(getLocalStorage(orderAddress)),'page':getLocalData(orderMenu,'page'),'token':'{{$token}}'},
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'html'	: $('#address_detail').html(data.data.html);
	      								break;
	      		case 'error' :tmp.clearQueue();
	      									//showMessage('',data.data.message);
	      									$('#address_detail').html(data.data.html);
	      									break;
	      		default		: showMessage('',data.data.message);
	      	}
				},
	      error: function (){
	      	showMessage('Error','Ocurrió un error al intentar mostrar la dirección para enviar tu pedido');
	      }
	    });
	  }

	  function showAmount(tmp){
	     $.ajax({
	      method: "POST",
	      url: "{{ route('orders.amount') }}",
	      data: {'products': JSON.stringify(getLocalData(orderProducts,'list')),'token':'{{$token}}'},
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'html'	: $('#payment_detail').html(data.data.html);
	      									btnPayNow();
	      									$('#div-pay-order').show();
	      									break;
	      		default			:	if(tmp)
	      										tmp.clearQueue();
	      									$('#div-pay-order').hide();
	      									//showMessage('',data.data.message);
	      									$('#payment_detail').html('&nbsp;');
	      	}
				},
	      error: function (){
	      	var retry = '<a href="{{route('orders.actual',['token'=>$token])}}">Intenta nuevamente</a>';
	      	showMessage('Error','Ocurrió un error al intentar mostrar el precio total de tu pedido.<br/>'+retry);
	      }
	    });
	  }

	  function updateAmount(){
	     $.ajax({
	      method: "POST",
	      url: "{{ route('orders.amount') }}",
	      data: {'products': JSON.stringify(getLocalData(orderProducts,'list')),'token':'{{$token}}'},
	      beforeSend: function(){
	      	$('.price-subtotal, .payment-total').html('<i class="fa fa-refresh fa-spin"></i>');
	      },
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'html'	: $('#payment_detail').html(data.data.html);
	      									btnPayNow();
	      									showToast('','Precio total actualizado');
	      								break;
	      		default		: showMessage('',data.data.message);
	      	}
				},
	      error: function (){
	      	showMessage('Error','Ocurrió un error al intentar mostrar el precio total de tu pedido');
	      }
	    });
	  }

  function buttonDelete(){
  	$('.btnDelete').unbind();
  	$('.btnDelete').on('click',function(e){
	    e.preventDefault();
	    var ntitle  = $(this).attr('nitem');
	    var orderid = $(this).attr('orderid');
	    var posid 	= $(this).attr('posid');
	    ons.notification.confirm({
	      messageHTML: '¿Deseas eliminar el producto <br/><strong>'+ntitle+'</strong><br/>de tu pedido ?',
	      title: 'Eliminar producto',
	      buttonLabels: ['Eliminar', 'Cancelar'],
	      animation: 'fade',
	      primaryButtonIndex: 0,
	      cancelable: false,
	      callback: function(index) {
	        if(index == 0) {
	        	deleteOrderProduct(orderid);
	        	showAmount();
	        	removeFromList(posid);
	        	showToast('','<b>'+ntitle+'</b> eliminado de tu pedido');
	        }
	      }
	    });
	    return false;
	  });
  }

	function buttonDuplicate(){
  	$('.btnDuplicate').unbind();
  	$('.btnDuplicate').on('click',function(e){
  		e.preventDefault();
	    var ntitle  = $(this).attr('nitem');
	    var orderid = $(this).attr('orderid');
	    var posid 	= $(this).attr('posid');
	    ons.notification.confirm({
	      messageHTML: '¿Deseas duplicar el producto <br/><strong>'+ntitle+'</strong><br/>en tu pedido ?',
	      title: 'Duplicar producto',
	      buttonLabels: ['Duplicar', 'Cancelar'],
	      animation: 'fade',
	      primaryButtonIndex: 0,
	      cancelable: false,
	      callback: function(index) {
	        if(index == 0) {
	        	var product = duplicateOrderProduct(orderid);
	        	addToList(posid,product);
	        	showAmount();
	        	swipeProduct(posid);
	        	showToast('','<b>'+ntitle+'</b> agregado a tu pedido');
	        }
	        return false;
	      }
	    });
	  });
  }

  function swipeProduct(pos){
  	var row = $('#product-row-'+pos);
  	row.removeClass('swipe-main-move');
    row.find(".swipe-menu").removeClass('swipe-menu-active').toggleClass('magictime tinLeftIn');
  }

  function addToList(pos,product){
  	$.ajax({
      method: "POST",
      url: "{{ route('menus.product') }}",
      data: {'product': product,'order':true},
      success: function(data){
      	switch(data.meta.status){
      		case 'html'	: $("#product-row-border-"+pos).after(data.data.html);
      									productPropertySelect();
      									addListener();
      									//swipeMenu();
      									buttonDelete();
      									buttonDuplicate();
      									$('.spin-add, .spin-sub').unbind();
      									$('.spin-row').spinner();
      									flipListener();
      									break;
      		default			: showMessage('',data.data.message);
      	}
			},
      error: function (){
      	showMessage('Error','Ocurrió un error al intentar mostrar el producto');
      }
    });
  }

  function removeFromList(posid){
  	/*$('#product-row-'+posid+',#product-row-border-'+posid).fadeTo("normal", 0.01, function(){
       $(this).slideUp("normal", function() {
           $(this).remove();
       });
   	});*/
   	$('#card-'+posid+', #back-side-'+posid).fadeTo("normal", 0.01, function(){
       $(this).slideUp("normal", function() {
           $(this).remove();
       });
   	});
   	$('#product-row-border-'+posid).remove();
  }

  /*function swipeMenu(){
    $(".swipe-main,.swipe-menu").swipe('destroy');
    $(".swipe-main").swipe( {
    swipeRight:function(event, direction, distance, duration, fingerCount) {
      $(this).addClass('swipe-main-move');
      $(this).find(".swipe-menu").addClass('swipe-menu-active').toggleClass('magictime tinLeftIn');
    },
    swipeLeft:function(event, direction, distance, duration, fingerCount) {
      $(this).removeClass('swipe-main-move');
      $(this).find(".swipe-menu").removeClass('swipe-menu-active').toggleClass('magictime tinLeftIn');
    },
    threshold:10
   });

    $(".swipe-menu").swipe( {
      swipeLeft:function(event, direction, distance, duration, fingerCount) {
        $(this).removeClass('swipe-menu-active').toggleClass('magictime tinLeftIn');
        $(this).parent().find(".swipe-main").removeClass('swipe-main-move');
      },
      threshold:10
    });
  }*/

  </script>
@endpush