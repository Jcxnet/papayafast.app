@extends('layouts.app')

@push('styles')
  <link rel="stylesheet" href="{{ asset('css/jquery.webui-popover.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/breadcrumbs.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/accordion.css') }}" />
@endpush

@section('content')
<ons-page>
   @include ('app.includes.toolbar')

		   <ons-carousel fullscreen overscrollable auto-scroll id="carousel" style="overflow-y: scroll;">

		    <ons-carousel-item id="checkout-billing" >
		    	@include ('app.includes.toolbarcheckout',['steps'=>['Datos de envío'=>'current','Método de pago'=>'locked','Enviar pedido'=>'locked']])
		      <div style="width: 96%;" class="center-block">
		      	@include ('app.payments.billing',['token'=>$token])
		      </div>
		    </ons-carousel-item>

		    <ons-carousel-item id="checkout-payment" >
		    	@include ('app.includes.toolbarcheckout',['steps'=>['Datos de envío'=>'visited','Método de pago'=>'current','Enviar pedido'=>'locked']])
		    	<div style="width: 96%; padding-left:3%;" class="">
		    		@include ('app.checkouts.payments',['token'=>$token])
		    	</div>
		    </ons-carousel-item>

		    <ons-carousel-item id="checkout-resume" >
		    	@include ('app.includes.toolbarcheckout',['steps'=>['Datos de envío'=>'visited','Método de pago'=>'visited','Enviar pedido'=>'current']])
		    	<div id="order-resume">
		    		<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
		    	</div>
		    </ons-carousel-item>
		  </ons-carousel>
		<br/>

  <ons-bottom-toolbar id="checkout-toolbar">
		<ons-row>
			<ons-col width="50px">
      	<ons-button modifier="large" onclick="prev()" >
		  		<span class="text-uppercase"> <i class="fa fa-chevron-left"></i> </span>
				</ons-button>
			</ons-col>
			<ons-col> &nbsp; </ons-col>
			<ons-col width="50px">
      	<ons-button modifier="large" onclick="next()" >
		  		<span class="text-uppercase"> <i class="fa fa-chevron-right"></i> </span>
				</ons-button>
			</ons-col>
		</ons-row>
	</ons-bottom-toolbar>

</ons-page>

<ons-modal id="cart-modal">
  @include ('app.payments.productslist',['token'=>$token])
  <div class="text-center">
  	<ons-button id="cart-button-close">
  		Cerrar
  	</ons-button>
  </div>
</ons-modal>
<ons-modal id="validation-modal">&nbsp;</ons-modal>
@endsection

@push('scripts')
  <script type="text/javascript" src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDsAn3nOIS1Q7_RrMcLIzoPJT-Np3PyXkU'></script>
	<script type="text/javascript" src="{{asset('lib/js/gmaps.min.js')}}"></script>
  <script src="{{ asset('lib/jquery/jquery.webui-popover.js') }}"></script>
  <script src="{{ asset('lib/js/numeric.js') }}"></script>
  <script src="{{ asset('lib/js/order.js') }}"></script>
  <script type="text/javascript">

  	var prev = function() {
		  var carousel = document.getElementById('carousel');
		  carousel.prev();
		};

		var next = function() {
		  $('body').find('.label-error').remove();
      $('body').find('.font-type-2').removeClass('input-error');
      var carousel = document.getElementById('carousel');
		  switch(carousel.getActiveIndex()){
		  	case 0: validateBilling(carousel); break;
		  	case 1: validatePayment(carousel); break;
		  	case 2: sendOrder(); break;
		  	default: return false;
		  }
		};

		$('#cart-button-close').on('click',function(){
			$('#cart-modal').hide();
		});

		$('#cart-button-show').on('click',function(){
			$('#cart-modal').show();
		});

		function generateOrder(carousel){
			$.ajax({
	      method: "POST",
	      url: "{{ route('orders.generate') }}",
	      data: {'checkout':getLocalData(orderPayment,'data'),'token':'{{$token}}'},
	      beforeSend: function(){
	      	showNotification('Generar pedido', '<i class="fa fa-spinner fa-spin"></i> Generando tu pedido...');
	      },
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'ok'		 : 	updateNotification('Generar pedido','Pedido generado','success');
	      										resumeOrder(carousel);
	      										break;
	      		case 'error' : 	updateNotification('Generar pedido','Ocurrió un error','error');
	      										showAlert('Error',data.data.message);
	      										break;
	      		default			 : 	updateNotification('Generar pedido','Evento inesperado','warning');
	      										showAlert('',data.data.message);

	      	}
	      	modalValidation(false);
				},
	      error: function (){
	      	updateNotification('Generar pedido','Ocurrió un error','error');
	      	modalValidation(false);
	      	showAlert('Aviso','Ocurrió un error al intentar generar el pedido');
	      }
	    });
		}

		function resumeOrder(carousel){
			$.ajax({
	      method: "POST",
	      url: "{{ route('orders.resume') }}",
	      data: {'checkout':getLocalData(orderPayment,'data'),'token':'{{$token}}'},
	      success: function(data){
	      	modalValidation(false);
	      	switch(data.meta.status){
	      		case 'ok'		 : 	$('#order-resume').html(data.data);
	      										closeNotification();
	      										carousel.next();
	      										break;
	      		case 'error' : 	showAlert('Error',data.data.message);
	      										$('#order-resume').html(data.data);
	      										break;
	      		default			 : 	updateNotification('Mi pedido','Evento inesperado','warning');
	      										showAlert('',data.data.message);

	      	}
				},
	      error: function (){
	      	modalValidation(false);
	      	showAlert('Aviso','Ocurrió un error al intentar mostrar el pedido');
	      }
	    });
		}


		function sendOrder(){
			$.ajax({
	      method: "POST",
	      url: "{{ route('orders.send') }}",
	      data: {'checkout':getLocalData(orderPayment,'data'),'address': JSON.stringify(getLocalStorage(orderAddress)),'token':'{{$token}}'},
	      beforeSend: function(){
	      	modalValidation(true);
	      	showNotification('Enviar pedido', '<i class="fa fa-spinner fa-spin"></i> Enviando pedido al local...');
	      },
	      success: function(data){
	      	switch(data.meta.status){
	      		case 'ok'		 : 	updateNotification('Enviar pedido','Pedido enviado al local','success');
	      										verifyOrder();
	      										break;
	      		case 'error' : 	modalValidation(false);
	      										showAlert('Error',data.data.message);
	      										break;
	      		default			 : 	modalValidation(false);
	      										updateNotification('Enviar pedido','Evento inesperado','warning');
	      										showAlert('',data.data.message);
	      	}
				},
	      error: function (){
	      	modalValidation(false);
	      	showMessage('Aviso','Ocurrió un error al intentar enviar el pedido al local');
	      }
	    });
		}

		function verifyOrder(){
			$.ajax({
	      method: "POST",
	      url: "{{ route('orders.verify') }}",
	      data: {'checkout':getLocalData(orderPayment,'data'),'token':'{{$token}}'},
	      beforeSend: function(){
	      	showNotification('Verificar pedido', '<i class="fa fa-spinner fa-spin"></i> Estamos verificando el pedido enviado...');
	      },
	      success: function(data){
	      	modalValidation(false);
	      	switch(data.meta.status){
	      		case 'ok'		 : 	updateNotification('Verificar pedido','Pedido verificado','success');
	      										clearOrderStorage();
            								releaseAddress();
                            window.location = data.data.url;
	      										break;
	      		case 'error' : 	showAlert('Error',data.data.message);
	      										break;
	      		default			 : 	updateNotification('Verificar pedido','Evento inesperado','warning');
	      										showAlert('',data.data.message);
	      	}
				},
	      error: function (){
	      	modalValidation(false);
	      	showMessage('Aviso','Ocurrió un error al intentar verificar el pedido');
	      }
	    });

		}

		function modalValidation(show){
			if(show){
				$('#validation-modal').show();
			}else{
				$('#validation-modal').hide();
			}
		}

		function showNotification(title,msg){
      theToast = $.toast({
        heading   : title,
        text      : msg,
        hideAfter : false,
        showHideTransition: 'slide',
        allowToastClose: false,
        stack: 6,
        position: 'mid-center',
      });
    }

    function updateNotification(title,msg,type){
      var txtColor; var bgColor; var icon;
      switch(type){
      	case 'info'		: bgColor = '#31708f'; txtColor = '#d9edf7'; icon = '<i class="fa fa-info-circle"></i>'; break;
      	case 'warning': bgColor = '#8a6d3b'; txtColor = '#fcf8e3'; icon = '<i class="fa fa-exclamation-triangle"></i>'; break;
      	case 'error'	: bgColor = '#a94442'; txtColor = '#f2dede'; icon = '<i class="fa fa-exclamation-circle"></i>'; break;
      	case 'success': bgColor = '#3c763d'; txtColor = '#dff0d8'; icon = '<i class="fa fa-check-circle"></i>'; break;
      	default: bgColor = '#2D2D2D'; txtColor = '#ffffff'; icon = '<i class="fa fa-spinner fa-pulse"></i>';
      }
      theToast.update({
        heading 	: title,
        text    	: icon+'&nbsp'+msg,
        bgColor 	: bgColor,
    		textColor : txtColor,
        hideAfter : false
      });
    }

  function releaseAddress(){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }} "} });
    $.ajax({
      method: "POST",
      url: "{{ route('addresses.release') }}",
      data: {'storage': getLocalData(addressesStorage,'data'),'token':'{{$token}}'},
      success: function(data){
        if(data.meta.status == 'ok'){
        		setArrayLocalData(addressesStorage,data.data.storage);
        }
      }
    });
  }

    function closeNotification(){
      $.toast().reset('all');
    }

    function showAlert(title,message){
    	closeNotification();
    	showMessage(title,message);
    }

    function showErrors(errors){
    	closeNotification();
      if(typeof(errors) == 'string')
      	errors = JSON.parse(errors);
      if (errors instanceof Array){
        for(pos in errors){
          error = errors[pos];
          $(error.id).addClass('input-error').next('button').after('<span class="text-danger text-normal label-error pull-left">'+error.msg+'&nbsp;</span>');
        }
      }else{
        $(errors.id).addClass('input-error').next('button').after('<span class="text-danger text-normal label-error pull-left">'+errors.msg+'&nbsp;</span>');
      }
      closeNotification();
      showToast('Se encontraron algunos errores');
    }

  </script>
@endpush