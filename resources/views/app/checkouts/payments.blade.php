
<div class="acc-container">

	<div class="acc-btn"><span class="acc-btn-title acc-btn-selected text-center text-title" data-method="1">Tarjeta de crédito</span></div>
	<div class="acc-content acc-content-open">
	  <div class="acc-content-inner">
	    @include ('app.payments.method-card',['token'=>$token])
	  </div>
	</div>

	<div class="acc-btn"><span class="acc-btn-title text-center text-title" data-method="2">Tarjeta de débito - POS</span></div>
	<div class="acc-content">
	  <div class="acc-content-inner">
	    @include ('app.payments.method-pos',['token'=>$token])
	  </div>
	</div>

	<div class="acc-btn"><span class="acc-btn-title text-center text-title" data-method="3">Efectivo</span></div>
	<div class="acc-content">
	  <div class="acc-content-inner">
	    @include ('app.payments.method-cash',['token'=>$token])
	  </div>
	</div>

</div>


@push('scripts')
<script type="text/javascript">
$(document).ready(function(){
  var animTime = 300,clickPolice = false;

  $(document).on('touchstart click', '.acc-btn', function(){
    if(!clickPolice){
       clickPolice = true;

      var currIndex = $(this).index('.acc-btn'),
          targetHeight = $('.acc-content-inner').eq(currIndex).outerHeight();

      $('.acc-btn-title').removeClass('acc-btn-selected');
      $(this).find('.acc-btn-title').addClass('acc-btn-selected');

      $('.acc-content').stop().animate({ height: 0 }, animTime);
      $('.acc-content').eq(currIndex).stop().animate({ height: targetHeight }, animTime);

      setTimeout(function(){ clickPolice = false; }, animTime);
    }

  });

});

	function dataPayment(){
		var type = $('.acc-btn-selected').attr('data-method');
    if(type == 1){
      var data = {
		    type    		: 'card',
		    nameoncard	: $('#nameoncard').val(),
		    numbercard 	: $('#numbercard').val(),
		    expirecard	: $('#expirecard').val(),
		    securecard	: $('#securecard').val(),
		    storecard		: $("#remembercard").is(':checked'),
		    inputid 		: ['nameoncard','numbercard','expirecard','securecard']
		  };
    }
    if(type == 2){
      var data = {
	      type    : 'pos',
	      total   : $('#total-amount').val(),
	      card    : $('input[name=pos_card]:checked').val(),
	      inputid : '#pos_label'
	    };
    }
    if(type == 3){
      var data = {
		    type    : 'cash',
		    total   : $('#total-amount').val(),
		    amount  : $('#cash_amount').val(),
		    inputid : '#cash_amount'
		  };
    }
    return data;
	}

	function validatePayment(carousel){
		$.ajax({
      method: "POST",
      url: "{{ route('checkouts.payment') }}",
      data: {'payment': dataPayment(),'checkout':getLocalData(orderPayment,'data'),'token':'{{$token}}'},
      beforeSend: function(){
      	modalValidation(true);
      	showNotification('Método de pago', '<i class="fa fa-spinner fa-spin"></i>Validando tu método de pago...');
      },
      success: function(data){
      	switch(data.meta.status){
      		case 'ok'		 : 	updateNotification('Método de pago','Método de pago verificado','success');
      										generateOrder(carousel); //generate the order in erp format
      										break;
      		case 'card'	 :  updateNotification('Método de pago','Método de pago verificado','success');
      										processCreditCard(carousel);
      										break;
      		case 'error' : 	updateNotification('Método de pago','Ocurrió un error','error');
      										showMessage('Error',data.data.message);
      										showErrors(data.data.errors);
      										break;
      		default			 : 	updateNotification('Método de pago','Evento inesperado','warning');
      										showMessage('',data.data.message);

      	}
      	modalValidation(false);
			},
      error: function (){
      	updateNotification('Método de pago','Ocurrió un error','error');
      	modalValidation(false);
      	showMessage('Aviso','Ocurrió un error al intentar validar el método de pago');
      }
    });
	}

	function processCreditCard(carousel){
		$.ajax({
      method: "POST",
      url: "{{ route('checkouts.card') }}",
      data: {'checkout':getLocalData(orderPayment,'data'),'payment': dataPayment(),'token':'{{$token}}'},
      beforeSend: function(){
     		showNotification('Tarjeta de crédito', '<i class="fa fa-spinner fa-spin"></i> Estamos realizando el cargo en tu tarjeta...');
      },
      success: function(data){
      	switch(data.meta.status){
      		case 'ok'		 : 	updateNotification('Tarjeta de crédito','El cargo en la tarjeta fue generado','success');
      										generateOrder(carousel); //generate the order in erp format
      										break;
      		case 'error' : 	modalValidation(false);
      										showAlert('Error',data.data.message);
      										break;
      		default			 : 	modalValidation(false);
      										updateNotification('Tarjeta de crédito','Evento inesperado','warning');
      										showAlert('',data.data.message);
      	}
			},
      error: function (){
      	modalValidation(false);
      	showMessage('Aviso','Ocurrió un error al intentar realizar el cargo en la tarjeta');
      }
    });
	}

</script>
@endpush