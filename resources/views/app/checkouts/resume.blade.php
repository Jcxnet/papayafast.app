<ons-row>
  <ons-col >
  	<h4>Resumen de mi pedido</h4>
  </ons-col>
</ons-row>
<ons-row>
  <ons-col >
    <div class="alert alert-info text-normal" role="alert">
    	<h4 class="text-uppercase">{{$data['billing']['type']}}</h4>
    	<hr>
    	@if($data['billing']['type']=='boleta')
    		Nombre: <span class="text-uppercase">{{$data['billing']['name']}}</span>
    	@else
    		<ul>
    			<li>RUC: <span class="text-uppercase">{{$data['billing']['ruc']}}</span></li>
    			<li>Empresa: <span class="text-uppercase">{{$data['billing']['razon']}}</span></li>
    			<li>Dirección: <span class="text-uppercase">{{$data['billing']['address']}}</span></li>
    		</ul>
    	@endif
    </div>
  </ons-col>
 </ons-row>


 @include ('app.checkouts.addressmap',['address'=>$data['address'],'location'=>$data['location']])

 <ons-row>
  <ons-col >
    <div class="alert alert-warning text-normal" role="alert">
    	<h4 class="text-uppercase">{{$data['payment']['detail']}}</h4>
    	<hr/>
    	@if($data['payment']['method']!='CARD')
    		@if($data['payment']['method']=='CASH')
    		Paga el pedido con <span class="text-uppercase">S/. {{sprintf("%.2f",$data['payment']['info'])}}</span>
    		@endif
    		@if($data['payment']['method']=='POS')
    		Paga con tarjeta <span class="text-uppercase">{{$data['payment']['info']}}</span>
    		@endif
    	@else
    		<ul>
    			<li>Tipo de tarjeta: <span class="text-uppercase">{{$data['payment']['cardtype']}}</span></li>
    			<li>Número de tarjeta: <span class="text-uppercase">****-****-****-{{$data['payment']['info']}}</span></li>
    		</ul>
    	@endif
    </div>
  </ons-col>
 </ons-row>

 @include ('app.checkouts.products',['products'=>$data['products']])

 <ons-row>
  <ons-col >
    <div class="alert alert-info " role="alert">
    	<div style="width:90%" class="center-block">
  			<span class="text-left clearfix">Subtotal: <span class="text-uppercase pull-right">S/. {{sprintf("%.2f",$data['amounts']['subtotal'])}}</span></span>
  			<span class="text-left clearfix">Comisión online: <span class="text-uppercase pull-right">S/. {{sprintf("%.2f",$data['amounts']['comision'])}}</span></span>
  			<hr>
  			<span class="text-left clearfix text-high text-black">Total: <span class="text-uppercase pull-right text-high text-black">S/. {{sprintf("%.2f",$data['amounts']['total'])}}</span></span>
  		</div>
    </div>
  </ons-col>
 </ons-row>
