<ons-row>
  <ons-col >
    <div class="alert alert-danger text-normal" role="alert">
      No se encontró la informacion de tu pedido, intenta nuevamente.
    </div>
    <div class="text-center clearfix">
      <a href="{{route('orders.actual',['token'=>$token])}}" class="text-desc text-info text-uppercase no-underline">Mi pedido</a>
    </div>
    <br/>
  </ons-col>
 </ons-row>