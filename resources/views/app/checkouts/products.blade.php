<ons-row>
  <ons-col>

  	<div class="alert alert-white" role="alert">
    	<h4 class="text-uppercase">Mis productos</h4>
    	<hr/>
        @foreach($products as $product)
				<ons-row class="item-product-row" >
				  <ons-col width="120px" class="text-center" align="center">
				    @if( count($product['image'])>0 AND $product['image'] !== FALSE )
				      <img src="{{ $product['image'][0] }}" class="circle-image">
				    @else
				      <img src="{{ asset('img/menu/0.jpg') }}" class="circle-image">
				    @endif
				  </ons-col >
				  <ons-col >
				      <div class="text-uppercase clearfix"><span class="link-menu text-black font-type-1 select-product-text" ><span class="notification">{{$product['cantidad']}}</span>&nbsp; {{ $product['title'] }} </span></div>
				      <span class="text-normal text-black clearfix" >S/. {{sprintf("%.2f",$product['price'])}}</span>
				  </ons-col>
				</ons-row>
				<ons-row ><div class="item-product-border">&nbsp;</div></ons-row>
				@endforeach
		</div>

  </ons-col>
</ons-row>