  <ons-row>
		<ons-col >
		<div class="alert alert-white" role="alert">
    	<h4 class="text-uppercase">Mi dirección</h4>
    	<hr/>
        	<div id="map_canvas" style="height:170px;"></div>
     </div>
  	</ons-col>
	</ons-row>



<script type="text/javascript">

		//jQuery(document).ready(function($) {

			var map = new GMaps({
		    div: '#map_canvas',
		    lat: {{$location['lat']}},
		    lng: {{$location['lng']}},
		    zoom: 16,
		    streetViewControl: false,
		    mapTypeControl: false,
		    scrollwheel: false,
		    click:function(){
		    	map.panTo({lat: marker_delivery.position.lat(), lng: marker_delivery.position.lng() });
		    	new google.maps.event.trigger( marker_delivery, 'click' );
		    }
		  });

			/*var infowindow = new google.maps.InfoWindow({
				    content: '<strong class="text-danger">{{$address['address']}}</strong><br/><span class="text-blue">Teléfono: {{$address['phone']}}</span>'
			});*/

			var marker_delivery = map.addMarker({
        position: {lat: parseFloat({{$location['lat']}}), lng: parseFloat({{$location['lng']}})},
        animation:google.maps.Animation.DROP,
        icon: "{{ asset('css/img/marker.delivery.png') }}",
        infoWindow: {
        	content: '<strong class="text-danger">{{$address['address']}}</strong><br/><span class="text-blue">Teléfono: {{$address['phone']}}</span>'
        },
			  click: function(e) {
			    map.map.panTo(e.position);
			  }

        /*infoWindow: infowindow,
        click:function(e){
        	if (infowindow){
						infowindow.close();
					}
	    		//map.panTo({lat: marker_delivery.position.lat(), lng: marker_delivery.position.lng() });
	    		infowindow.open(map, marker_delivery);
        }*/
      });

			/*infowindow.open(map, marker_delivery);
			map.panTo({lat: marker_delivery.position.lat(), lng: marker_delivery.position.lng() });*/
			new google.maps.event.trigger( marker_delivery, 'click' );
		//});

</script>
