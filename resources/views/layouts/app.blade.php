<!DOCTYPE html>
<html lang="en" ng-app="app" ng-csp>
  <head>
    <meta charset="utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="mobile-web-app-capable" content="yes" />

    <!-- CSS dependencies -->
    <link href='https://fonts.googleapis.com/css?family=Oswald:300,400' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Istok+Web' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('lib/onsen/css/onsenui.css') }}" />
    <link rel="stylesheet" href="{{ asset('lib/onsen/css/onsen-css-components-sunshine-theme.css') }}" />

    @stack('styles')
    <link rel="stylesheet" href="{{ asset('css/jquery.toast.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/theme.css') }}" />



    <title>{{ (isset($title))?$title: 'PapayaFast v1'}}</title>

    @stack('head_scripts')
  </head>
  <body class="{{ (isset($body_class))?$body_class: 'bg-blue'}} font-type-1">

 		@include('app.includes.slidemenu')

 		<section id="main_section">
 			@yield('content')
 		</section>
    <!-- JS dependencies -->
    <script src="{{ asset('lib/jquery/jquery-2.2.0.min.js') }}"></script>
    <script src="{{ asset('lib/onsen/js/onsenui.js') }}"></script>
    <script src="{{ asset('lib/jquery/jquery.touchSwipe.min.js') }}"></script>
    <script src="{{ asset('lib/js/alert.js') }}"></script>
    <script src="{{ asset('lib/jquery/js.cookie.js') }}"></script>
    <script src="{{ asset('lib/jquery/jquery.storageapi.js') }}"></script>
    <script src="{{ asset('lib/jquery/jquery.toast.js') }}"></script>
    <script src="{{ asset('lib/js/common.js') }}"></script>

    @if (Session::has('message'))
   		<script> showMessage('',"{!! session('message') !!}"); </script>
  	@endif

    @stack('scripts')
  </body>
</html>